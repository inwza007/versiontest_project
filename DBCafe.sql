--
-- File generated with SQLiteStudio v3.4.4 on Mon Oct 23 18:28:50 2023
--
-- Text encoding used: UTF-8
--
PRAGMA foreign_keys = off;
BEGIN TRANSACTION;

-- Table: customer
DROP TABLE IF EXISTS customer;

CREATE TABLE IF NOT EXISTS customer (
    customer_id        INTEGER   PRIMARY KEY ASC AUTOINCREMENT
                                 UNIQUE
                                 NOT NULL,
    customer_firstname TEXT      NOT NULL,
    customer_lastname  TEXT      NOT NULL,
    customer_tel       TEXT (10) UNIQUE
                                 NOT NULL,
    customer_point     INTEGER   NOT NULL,
    customer_birthdate DATE      DEFAULT ( (date('now', 'localtime') ) ) 
                                 NOT NULL
);

INSERT INTO customer (
                         customer_id,
                         customer_firstname,
                         customer_lastname,
                         customer_tel,
                         customer_point,
                         customer_birthdate
                     )
                     VALUES (
                         1,
                         'New',
                         'Gen',
                         '0887679156',
                         180,
                         '2004-05-15 00:00:00'
                     );

INSERT INTO customer (
                         customer_id,
                         customer_firstname,
                         customer_lastname,
                         customer_tel,
                         customer_point,
                         customer_birthdate
                     )
                     VALUES (
                         2,
                         'John',
                         'Doe',
                         '0887654321',
                         10,
                         '2005-08-20 00:00:00'
                     );

INSERT INTO customer (
                         customer_id,
                         customer_firstname,
                         customer_lastname,
                         customer_tel,
                         customer_point,
                         customer_birthdate
                     )
                     VALUES (
                         3,
                         'New',
                         'Smith',
                         '0826134985',
                         20,
                         '2000-10-06 00:00:00'
                     );

INSERT INTO customer (
                         customer_id,
                         customer_firstname,
                         customer_lastname,
                         customer_tel,
                         customer_point,
                         customer_birthdate
                     )
                     VALUES (
                         4,
                         'Jane',
                         'Gen',
                         '0945163750',
                         40,
                         '2003-09-30 00:00:00'
                     );

INSERT INTO customer (
                         customer_id,
                         customer_firstname,
                         customer_lastname,
                         customer_tel,
                         customer_point,
                         customer_birthdate
                     )
                     VALUES (
                         5,
                         'Smith',
                         'Dog',
                         '0896581936',
                         44,
                         '2012-12-08 00:00:00'
                     );

INSERT INTO customer (
                         customer_id,
                         customer_firstname,
                         customer_lastname,
                         customer_tel,
                         customer_point,
                         customer_birthdate
                     )
                     VALUES (
                         6,
                         'Jennifer',
                         'Miller',
                         '0649153720',
                         55,
                         '2013-07-09 00:00:00'
                     );

INSERT INTO customer (
                         customer_id,
                         customer_firstname,
                         customer_lastname,
                         customer_tel,
                         customer_point,
                         customer_birthdate
                     )
                     VALUES (
                         7,
                         'William',
                         'Wilson',
                         '0899636963',
                         82,
                         '1998-06-19 00:00:00'
                     );

INSERT INTO customer (
                         customer_id,
                         customer_firstname,
                         customer_lastname,
                         customer_tel,
                         customer_point,
                         customer_birthdate
                     )
                     VALUES (
                         8,
                         'Linda',
                         'Moore',
                         '0846153720',
                         63,
                         '1999-01-17 00:00:00'
                     );

INSERT INTO customer (
                         customer_id,
                         customer_firstname,
                         customer_lastname,
                         customer_tel,
                         customer_point,
                         customer_birthdate
                     )
                     VALUES (
                         9,
                         'David',
                         'Taylor',
                         '0963582471',
                         7,
                         '1990-03-16 00:00:00'
                     );

INSERT INTO customer (
                         customer_id,
                         customer_firstname,
                         customer_lastname,
                         customer_tel,
                         customer_point,
                         customer_birthdate
                     )
                     VALUES (
                         10,
                         'Patricia',
                         'Anderson',
                         '0996345854',
                         40,
                         '1990-04-13 00:00:00'
                     );


-- Table: material
DROP TABLE IF EXISTS material;

CREATE TABLE IF NOT EXISTS material (
    material_id     INTEGER   PRIMARY KEY ASC AUTOINCREMENT
                              UNIQUE
                              NOT NULL,
    material_name   TEXT (50) NOT NULL,
    material_qty    INTEGER   NOT NULL,
    material_min    INTEGER   NOT NULL,
    material_unit   TEXT (15) NOT NULL,
    material_status TEXT (10) CHECK (material_status IN ('Full', 'Almost', 'Destitute') ) 
                              NOT NULL,
    material_price  REAL
);

INSERT INTO material (
                         material_id,
                         material_name,
                         material_qty,
                         material_min,
                         material_unit,
                         material_status,
                         material_price
                     )
                     VALUES (
                         1,
                         'กาแฟเมล็ด',
                         100,
                         10,
                         'กิโลกรัม',
                         'Full',
                         300.0
                     );

INSERT INTO material (
                         material_id,
                         material_name,
                         material_qty,
                         material_min,
                         material_unit,
                         material_status,
                         material_price
                     )
                     VALUES (
                         2,
                         'นมสด',
                         50,
                         10,
                         'ลิตร',
                         'Full',
                         50.0
                     );

INSERT INTO material (
                         material_id,
                         material_name,
                         material_qty,
                         material_min,
                         material_unit,
                         material_status,
                         material_price
                     )
                     VALUES (
                         3,
                         'น้ำตาล',
                         200,
                         50,
                         'กิโลกรัม',
                         'Full',
                         30.0
                     );

INSERT INTO material (
                         material_id,
                         material_name,
                         material_qty,
                         material_min,
                         material_unit,
                         material_status,
                         material_price
                     )
                     VALUES (
                         4,
                         'ควิกเนสทีน',
                         30,
                         15,
                         'กิโลกรัม',
                         'Full',
                         500.0
                     );

INSERT INTO material (
                         material_id,
                         material_name,
                         material_qty,
                         material_min,
                         material_unit,
                         material_status,
                         material_price
                     )
                     VALUES (
                         5,
                         'นมข้นหวาน',
                         20,
                         10,
                         'กล่อง',
                         'Full',
                         70.0
                     );

INSERT INTO material (
                         material_id,
                         material_name,
                         material_qty,
                         material_min,
                         material_unit,
                         material_status,
                         material_price
                     )
                     VALUES (
                         6,
                         'ผงกาแฟ',
                         30,
                         5,
                         'กิโลกรัม',
                         'Full',
                         50.0
                     );


-- Table: material_check
DROP TABLE IF EXISTS material_check;

CREATE TABLE IF NOT EXISTS material_check (
    mc_id   INTEGER PRIMARY KEY
                    UNIQUE
                    NOT NULL,
    user_id INTEGER REFERENCES user (user_id) ON DELETE CASCADE
                                              ON UPDATE CASCADE
                    NOT NULL,
    mc_time DATE    DEFAULT ( (datetime('now', 'localtime') ) ) 
                    NOT NULL
);

INSERT INTO material_check (
                               mc_id,
                               user_id,
                               mc_time
                           )
                           VALUES (
                               1,
                               1,
                               '2023-10-02 13:42:51'
                           );

INSERT INTO material_check (
                               mc_id,
                               user_id,
                               mc_time
                           )
                           VALUES (
                               2,
                               2,
                               '2023-10-02 13:42:52'
                           );

INSERT INTO material_check (
                               mc_id,
                               user_id,
                               mc_time
                           )
                           VALUES (
                               3,
                               3,
                               '2023-10-02 13:42:51'
                           );


-- Table: material_check_detail
DROP TABLE IF EXISTS material_check_detail;

CREATE TABLE IF NOT EXISTS material_check_detail (
    mcd_id            INTEGER   PRIMARY KEY
                                UNIQUE
                                NOT NULL,
    mc_id             INTEGER   REFERENCES material_check (mc_id) ON DELETE CASCADE
                                                                  ON UPDATE CASCADE
                                NOT NULL,
    material_id       INTEGER   REFERENCES material (material_id) ON DELETE CASCADE
                                                                  ON UPDATE CASCADE
                                NOT NULL,
    supplier_id       INTEGER   REFERENCES supplier (supplier_id) ON DELETE CASCADE
                                                                  ON UPDATE CASCADE
                                NOT NULL,
    mcd_dmg_qty       INTEGER   NOT NULL,
    mcd_qty_per_price INTEGER   NOT NULL,
    mcd_dmg_total     INTEGER   NOT NULL,
    mcd_unit          TEXT (15) NOT NULL,
    mcd_date_import   DATE      DEFAULT (date(datetime('now', 'localtime') ) ) 
                                NOT NULL,
    mcd_exd           DATE      DEFAULT (date(datetime('now', 'localtime') ) ) 
                                NOT NULL
);

INSERT INTO material_check_detail (
                                      mcd_id,
                                      mc_id,
                                      material_id,
                                      supplier_id,
                                      mcd_dmg_qty,
                                      mcd_qty_per_price,
                                      mcd_dmg_total,
                                      mcd_unit,
                                      mcd_date_import,
                                      mcd_exd
                                  )
                                  VALUES (
                                      1,
                                      1,
                                      1,
                                      1,
                                      0,
                                      50,
                                      0,
                                      'กิโลกรัม',
                                      '2023-10-14',
                                      '2023-10-14'
                                  );

INSERT INTO material_check_detail (
                                      mcd_id,
                                      mc_id,
                                      material_id,
                                      supplier_id,
                                      mcd_dmg_qty,
                                      mcd_qty_per_price,
                                      mcd_dmg_total,
                                      mcd_unit,
                                      mcd_date_import,
                                      mcd_exd
                                  )
                                  VALUES (
                                      2,
                                      2,
                                      2,
                                      2,
                                      0,
                                      40,
                                      0,
                                      'กิโลกรัม',
                                      '2023-10-14',
                                      '2023-10-14'
                                  );

INSERT INTO material_check_detail (
                                      mcd_id,
                                      mc_id,
                                      material_id,
                                      supplier_id,
                                      mcd_dmg_qty,
                                      mcd_qty_per_price,
                                      mcd_dmg_total,
                                      mcd_unit,
                                      mcd_date_import,
                                      mcd_exd
                                  )
                                  VALUES (
                                      3,
                                      3,
                                      3,
                                      3,
                                      0,
                                      50,
                                      0,
                                      'กิโลกรัม',
                                      '2023-10-14',
                                      '2023-10-14'
                                  );


-- Table: material_reciept
DROP TABLE IF EXISTS material_reciept;

CREATE TABLE IF NOT EXISTS material_reciept (
    mr_id        INTEGER  PRIMARY KEY
                          UNIQUE
                          NOT NULL,
    mr_date      DATETIME DEFAULT ( (datetime('now', 'localtime') ) ) 
                          NOT NULL,
    supplier_id  INTEGER  REFERENCES supplier (supplier_id) ON DELETE CASCADE
                                                            ON UPDATE CASCADE
                          NOT NULL,
    mr_total_qty INTEGER  NOT NULL,
    mr_total     INTEGER  NOT NULL,
    user_id      INTEGER  REFERENCES user (user_id) ON DELETE CASCADE
                                                    ON UPDATE CASCADE,
    store_id     INTEGER  REFERENCES store (store_id) ON DELETE CASCADE
                                                      ON UPDATE CASCADE
);

INSERT INTO material_reciept (
                                 mr_id,
                                 mr_date,
                                 supplier_id,
                                 mr_total_qty,
                                 mr_total,
                                 user_id,
                                 store_id
                             )
                             VALUES (
                                 1,
                                 '2023-10-14 08:03:34',
                                 1,
                                 5,
                                 750,
                                 1,
                                 1
                             );

INSERT INTO material_reciept (
                                 mr_id,
                                 mr_date,
                                 supplier_id,
                                 mr_total_qty,
                                 mr_total,
                                 user_id,
                                 store_id
                             )
                             VALUES (
                                 2,
                                 '2023-10-14 10:53:12',
                                 2,
                                 2,
                                 60,
                                 2,
                                 2
                             );

INSERT INTO material_reciept (
                                 mr_id,
                                 mr_date,
                                 supplier_id,
                                 mr_total_qty,
                                 mr_total,
                                 user_id,
                                 store_id
                             )
                             VALUES (
                                 3,
                                 '2023-10-14 13:43:07',
                                 3,
                                 1,
                                 30,
                                 3,
                                 3
                             );

INSERT INTO material_reciept (
                                 mr_id,
                                 mr_date,
                                 supplier_id,
                                 mr_total_qty,
                                 mr_total,
                                 user_id,
                                 store_id
                             )
                             VALUES (
                                 4,
                                 '2023-10-22 07:30:27',
                                 1,
                                 8,
                                 890,
                                 NULL,
                                 NULL
                             );


-- Table: material_reciept_detail
DROP TABLE IF EXISTS material_reciept_detail;

CREATE TABLE IF NOT EXISTS material_reciept_detail (
    mrd_id        INTEGER PRIMARY KEY
                          UNIQUE
                          NOT NULL,
    mr_id         INTEGER REFERENCES material_reciept (mr_id) ON DELETE CASCADE
                                                              ON UPDATE CASCADE
                          NOT NULL,
    material_id   INTEGER REFERENCES material (material_id) ON DELETE CASCADE
                                                            ON UPDATE CASCADE
                          NOT NULL,
    material_name TEXT    REFERENCES material (material_name),
    material_unit TEXT    REFERENCES material (material_unit),
    mrd_price     REAL,
    mrd_qty       INTEGER,
    mrd_total     REAL
);

INSERT INTO material_reciept_detail (
                                        mrd_id,
                                        mr_id,
                                        material_id,
                                        material_name,
                                        material_unit,
                                        mrd_price,
                                        mrd_qty,
                                        mrd_total
                                    )
                                    VALUES (
                                        1,
                                        1,
                                        1,
                                        'กาแฟเมล็ด',
                                        'กิโลกรัม',
                                        300.0,
                                        2,
                                        600.0
                                    );

INSERT INTO material_reciept_detail (
                                        mrd_id,
                                        mr_id,
                                        material_id,
                                        material_name,
                                        material_unit,
                                        mrd_price,
                                        mrd_qty,
                                        mrd_total
                                    )
                                    VALUES (
                                        2,
                                        1,
                                        2,
                                        'นมสด',
                                        'ลิตร',
                                        50.0,
                                        3,
                                        150.0
                                    );

INSERT INTO material_reciept_detail (
                                        mrd_id,
                                        mr_id,
                                        material_id,
                                        material_name,
                                        material_unit,
                                        mrd_price,
                                        mrd_qty,
                                        mrd_total
                                    )
                                    VALUES (
                                        3,
                                        2,
                                        3,
                                        'น้ำตาล',
                                        'กิโลกรัม',
                                        30.0,
                                        2,
                                        60.0
                                    );

INSERT INTO material_reciept_detail (
                                        mrd_id,
                                        mr_id,
                                        material_id,
                                        material_name,
                                        material_unit,
                                        mrd_price,
                                        mrd_qty,
                                        mrd_total
                                    )
                                    VALUES (
                                        4,
                                        3,
                                        3,
                                        'น้ำตาล',
                                        'กิโลกรัม',
                                        30.0,
                                        1,
                                        30.0
                                    );

INSERT INTO material_reciept_detail (
                                        mrd_id,
                                        mr_id,
                                        material_id,
                                        material_name,
                                        material_unit,
                                        mrd_price,
                                        mrd_qty,
                                        mrd_total
                                    )
                                    VALUES (
                                        5,
                                        4,
                                        4,
                                        'ควิกเนสทีน',
                                        'กิโลกรัม',
                                        500.0,
                                        1,
                                        500.0
                                    );

INSERT INTO material_reciept_detail (
                                        mrd_id,
                                        mr_id,
                                        material_id,
                                        material_name,
                                        material_unit,
                                        mrd_price,
                                        mrd_qty,
                                        mrd_total
                                    )
                                    VALUES (
                                        6,
                                        4,
                                        5,
                                        'นมข้นหวาน',
                                        'กล่อง',
                                        70.0,
                                        2,
                                        140.0
                                    );

INSERT INTO material_reciept_detail (
                                        mrd_id,
                                        mr_id,
                                        material_id,
                                        material_name,
                                        material_unit,
                                        mrd_price,
                                        mrd_qty,
                                        mrd_total
                                    )
                                    VALUES (
                                        7,
                                        4,
                                        2,
                                        'นมสด',
                                        'ลิตร',
                                        50.0,
                                        1,
                                        50.0
                                    );

INSERT INTO material_reciept_detail (
                                        mrd_id,
                                        mr_id,
                                        material_id,
                                        material_name,
                                        material_unit,
                                        mrd_price,
                                        mrd_qty,
                                        mrd_total
                                    )
                                    VALUES (
                                        8,
                                        4,
                                        6,
                                        'ผงกาแฟ',
                                        'กิโลกรัม',
                                        50.0,
                                        4,
                                        200.0
                                    );


-- Table: payroll
DROP TABLE IF EXISTS payroll;

CREATE TABLE IF NOT EXISTS payroll (
    payroll_id              INTEGER         PRIMARY KEY ASC AUTOINCREMENT
                                            UNIQUE
                                            NOT NULL,
    user_id                 INTEGER         NOT NULL
                                            REFERENCES user (user_id) ON DELETE CASCADE
                                                                      ON UPDATE CASCADE,
    payroll_approve         TEXT (8)        NOT NULL
                                            CHECK (payroll_approve IN ('No', 'Yes') ),
    payroll_date            DATETIME        DEFAULT (datetime('now', 'localtime') ) 
                                            NOT NULL,
    payroll_type            TEXT (10)       CHECK (payroll_type IN ('Cash', 'PromptPay', 'Credit') ) 
                                            NOT NULL,
    payroll_total_time_work INTEGER         NOT NULL,
    payroll_price_per_hour  INTEGER         NOT NULL,
    payroll_total_salary    DECIMAL (10, 2) NOT NULL
);

INSERT INTO payroll (
                        payroll_id,
                        user_id,
                        payroll_approve,
                        payroll_date,
                        payroll_type,
                        payroll_total_time_work,
                        payroll_price_per_hour,
                        payroll_total_salary
                    )
                    VALUES (
                        1,
                        1,
                        'Yes',
                        '2023-10-14',
                        'Cash',
                        9,
                        45,
                        405
                    );

INSERT INTO payroll (
                        payroll_id,
                        user_id,
                        payroll_approve,
                        payroll_date,
                        payroll_type,
                        payroll_total_time_work,
                        payroll_price_per_hour,
                        payroll_total_salary
                    )
                    VALUES (
                        2,
                        2,
                        'Yes',
                        '2023-10-14',
                        'Cash',
                        9,
                        45,
                        405
                    );

INSERT INTO payroll (
                        payroll_id,
                        user_id,
                        payroll_approve,
                        payroll_date,
                        payroll_type,
                        payroll_total_time_work,
                        payroll_price_per_hour,
                        payroll_total_salary
                    )
                    VALUES (
                        3,
                        3,
                        'Yes',
                        '2023-10-14',
                        'Credit',
                        9,
                        45,
                        405
                    );

INSERT INTO payroll (
                        payroll_id,
                        user_id,
                        payroll_approve,
                        payroll_date,
                        payroll_type,
                        payroll_total_time_work,
                        payroll_price_per_hour,
                        payroll_total_salary
                    )
                    VALUES (
                        4,
                        4,
                        'Yes',
                        '2023-10-14',
                        'Cash',
                        9,
                        45,
                        405
                    );

INSERT INTO payroll (
                        payroll_id,
                        user_id,
                        payroll_approve,
                        payroll_date,
                        payroll_type,
                        payroll_total_time_work,
                        payroll_price_per_hour,
                        payroll_total_salary
                    )
                    VALUES (
                        5,
                        5,
                        'Yes',
                        '2023-10-14',
                        'PromptPay',
                        9,
                        45,
                        405
                    );

INSERT INTO payroll (
                        payroll_id,
                        user_id,
                        payroll_approve,
                        payroll_date,
                        payroll_type,
                        payroll_total_time_work,
                        payroll_price_per_hour,
                        payroll_total_salary
                    )
                    VALUES (
                        6,
                        6,
                        'Yes',
                        '2023-10-14',
                        'PromptPay',
                        9,
                        45,
                        405
                    );

INSERT INTO payroll (
                        payroll_id,
                        user_id,
                        payroll_approve,
                        payroll_date,
                        payroll_type,
                        payroll_total_time_work,
                        payroll_price_per_hour,
                        payroll_total_salary
                    )
                    VALUES (
                        7,
                        7,
                        'Yes',
                        '2023-10-14',
                        'Credit',
                        9,
                        45,
                        405
                    );

INSERT INTO payroll (
                        payroll_id,
                        user_id,
                        payroll_approve,
                        payroll_date,
                        payroll_type,
                        payroll_total_time_work,
                        payroll_price_per_hour,
                        payroll_total_salary
                    )
                    VALUES (
                        8,
                        8,
                        'No',
                        '2023-10-14',
                        'PromptPay',
                        9,
                        45,
                        405
                    );

INSERT INTO payroll (
                        payroll_id,
                        user_id,
                        payroll_approve,
                        payroll_date,
                        payroll_type,
                        payroll_total_time_work,
                        payroll_price_per_hour,
                        payroll_total_salary
                    )
                    VALUES (
                        9,
                        9,
                        'Yes',
                        '2023-10-14',
                        'Credit',
                        9,
                        45,
                        405
                    );

INSERT INTO payroll (
                        payroll_id,
                        user_id,
                        payroll_approve,
                        payroll_date,
                        payroll_type,
                        payroll_total_time_work,
                        payroll_price_per_hour,
                        payroll_total_salary
                    )
                    VALUES (
                        10,
                        10,
                        'Yes',
                        '2023-10-14',
                        'Credit',
                        9,
                        45,
                        405
                    );

INSERT INTO payroll (
                        payroll_id,
                        user_id,
                        payroll_approve,
                        payroll_date,
                        payroll_type,
                        payroll_total_time_work,
                        payroll_price_per_hour,
                        payroll_total_salary
                    )
                    VALUES (
                        11,
                        11,
                        'No',
                        '2023-10-14',
                        'PromptPay',
                        9,
                        45,
                        405
                    );

INSERT INTO payroll (
                        payroll_id,
                        user_id,
                        payroll_approve,
                        payroll_date,
                        payroll_type,
                        payroll_total_time_work,
                        payroll_price_per_hour,
                        payroll_total_salary
                    )
                    VALUES (
                        12,
                        12,
                        'No',
                        '2023-10-14',
                        'PromptPay',
                        9,
                        45,
                        405
                    );

INSERT INTO payroll (
                        payroll_id,
                        user_id,
                        payroll_approve,
                        payroll_date,
                        payroll_type,
                        payroll_total_time_work,
                        payroll_price_per_hour,
                        payroll_total_salary
                    )
                    VALUES (
                        13,
                        13,
                        'No',
                        '2023-10-14',
                        'Cash',
                        9,
                        45,
                        405
                    );

INSERT INTO payroll (
                        payroll_id,
                        user_id,
                        payroll_approve,
                        payroll_date,
                        payroll_type,
                        payroll_total_time_work,
                        payroll_price_per_hour,
                        payroll_total_salary
                    )
                    VALUES (
                        14,
                        14,
                        'Yes',
                        '2023-10-14',
                        'Credit',
                        9,
                        45,
                        405
                    );

INSERT INTO payroll (
                        payroll_id,
                        user_id,
                        payroll_approve,
                        payroll_date,
                        payroll_type,
                        payroll_total_time_work,
                        payroll_price_per_hour,
                        payroll_total_salary
                    )
                    VALUES (
                        15,
                        15,
                        'Yes',
                        '2023-10-14',
                        'Cash',
                        9,
                        45,
                        405
                    );


-- Table: product
DROP TABLE IF EXISTS product;

CREATE TABLE IF NOT EXISTS product (
    product_id          INTEGER   PRIMARY KEY ASC AUTOINCREMENT
                                  NOT NULL,
    product_name        TEXT (50) UNIQUE
                                  NOT NULL,
    product_price       REAL      NOT NULL,
    product_type        TEXT (10) NOT NULL
                                  CHECK (product_type IN ('drink', 'dessert') ),
    product_sub_type    TEXT (5)  NOT NULL
                                  DEFAULT HCF,
    product_size        TEXT (5)  DEFAULT SML
                                  NOT NULL,
    product_sweet_level TEXT (5)  DEFAULT (123) 
                                  NOT NULL
);

INSERT INTO product (
                        product_id,
                        product_name,
                        product_price,
                        product_type,
                        product_sub_type,
                        product_size,
                        product_sweet_level
                    )
                    VALUES (
                        1,
                        'Espresso',
                        50.0,
                        'drink',
                        'HCF',
                        'SML',
                        '0123'
                    );

INSERT INTO product (
                        product_id,
                        product_name,
                        product_price,
                        product_type,
                        product_sub_type,
                        product_size,
                        product_sweet_level
                    )
                    VALUES (
                        2,
                        'Americano',
                        40.0,
                        'drink',
                        'HC',
                        'SML',
                        '0123'
                    );

INSERT INTO product (
                        product_id,
                        product_name,
                        product_price,
                        product_type,
                        product_sub_type,
                        product_size,
                        product_sweet_level
                    )
                    VALUES (
                        3,
                        'Pumpkin & Pepita Loaf',
                        50.0,
                        'dessert',
                        '-',
                        '-',
                        '-'
                    );

INSERT INTO product (
                        product_id,
                        product_name,
                        product_price,
                        product_type,
                        product_sub_type,
                        product_size,
                        product_sweet_level
                    )
                    VALUES (
                        4,
                        'Butter Cake',
                        60.0,
                        'dessert',
                        '-',
                        '-',
                        '-'
                    );

INSERT INTO product (
                        product_id,
                        product_name,
                        product_price,
                        product_type,
                        product_sub_type,
                        product_size,
                        product_sweet_level
                    )
                    VALUES (
                        5,
                        'Cappuccino',
                        50.0,
                        'drink',
                        'HC',
                        'SML',
                        '0123'
                    );

INSERT INTO product (
                        product_id,
                        product_name,
                        product_price,
                        product_type,
                        product_sub_type,
                        product_size,
                        product_sweet_level
                    )
                    VALUES (
                        6,
                        'Green Tea',
                        55.0,
                        'drink',
                        'HCF',
                        'SML',
                        '0123'
                    );

INSERT INTO product (
                        product_id,
                        product_name,
                        product_price,
                        product_type,
                        product_sub_type,
                        product_size,
                        product_sweet_level
                    )
                    VALUES (
                        7,
                        'Latté',
                        45.0,
                        'drink',
                        'HC',
                        'SML',
                        '0123'
                    );

INSERT INTO product (
                        product_id,
                        product_name,
                        product_price,
                        product_type,
                        product_sub_type,
                        product_size,
                        product_sweet_level
                    )
                    VALUES (
                        8,
                        'Oat Milk Cappuccino',
                        55.0,
                        'drink',
                        'HCF',
                        'SML',
                        '0123'
                    );

INSERT INTO product (
                        product_id,
                        product_name,
                        product_price,
                        product_type,
                        product_sub_type,
                        product_size,
                        product_sweet_level
                    )
                    VALUES (
                        9,
                        'Mocha',
                        55.0,
                        'drink',
                        'HC',
                        'SML',
                        '0123'
                    );

INSERT INTO product (
                        product_id,
                        product_name,
                        product_price,
                        product_type,
                        product_sub_type,
                        product_size,
                        product_sweet_level
                    )
                    VALUES (
                        10,
                        'Muffin',
                        50.0,
                        'dessert',
                        '-',
                        '-',
                        '-'
                    );

INSERT INTO product (
                        product_id,
                        product_name,
                        product_price,
                        product_type,
                        product_sub_type,
                        product_size,
                        product_sweet_level
                    )
                    VALUES (
                        11,
                        'Baked Apple Croissant',
                        50.0,
                        'dessert',
                        '-',
                        '-',
                        '-'
                    );

INSERT INTO product (
                        product_id,
                        product_name,
                        product_price,
                        product_type,
                        product_sub_type,
                        product_size,
                        product_sweet_level
                    )
                    VALUES (
                        12,
                        'Butter Croissant',
                        55.0,
                        'dessert',
                        '-',
                        '-',
                        '-'
                    );

INSERT INTO product (
                        product_id,
                        product_name,
                        product_price,
                        product_type,
                        product_sub_type,
                        product_size,
                        product_sweet_level
                    )
                    VALUES (
                        13,
                        'Ham Croissant',
                        60.0,
                        'dessert',
                        '-',
                        '-',
                        '-'
                    );

INSERT INTO product (
                        product_id,
                        product_name,
                        product_price,
                        product_type,
                        product_sub_type,
                        product_size,
                        product_sweet_level
                    )
                    VALUES (
                        14,
                        'Cheese Danish',
                        60.0,
                        'dessert',
                        '-',
                        '-',
                        '-'
                    );

INSERT INTO product (
                        product_id,
                        product_name,
                        product_price,
                        product_type,
                        product_sub_type,
                        product_size,
                        product_sweet_level
                    )
                    VALUES (
                        15,
                        'Blueberry Muffin',
                        45.0,
                        'dessert',
                        '-',
                        '-',
                        '-'
                    );

INSERT INTO product (
                        product_id,
                        product_name,
                        product_price,
                        product_type,
                        product_sub_type,
                        product_size,
                        product_sweet_level
                    )
                    VALUES (
                        16,
                        'Chocolate Chip Cookie',
                        55.0,
                        'dessert',
                        '-',
                        '-',
                        '-'
                    );

INSERT INTO product (
                        product_id,
                        product_name,
                        product_price,
                        product_type,
                        product_sub_type,
                        product_size,
                        product_sweet_level
                    )
                    VALUES (
                        17,
                        'Plain Bagel',
                        50.0,
                        'dessert',
                        '-',
                        '-',
                        '-'
                    );

INSERT INTO product (
                        product_id,
                        product_name,
                        product_price,
                        product_type,
                        product_sub_type,
                        product_size,
                        product_sweet_level
                    )
                    VALUES (
                        18,
                        'Caramel milk',
                        40.0,
                        'drink',
                        'HCF',
                        'SML',
                        '0123'
                    );

INSERT INTO product (
                        product_id,
                        product_name,
                        product_price,
                        product_type,
                        product_sub_type,
                        product_size,
                        product_sweet_level
                    )
                    VALUES (
                        19,
                        'Pineapple ',
                        45.0,
                        'drink',
                        'HC',
                        'SML',
                        '0123'
                    );

INSERT INTO product (
                        product_id,
                        product_name,
                        product_price,
                        product_type,
                        product_sub_type,
                        product_size,
                        product_sweet_level
                    )
                    VALUES (
                        20,
                        'Thai tea',
                        55.0,
                        'drink',
                        'HCF',
                        'SML',
                        '0123'
                    );

INSERT INTO product (
                        product_id,
                        product_name,
                        product_price,
                        product_type,
                        product_sub_type,
                        product_size,
                        product_sweet_level
                    )
                    VALUES (
                        21,
                        'Glazed Doughnut',
                        45.0,
                        'dessert',
                        '-',
                        '-',
                        '-'
                    );

INSERT INTO product (
                        product_id,
                        product_name,
                        product_price,
                        product_type,
                        product_sub_type,
                        product_size,
                        product_sweet_level
                    )
                    VALUES (
                        22,
                        'Honey lemon',
                        55.0,
                        'drink',
                        'HCF',
                        'SML',
                        '0123'
                    );

INSERT INTO product (
                        product_id,
                        product_name,
                        product_price,
                        product_type,
                        product_sub_type,
                        product_size,
                        product_sweet_level
                    )
                    VALUES (
                        23,
                        'Peach Tea',
                        60.0,
                        'drink',
                        'HC',
                        'SML',
                        '0123'
                    );

INSERT INTO product (
                        product_id,
                        product_name,
                        product_price,
                        product_type,
                        product_sub_type,
                        product_size,
                        product_sweet_level
                    )
                    VALUES (
                        24,
                        'Black tea',
                        50.0,
                        'drink',
                        'HCF',
                        'SML',
                        '0123'
                    );

INSERT INTO product (
                        product_id,
                        product_name,
                        product_price,
                        product_type,
                        product_sub_type,
                        product_size,
                        product_sweet_level
                    )
                    VALUES (
                        25,
                        'Jasmine tea',
                        45.0,
                        'drink',
                        'HC',
                        'SML',
                        '0123'
                    );

INSERT INTO product (
                        product_id,
                        product_name,
                        product_price,
                        product_type,
                        product_sub_type,
                        product_size,
                        product_sweet_level
                    )
                    VALUES (
                        26,
                        'Pink milk',
                        55.0,
                        'drink',
                        'HCF',
                        'SML',
                        '0123'
                    );

INSERT INTO product (
                        product_id,
                        product_name,
                        product_price,
                        product_type,
                        product_sub_type,
                        product_size,
                        product_sweet_level
                    )
                    VALUES (
                        27,
                        'Taro milk',
                        50.0,
                        'drink',
                        'HC',
                        'SML',
                        '0123'
                    );

INSERT INTO product (
                        product_id,
                        product_name,
                        product_price,
                        product_type,
                        product_sub_type,
                        product_size,
                        product_sweet_level
                    )
                    VALUES (
                        28,
                        'Peach Strawberry ',
                        50.0,
                        'drink',
                        'HCF',
                        'SML',
                        '0123'
                    );

INSERT INTO product (
                        product_id,
                        product_name,
                        product_price,
                        product_type,
                        product_sub_type,
                        product_size,
                        product_sweet_level
                    )
                    VALUES (
                        29,
                        'Mango Dragonfruit',
                        45.0,
                        'drink',
                        'HCF',
                        'SML',
                        '0123'
                    );

INSERT INTO product (
                        product_id,
                        product_name,
                        product_price,
                        product_type,
                        product_sub_type,
                        product_size,
                        product_sweet_level
                    )
                    VALUES (
                        30,
                        'Milo',
                        55.0,
                        'drink',
                        'HCF',
                        'SML',
                        '0123'
                    );


-- Table: promotion
DROP TABLE IF EXISTS promotion;

CREATE TABLE IF NOT EXISTS promotion (
    pr_id         INTEGER   PRIMARY KEY ASC AUTOINCREMENT
                            NOT NULL
                            UNIQUE,
    pr_name       TEXT (50) NOT NULL,
    pr_start_date DATE      NOT NULL
                            DEFAULT ( (date('now', 'localtime') ) ),
    pr_end_date   DATE      NOT NULL
                            DEFAULT ( (date('now', 'localtime') ) ),
    pr_status     TEXT (10) CHECK (pr_status IN ('Active', 'Inactive') ) 
                            NOT NULL
);

INSERT INTO promotion (
                          pr_id,
                          pr_name,
                          pr_start_date,
                          pr_end_date,
                          pr_status
                      )
                      VALUES (
                          1,
                          'โปรโมชันส่วนลดวันเกิด',
                          '2023-02-01',
                          '2023-02-28',
                          'Active'
                      );

INSERT INTO promotion (
                          pr_id,
                          pr_name,
                          pr_start_date,
                          pr_end_date,
                          pr_status
                      )
                      VALUES (
                          2,
                          'ใช้ 10 เเต้ม ลด 1 บาท',
                          '2023-03-01',
                          '2023-03-31',
                          'Active'
                      );


-- Table: promotion_detail
DROP TABLE IF EXISTS promotion_detail;

CREATE TABLE IF NOT EXISTS promotion_detail (
    prd_id                                 PRIMARY KEY
                                           UNIQUE
                                           NOT NULL,
    pr_id                   INTEGER        NOT NULL
                                           REFERENCES promotion (pr_id) ON DELETE CASCADE
                                                                        ON UPDATE CASCADE,
    product_id              INTEGER        NOT NULL
                                           REFERENCES product (product_id) ON DELETE CASCADE
                                                                           ON UPDATE CASCADE,
    prd_discount_percentage DECIMAL (5, 2) NOT NULL
);

INSERT INTO promotion_detail (
                                 prd_id,
                                 pr_id,
                                 product_id,
                                 prd_discount_percentage
                             )
                             VALUES (
                                 1,
                                 1,
                                 1,
                                 100
                             );

INSERT INTO promotion_detail (
                                 prd_id,
                                 pr_id,
                                 product_id,
                                 prd_discount_percentage
                             )
                             VALUES (
                                 2,
                                 2,
                                 2,
                                 50
                             );


-- Table: reciept
DROP TABLE IF EXISTS reciept;

CREATE TABLE IF NOT EXISTS reciept (
    reciept_id   INTEGER   UNIQUE
                           PRIMARY KEY ASC,
    customer_id  INTEGER   REFERENCES customer (customer_id) ON DELETE RESTRICT
                                                             ON UPDATE CASCADE
                           NOT NULL,
    user_id      INTEGER   NOT NULL
                           REFERENCES user (user_id) ON DELETE CASCADE
                                                     ON UPDATE CASCADE,
    store_id     INTEGER   REFERENCES store (store_id) ON DELETE CASCADE
                                                       ON UPDATE CASCADE,
    re_total     REAL,
    re_discount  REAL,
    re_net       REAL,
    re_pay       REAL,
    re_pay_type  TEXT (10) CHECK (re_pay_type IN ('Cash', 'PromptPay', 'Credit') ),
    re_change    REAL,
    re_date      DATETIME  DEFAULT (datetime('now', 'localtime') ),
    re_total_qty INTEGER
);

INSERT INTO reciept (
                        reciept_id,
                        customer_id,
                        user_id,
                        store_id,
                        re_total,
                        re_discount,
                        re_net,
                        re_pay,
                        re_pay_type,
                        re_change,
                        re_date,
                        re_total_qty
                    )
                    VALUES (
                        1,
                        1,
                        1,
                        2,
                        160.0,
                        0.0,
                        160.0,
                        160.0,
                        'Cash',
                        0.0,
                        '2023-10-01 22:18:01',
                        NULL
                    );

INSERT INTO reciept (
                        reciept_id,
                        customer_id,
                        user_id,
                        store_id,
                        re_total,
                        re_discount,
                        re_net,
                        re_pay,
                        re_pay_type,
                        re_change,
                        re_date,
                        re_total_qty
                    )
                    VALUES (
                        2,
                        2,
                        1,
                        2,
                        100.0,
                        0.0,
                        100.0,
                        100.0,
                        'PromptPay',
                        0.0,
                        '2023-10-02 13:42:51',
                        NULL
                    );

INSERT INTO reciept (
                        reciept_id,
                        customer_id,
                        user_id,
                        store_id,
                        re_total,
                        re_discount,
                        re_net,
                        re_pay,
                        re_pay_type,
                        re_change,
                        re_date,
                        re_total_qty
                    )
                    VALUES (
                        3,
-                       1,
-                       1,
                        NULL,
                        50.0,
                        0.0,
                        0.0,
                        0.0,
                        'Cash',
                        0.0,
                        '2023-10-17 14:25:54',
                        1
                    );

INSERT INTO reciept (
                        reciept_id,
                        customer_id,
                        user_id,
                        store_id,
                        re_total,
                        re_discount,
                        re_net,
                        re_pay,
                        re_pay_type,
                        re_change,
                        re_date,
                        re_total_qty
                    )
                    VALUES (
                        4,
-                       1,
-                       1,
                        NULL,
                        45.0,
                        0.0,
                        0.0,
                        0.0,
                        'Cash',
                        0.0,
                        '2023-10-17 14:30:13',
                        1
                    );

INSERT INTO reciept (
                        reciept_id,
                        customer_id,
                        user_id,
                        store_id,
                        re_total,
                        re_discount,
                        re_net,
                        re_pay,
                        re_pay_type,
                        re_change,
                        re_date,
                        re_total_qty
                    )
                    VALUES (
                        5,
-                       1,
-                       1,
                        NULL,
                        50.0,
                        0.0,
                        0.0,
                        0.0,
                        'Cash',
                        0.0,
                        '2023-10-17 14:31:28',
                        1
                    );

INSERT INTO reciept (
                        reciept_id,
                        customer_id,
                        user_id,
                        store_id,
                        re_total,
                        re_discount,
                        re_net,
                        re_pay,
                        re_pay_type,
                        re_change,
                        re_date,
                        re_total_qty
                    )
                    VALUES (
                        6,
-                       1,
-                       1,
                        NULL,
                        0.0,
                        0.0,
                        0.0,
                        0.0,
                        'Cash',
                        0.0,
                        '2023-10-17 14:35:13',
                        3
                    );

INSERT INTO reciept (
                        reciept_id,
                        customer_id,
                        user_id,
                        store_id,
                        re_total,
                        re_discount,
                        re_net,
                        re_pay,
                        re_pay_type,
                        re_change,
                        re_date,
                        re_total_qty
                    )
                    VALUES (
                        7,
-                       1,
-                       1,
                        NULL,
                        0.0,
                        0.0,
                        0.0,
                        0.0,
                        'Cash',
                        0.0,
                        '2023-10-17 15:41:11',
                        1
                    );

INSERT INTO reciept (
                        reciept_id,
                        customer_id,
                        user_id,
                        store_id,
                        re_total,
                        re_discount,
                        re_net,
                        re_pay,
                        re_pay_type,
                        re_change,
                        re_date,
                        re_total_qty
                    )
                    VALUES (
                        8,
-                       1,
-                       1,
                        NULL,
                        45.0,
                        0.0,
                        0.0,
                        0.0,
                        'Cash',
                        0.0,
                        '2023-10-17 15:42:42',
                        1
                    );

INSERT INTO reciept (
                        reciept_id,
                        customer_id,
                        user_id,
                        store_id,
                        re_total,
                        re_discount,
                        re_net,
                        re_pay,
                        re_pay_type,
                        re_change,
                        re_date,
                        re_total_qty
                    )
                    VALUES (
                        9,
-                       1,
-                       1,
                        NULL,
                        50.0,
                        0.0,
                        0.0,
                        0.0,
                        'Cash',
                        0.0,
                        '2023-10-17 15:46:38',
                        1
                    );

INSERT INTO reciept (
                        reciept_id,
                        customer_id,
                        user_id,
                        store_id,
                        re_total,
                        re_discount,
                        re_net,
                        re_pay,
                        re_pay_type,
                        re_change,
                        re_date,
                        re_total_qty
                    )
                    VALUES (
                        10,
-                       1,
-                       1,
                        NULL,
                        110.0,
                        0.0,
                        0.0,
                        0.0,
                        'Cash',
                        0.0,
                        '2023-10-17 15:48:34',
                        2
                    );

INSERT INTO reciept (
                        reciept_id,
                        customer_id,
                        user_id,
                        store_id,
                        re_total,
                        re_discount,
                        re_net,
                        re_pay,
                        re_pay_type,
                        re_change,
                        re_date,
                        re_total_qty
                    )
                    VALUES (
                        11,
-                       1,
-                       1,
                        NULL,
                        110.0,
                        0.0,
                        0.0,
                        0.0,
                        'Cash',
                        0.0,
                        '2023-10-17 15:48:36',
                        2
                    );

INSERT INTO reciept (
                        reciept_id,
                        customer_id,
                        user_id,
                        store_id,
                        re_total,
                        re_discount,
                        re_net,
                        re_pay,
                        re_pay_type,
                        re_change,
                        re_date,
                        re_total_qty
                    )
                    VALUES (
                        12,
-                       1,
-                       1,
                        NULL,
                        110.0,
                        0.0,
                        0.0,
                        0.0,
                        'Cash',
                        0.0,
                        '2023-10-17 15:48:38',
                        2
                    );

INSERT INTO reciept (
                        reciept_id,
                        customer_id,
                        user_id,
                        store_id,
                        re_total,
                        re_discount,
                        re_net,
                        re_pay,
                        re_pay_type,
                        re_change,
                        re_date,
                        re_total_qty
                    )
                    VALUES (
                        13,
-                       1,
-                       1,
                        NULL,
                        110.0,
                        0.0,
                        0.0,
                        0.0,
                        'Cash',
                        0.0,
                        '2023-10-17 15:48:39',
                        2
                    );

INSERT INTO reciept (
                        reciept_id,
                        customer_id,
                        user_id,
                        store_id,
                        re_total,
                        re_discount,
                        re_net,
                        re_pay,
                        re_pay_type,
                        re_change,
                        re_date,
                        re_total_qty
                    )
                    VALUES (
                        14,
-                       1,
-                       1,
                        NULL,
                        110.0,
                        0.0,
                        0.0,
                        0.0,
                        'Cash',
                        0.0,
                        '2023-10-17 15:48:39',
                        2
                    );

INSERT INTO reciept (
                        reciept_id,
                        customer_id,
                        user_id,
                        store_id,
                        re_total,
                        re_discount,
                        re_net,
                        re_pay,
                        re_pay_type,
                        re_change,
                        re_date,
                        re_total_qty
                    )
                    VALUES (
                        15,
-                       1,
-                       1,
                        NULL,
                        110.0,
                        0.0,
                        0.0,
                        0.0,
                        'Cash',
                        0.0,
                        '2023-10-17 15:48:40',
                        2
                    );

INSERT INTO reciept (
                        reciept_id,
                        customer_id,
                        user_id,
                        store_id,
                        re_total,
                        re_discount,
                        re_net,
                        re_pay,
                        re_pay_type,
                        re_change,
                        re_date,
                        re_total_qty
                    )
                    VALUES (
                        16,
-                       1,
-                       1,
                        NULL,
                        110.0,
                        0.0,
                        0.0,
                        0.0,
                        'Cash',
                        0.0,
                        '2023-10-17 15:48:40',
                        2
                    );

INSERT INTO reciept (
                        reciept_id,
                        customer_id,
                        user_id,
                        store_id,
                        re_total,
                        re_discount,
                        re_net,
                        re_pay,
                        re_pay_type,
                        re_change,
                        re_date,
                        re_total_qty
                    )
                    VALUES (
                        17,
-                       1,
-                       1,
                        NULL,
                        45.0,
                        0.0,
                        0.0,
                        0.0,
                        'Cash',
                        0.0,
                        '2023-10-17 15:49:02',
                        1
                    );

INSERT INTO reciept (
                        reciept_id,
                        customer_id,
                        user_id,
                        store_id,
                        re_total,
                        re_discount,
                        re_net,
                        re_pay,
                        re_pay_type,
                        re_change,
                        re_date,
                        re_total_qty
                    )
                    VALUES (
                        18,
-                       1,
-                       1,
                        NULL,
                        100.0,
                        0.0,
                        0.0,
                        0.0,
                        'Cash',
                        0.0,
                        '2023-10-17 15:52:05',
                        2
                    );

INSERT INTO reciept (
                        reciept_id,
                        customer_id,
                        user_id,
                        store_id,
                        re_total,
                        re_discount,
                        re_net,
                        re_pay,
                        re_pay_type,
                        re_change,
                        re_date,
                        re_total_qty
                    )
                    VALUES (
                        19,
-                       1,
-                       1,
                        NULL,
                        60.0,
                        0.0,
                        0.0,
                        0.0,
                        'Cash',
                        0.0,
                        '2023-10-17 15:54:51',
                        1
                    );

INSERT INTO reciept (
                        reciept_id,
                        customer_id,
                        user_id,
                        store_id,
                        re_total,
                        re_discount,
                        re_net,
                        re_pay,
                        re_pay_type,
                        re_change,
                        re_date,
                        re_total_qty
                    )
                    VALUES (
                        20,
-                       1,
-                       1,
                        NULL,
                        100.0,
                        0.0,
                        0.0,
                        0.0,
                        'Cash',
                        0.0,
                        '2023-10-17 15:56:28',
                        2
                    );

INSERT INTO reciept (
                        reciept_id,
                        customer_id,
                        user_id,
                        store_id,
                        re_total,
                        re_discount,
                        re_net,
                        re_pay,
                        re_pay_type,
                        re_change,
                        re_date,
                        re_total_qty
                    )
                    VALUES (
                        21,
-                       1,
-                       1,
                        NULL,
                        50.0,
                        0.0,
                        0.0,
                        0.0,
                        'Cash',
                        0.0,
                        '2023-10-17 15:59:25',
                        1
                    );

INSERT INTO reciept (
                        reciept_id,
                        customer_id,
                        user_id,
                        store_id,
                        re_total,
                        re_discount,
                        re_net,
                        re_pay,
                        re_pay_type,
                        re_change,
                        re_date,
                        re_total_qty
                    )
                    VALUES (
                        22,
-                       1,
-                       1,
                        NULL,
                        50.0,
                        0.0,
                        0.0,
                        0.0,
                        'Cash',
                        0.0,
                        '2023-10-17 15:59:35',
                        1
                    );

INSERT INTO reciept (
                        reciept_id,
                        customer_id,
                        user_id,
                        store_id,
                        re_total,
                        re_discount,
                        re_net,
                        re_pay,
                        re_pay_type,
                        re_change,
                        re_date,
                        re_total_qty
                    )
                    VALUES (
                        23,
-                       1,
-                       1,
                        NULL,
                        55.0,
                        0.0,
                        0.0,
                        0.0,
                        'Cash',
                        0.0,
                        '2023-10-17 15:59:55',
                        1
                    );

INSERT INTO reciept (
                        reciept_id,
                        customer_id,
                        user_id,
                        store_id,
                        re_total,
                        re_discount,
                        re_net,
                        re_pay,
                        re_pay_type,
                        re_change,
                        re_date,
                        re_total_qty
                    )
                    VALUES (
                        24,
-                       1,
-                       1,
                        NULL,
                        45.0,
                        0.0,
                        0.0,
                        0.0,
                        'Cash',
                        0.0,
                        '2023-10-17 16:05:31',
                        1
                    );

INSERT INTO reciept (
                        reciept_id,
                        customer_id,
                        user_id,
                        store_id,
                        re_total,
                        re_discount,
                        re_net,
                        re_pay,
                        re_pay_type,
                        re_change,
                        re_date,
                        re_total_qty
                    )
                    VALUES (
                        25,
-                       1,
-                       1,
                        NULL,
                        150.0,
                        0.0,
                        0.0,
                        0.0,
                        'Cash',
                        0.0,
                        '2023-10-17 16:08:02',
                        3
                    );

INSERT INTO reciept (
                        reciept_id,
                        customer_id,
                        user_id,
                        store_id,
                        re_total,
                        re_discount,
                        re_net,
                        re_pay,
                        re_pay_type,
                        re_change,
                        re_date,
                        re_total_qty
                    )
                    VALUES (
                        26,
-                       1,
-                       1,
                        NULL,
                        60.0,
                        0.0,
                        0.0,
                        0.0,
                        'Cash',
                        0.0,
                        '2023-10-17 16:08:35',
                        1
                    );

INSERT INTO reciept (
                        reciept_id,
                        customer_id,
                        user_id,
                        store_id,
                        re_total,
                        re_discount,
                        re_net,
                        re_pay,
                        re_pay_type,
                        re_change,
                        re_date,
                        re_total_qty
                    )
                    VALUES (
                        27,
-                       1,
-                       1,
                        NULL,
                        60.0,
                        0.0,
                        0.0,
                        0.0,
                        'Cash',
                        0.0,
                        '2023-10-17 16:13:57',
                        1
                    );

INSERT INTO reciept (
                        reciept_id,
                        customer_id,
                        user_id,
                        store_id,
                        re_total,
                        re_discount,
                        re_net,
                        re_pay,
                        re_pay_type,
                        re_change,
                        re_date,
                        re_total_qty
                    )
                    VALUES (
                        28,
-                       1,
-                       1,
                        NULL,
                        60.0,
                        0.0,
                        0.0,
                        0.0,
                        'Cash',
                        0.0,
                        '2023-10-17 16:15:46',
                        1
                    );

INSERT INTO reciept (
                        reciept_id,
                        customer_id,
                        user_id,
                        store_id,
                        re_total,
                        re_discount,
                        re_net,
                        re_pay,
                        re_pay_type,
                        re_change,
                        re_date,
                        re_total_qty
                    )
                    VALUES (
                        29,
-                       1,
-                       1,
                        NULL,
                        110.0,
                        0.0,
                        0.0,
                        0.0,
                        'Cash',
                        0.0,
                        '2023-10-17 16:20:49',
                        2
                    );

INSERT INTO reciept (
                        reciept_id,
                        customer_id,
                        user_id,
                        store_id,
                        re_total,
                        re_discount,
                        re_net,
                        re_pay,
                        re_pay_type,
                        re_change,
                        re_date,
                        re_total_qty
                    )
                    VALUES (
                        30,
-                       1,
-                       1,
                        NULL,
                        110.0,
                        0.0,
                        0.0,
                        0.0,
                        'Cash',
                        0.0,
                        '2023-10-17 16:20:51',
                        2
                    );

INSERT INTO reciept (
                        reciept_id,
                        customer_id,
                        user_id,
                        store_id,
                        re_total,
                        re_discount,
                        re_net,
                        re_pay,
                        re_pay_type,
                        re_change,
                        re_date,
                        re_total_qty
                    )
                    VALUES (
                        31,
-                       1,
-                       1,
                        NULL,
                        60.0,
                        0.0,
                        0.0,
                        0.0,
                        'Cash',
                        0.0,
                        '2023-10-17 16:23:14',
                        1
                    );

INSERT INTO reciept (
                        reciept_id,
                        customer_id,
                        user_id,
                        store_id,
                        re_total,
                        re_discount,
                        re_net,
                        re_pay,
                        re_pay_type,
                        re_change,
                        re_date,
                        re_total_qty
                    )
                    VALUES (
                        32,
                        0,
                        0,
                        NULL,
                        110.0,
                        0.0,
                        0.0,
                        0.0,
                        'Cash',
                        0.0,
                        '2023-10-17 16:24:28',
                        2
                    );

INSERT INTO reciept (
                        reciept_id,
                        customer_id,
                        user_id,
                        store_id,
                        re_total,
                        re_discount,
                        re_net,
                        re_pay,
                        re_pay_type,
                        re_change,
                        re_date,
                        re_total_qty
                    )
                    VALUES (
                        33,
                        3,
                        0,
                        NULL,
                        60.0,
                        0.0,
                        0.0,
                        0.0,
                        'Cash',
                        0.0,
                        '2023-10-17 16:27:15',
                        1
                    );

INSERT INTO reciept (
                        reciept_id,
                        customer_id,
                        user_id,
                        store_id,
                        re_total,
                        re_discount,
                        re_net,
                        re_pay,
                        re_pay_type,
                        re_change,
                        re_date,
                        re_total_qty
                    )
                    VALUES (
                        34,
                        0,
                        0,
                        NULL,
                        55.0,
                        0.0,
                        0.0,
                        0.0,
                        'Cash',
                        0.0,
                        '2023-10-17 16:29:51',
                        1
                    );

INSERT INTO reciept (
                        reciept_id,
                        customer_id,
                        user_id,
                        store_id,
                        re_total,
                        re_discount,
                        re_net,
                        re_pay,
                        re_pay_type,
                        re_change,
                        re_date,
                        re_total_qty
                    )
                    VALUES (
                        35,
                        0,
                        0,
                        NULL,
                        50.0,
                        0.0,
                        0.0,
                        0.0,
                        'Cash',
                        0.0,
                        '2023-10-17 22:28:56',
                        1
                    );

INSERT INTO reciept (
                        reciept_id,
                        customer_id,
                        user_id,
                        store_id,
                        re_total,
                        re_discount,
                        re_net,
                        re_pay,
                        re_pay_type,
                        re_change,
                        re_date,
                        re_total_qty
                    )
                    VALUES (
                        36,
                        0,
                        0,
                        NULL,
                        50.0,
                        0.0,
                        0.0,
                        0.0,
                        'Cash',
                        0.0,
                        '2023-10-19 13:04:24',
                        1
                    );

INSERT INTO reciept (
                        reciept_id,
                        customer_id,
                        user_id,
                        store_id,
                        re_total,
                        re_discount,
                        re_net,
                        re_pay,
                        re_pay_type,
                        re_change,
                        re_date,
                        re_total_qty
                    )
                    VALUES (
                        37,
                        0,
                        0,
                        NULL,
                        50.0,
                        0.0,
                        0.0,
                        0.0,
                        'Cash',
                        0.0,
                        '2023-10-19 13:48:35',
                        1
                    );

INSERT INTO reciept (
                        reciept_id,
                        customer_id,
                        user_id,
                        store_id,
                        re_total,
                        re_discount,
                        re_net,
                        re_pay,
                        re_pay_type,
                        re_change,
                        re_date,
                        re_total_qty
                    )
                    VALUES (
                        38,
                        0,
                        0,
                        NULL,
                        50.0,
                        0.0,
                        0.0,
                        0.0,
                        'Cash',
                        0.0,
                        '2023-10-19 14:00:40',
                        1
                    );

INSERT INTO reciept (
                        reciept_id,
                        customer_id,
                        user_id,
                        store_id,
                        re_total,
                        re_discount,
                        re_net,
                        re_pay,
                        re_pay_type,
                        re_change,
                        re_date,
                        re_total_qty
                    )
                    VALUES (
                        39,
                        0,
                        0,
                        NULL,
                        50.0,
                        0.0,
                        0.0,
                        0.0,
                        'Cash',
                        0.0,
                        '2023-10-19 16:00:42',
                        1
                    );

INSERT INTO reciept (
                        reciept_id,
                        customer_id,
                        user_id,
                        store_id,
                        re_total,
                        re_discount,
                        re_net,
                        re_pay,
                        re_pay_type,
                        re_change,
                        re_date,
                        re_total_qty
                    )
                    VALUES (
                        40,
                        0,
                        0,
                        NULL,
                        50.0,
                        0.0,
                        0.0,
                        0.0,
                        'Cash',
                        0.0,
                        '2023-10-19 16:00:47',
                        1
                    );

INSERT INTO reciept (
                        reciept_id,
                        customer_id,
                        user_id,
                        store_id,
                        re_total,
                        re_discount,
                        re_net,
                        re_pay,
                        re_pay_type,
                        re_change,
                        re_date,
                        re_total_qty
                    )
                    VALUES (
                        41,
                        0,
                        0,
                        NULL,
                        50.0,
                        0.0,
                        0.0,
                        0.0,
                        'Cash',
                        0.0,
                        '2023-10-19 16:00:47',
                        1
                    );

INSERT INTO reciept (
                        reciept_id,
                        customer_id,
                        user_id,
                        store_id,
                        re_total,
                        re_discount,
                        re_net,
                        re_pay,
                        re_pay_type,
                        re_change,
                        re_date,
                        re_total_qty
                    )
                    VALUES (
                        42,
                        0,
                        0,
                        NULL,
                        50.0,
                        0.0,
                        0.0,
                        0.0,
                        'Cash',
                        0.0,
                        '2023-10-19 16:00:47',
                        1
                    );

INSERT INTO reciept (
                        reciept_id,
                        customer_id,
                        user_id,
                        store_id,
                        re_total,
                        re_discount,
                        re_net,
                        re_pay,
                        re_pay_type,
                        re_change,
                        re_date,
                        re_total_qty
                    )
                    VALUES (
                        43,
                        0,
                        0,
                        NULL,
                        50.0,
                        0.0,
                        0.0,
                        0.0,
                        'Cash',
                        0.0,
                        '2023-10-19 16:00:47',
                        1
                    );

INSERT INTO reciept (
                        reciept_id,
                        customer_id,
                        user_id,
                        store_id,
                        re_total,
                        re_discount,
                        re_net,
                        re_pay,
                        re_pay_type,
                        re_change,
                        re_date,
                        re_total_qty
                    )
                    VALUES (
                        44,
                        0,
                        0,
                        NULL,
                        50.0,
                        0.0,
                        0.0,
                        0.0,
                        'Cash',
                        0.0,
                        '2023-10-19 16:00:48',
                        1
                    );

INSERT INTO reciept (
                        reciept_id,
                        customer_id,
                        user_id,
                        store_id,
                        re_total,
                        re_discount,
                        re_net,
                        re_pay,
                        re_pay_type,
                        re_change,
                        re_date,
                        re_total_qty
                    )
                    VALUES (
                        45,
                        0,
                        0,
                        NULL,
                        60.0,
                        0.0,
                        0.0,
                        0.0,
                        'Cash',
                        0.0,
                        '2023-10-20 12:05:34',
                        1
                    );

INSERT INTO reciept (
                        reciept_id,
                        customer_id,
                        user_id,
                        store_id,
                        re_total,
                        re_discount,
                        re_net,
                        re_pay,
                        re_pay_type,
                        re_change,
                        re_date,
                        re_total_qty
                    )
                    VALUES (
                        46,
                        0,
                        0,
                        NULL,
                        50.0,
                        0.0,
                        0.0,
                        0.0,
                        'Cash',
                        0.0,
                        '2023-10-20 12:16:07',
                        1
                    );

INSERT INTO reciept (
                        reciept_id,
                        customer_id,
                        user_id,
                        store_id,
                        re_total,
                        re_discount,
                        re_net,
                        re_pay,
                        re_pay_type,
                        re_change,
                        re_date,
                        re_total_qty
                    )
                    VALUES (
                        47,
                        0,
                        0,
                        NULL,
                        50.0,
                        0.0,
                        0.0,
                        0.0,
                        'Cash',
                        0.0,
                        '2023-10-20 12:16:15',
                        1
                    );

INSERT INTO reciept (
                        reciept_id,
                        customer_id,
                        user_id,
                        store_id,
                        re_total,
                        re_discount,
                        re_net,
                        re_pay,
                        re_pay_type,
                        re_change,
                        re_date,
                        re_total_qty
                    )
                    VALUES (
                        48,
                        0,
                        0,
                        NULL,
                        50.0,
                        0.0,
                        0.0,
                        0.0,
                        'Cash',
                        0.0,
                        '2023-10-20 12:16:15',
                        1
                    );

INSERT INTO reciept (
                        reciept_id,
                        customer_id,
                        user_id,
                        store_id,
                        re_total,
                        re_discount,
                        re_net,
                        re_pay,
                        re_pay_type,
                        re_change,
                        re_date,
                        re_total_qty
                    )
                    VALUES (
                        49,
                        0,
                        0,
                        NULL,
                        50.0,
                        0.0,
                        0.0,
                        0.0,
                        'Cash',
                        0.0,
                        '2023-10-20 12:16:15',
                        1
                    );

INSERT INTO reciept (
                        reciept_id,
                        customer_id,
                        user_id,
                        store_id,
                        re_total,
                        re_discount,
                        re_net,
                        re_pay,
                        re_pay_type,
                        re_change,
                        re_date,
                        re_total_qty
                    )
                    VALUES (
                        50,
                        0,
                        0,
                        NULL,
                        50.0,
                        0.0,
                        0.0,
                        0.0,
                        'Cash',
                        0.0,
                        '2023-10-20 15:51:58',
                        1
                    );


-- Table: reciept_detail
DROP TABLE IF EXISTS reciept_detail;

CREATE TABLE IF NOT EXISTS reciept_detail (
    reciept_detail_id INTEGER PRIMARY KEY ASC AUTOINCREMENT
                              UNIQUE,
    reciept_id        INTEGER REFERENCES reciept (reciept_id) ON DELETE CASCADE
                                                              ON UPDATE CASCADE,
    product_id        INTEGER REFERENCES product (product_id) ON DELETE RESTRICT
                                                              ON UPDATE CASCADE,
    pd_id             INTEGER REFERENCES promotion_detail (prd_id) ON DELETE CASCADE
                                                                   ON UPDATE CASCADE,
    qty               INTEGER,
    product_price     REAL,
    total_price       REAL,
    rd_discount       REAL,
    rd_net            REAL,
    product_name      TEXT    REFERENCES product (product_id) ON DELETE CASCADE
                                                              ON UPDATE CASCADE,
    product_size      TEXT    REFERENCES product (product_id) ON DELETE CASCADE
                                                              ON UPDATE CASCADE
);

INSERT INTO reciept_detail (
                               reciept_detail_id,
                               reciept_id,
                               product_id,
                               pd_id,
                               qty,
                               product_price,
                               total_price,
                               rd_discount,
                               rd_net,
                               product_name,
                               product_size
                           )
                           VALUES (
                               1,
                               1,
                               1,
                               1,
                               2,
                               30.0,
                               60.0,
                               0.0,
                               60.0,
                               NULL,
                               NULL
                           );

INSERT INTO reciept_detail (
                               reciept_detail_id,
                               reciept_id,
                               product_id,
                               pd_id,
                               qty,
                               product_price,
                               total_price,
                               rd_discount,
                               rd_net,
                               product_name,
                               product_size
                           )
                           VALUES (
                               2,
                               1,
                               4,
                               1,
                               1,
                               60.0,
                               60.0,
                               0.0,
                               60.0,
                               NULL,
                               NULL
                           );

INSERT INTO reciept_detail (
                               reciept_detail_id,
                               reciept_id,
                               product_id,
                               pd_id,
                               qty,
                               product_price,
                               total_price,
                               rd_discount,
                               rd_net,
                               product_name,
                               product_size
                           )
                           VALUES (
                               3,
                               1,
                               2,
                               1,
                               1,
                               40.0,
                               40.0,
                               0.0,
                               40.0,
                               NULL,
                               NULL
                           );

INSERT INTO reciept_detail (
                               reciept_detail_id,
                               reciept_id,
                               product_id,
                               pd_id,
                               qty,
                               product_price,
                               total_price,
                               rd_discount,
                               rd_net,
                               product_name,
                               product_size
                           )
                           VALUES (
                               4,
                               36,
                               11,
                               NULL,
                               1,
                               50.0,
                               50.0,
                               NULL,
                               NULL,
                               'Baked Apple Croissant',
                               ''
                           );

INSERT INTO reciept_detail (
                               reciept_detail_id,
                               reciept_id,
                               product_id,
                               pd_id,
                               qty,
                               product_price,
                               total_price,
                               rd_discount,
                               rd_net,
                               product_name,
                               product_size
                           )
                           VALUES (
                               5,
                               37,
                               24,
                               NULL,
                               1,
                               50.0,
                               50.0,
                               NULL,
                               NULL,
                               'Black tea',
                               'S'
                           );

INSERT INTO reciept_detail (
                               reciept_detail_id,
                               reciept_id,
                               product_id,
                               pd_id,
                               qty,
                               product_price,
                               total_price,
                               rd_discount,
                               rd_net,
                               product_name,
                               product_size
                           )
                           VALUES (
                               6,
                               38,
                               11,
                               NULL,
                               1,
                               50.0,
                               50.0,
                               NULL,
                               NULL,
                               'Baked Apple Croissant',
                               ''
                           );

INSERT INTO reciept_detail (
                               reciept_detail_id,
                               reciept_id,
                               product_id,
                               pd_id,
                               qty,
                               product_price,
                               total_price,
                               rd_discount,
                               rd_net,
                               product_name,
                               product_size
                           )
                           VALUES (
                               7,
                               39,
                               11,
                               NULL,
                               1,
                               50.0,
                               50.0,
                               NULL,
                               NULL,
                               'Baked Apple Croissant',
                               ''
                           );

INSERT INTO reciept_detail (
                               reciept_detail_id,
                               reciept_id,
                               product_id,
                               pd_id,
                               qty,
                               product_price,
                               total_price,
                               rd_discount,
                               rd_net,
                               product_name,
                               product_size
                           )
                           VALUES (
                               8,
                               40,
                               11,
                               NULL,
                               1,
                               50.0,
                               50.0,
                               NULL,
                               NULL,
                               'Baked Apple Croissant',
                               ''
                           );

INSERT INTO reciept_detail (
                               reciept_detail_id,
                               reciept_id,
                               product_id,
                               pd_id,
                               qty,
                               product_price,
                               total_price,
                               rd_discount,
                               rd_net,
                               product_name,
                               product_size
                           )
                           VALUES (
                               9,
                               41,
                               11,
                               NULL,
                               1,
                               50.0,
                               50.0,
                               NULL,
                               NULL,
                               'Baked Apple Croissant',
                               ''
                           );

INSERT INTO reciept_detail (
                               reciept_detail_id,
                               reciept_id,
                               product_id,
                               pd_id,
                               qty,
                               product_price,
                               total_price,
                               rd_discount,
                               rd_net,
                               product_name,
                               product_size
                           )
                           VALUES (
                               10,
                               42,
                               11,
                               NULL,
                               1,
                               50.0,
                               50.0,
                               NULL,
                               NULL,
                               'Baked Apple Croissant',
                               ''
                           );

INSERT INTO reciept_detail (
                               reciept_detail_id,
                               reciept_id,
                               product_id,
                               pd_id,
                               qty,
                               product_price,
                               total_price,
                               rd_discount,
                               rd_net,
                               product_name,
                               product_size
                           )
                           VALUES (
                               11,
                               43,
                               11,
                               NULL,
                               1,
                               50.0,
                               50.0,
                               NULL,
                               NULL,
                               'Baked Apple Croissant',
                               ''
                           );

INSERT INTO reciept_detail (
                               reciept_detail_id,
                               reciept_id,
                               product_id,
                               pd_id,
                               qty,
                               product_price,
                               total_price,
                               rd_discount,
                               rd_net,
                               product_name,
                               product_size
                           )
                           VALUES (
                               12,
                               44,
                               11,
                               NULL,
                               1,
                               50.0,
                               50.0,
                               NULL,
                               NULL,
                               'Baked Apple Croissant',
                               ''
                           );

INSERT INTO reciept_detail (
                               reciept_detail_id,
                               reciept_id,
                               product_id,
                               pd_id,
                               qty,
                               product_price,
                               total_price,
                               rd_discount,
                               rd_net,
                               product_name,
                               product_size
                           )
                           VALUES (
                               13,
                               45,
                               24,
                               NULL,
                               1,
                               60.0,
                               60.0,
                               NULL,
                               NULL,
                               'Black tea',
                               'L'
                           );

INSERT INTO reciept_detail (
                               reciept_detail_id,
                               reciept_id,
                               product_id,
                               pd_id,
                               qty,
                               product_price,
                               total_price,
                               rd_discount,
                               rd_net,
                               product_name,
                               product_size
                           )
                           VALUES (
                               14,
                               46,
                               11,
                               NULL,
                               1,
                               50.0,
                               50.0,
                               NULL,
                               NULL,
                               'Baked Apple Croissant',
                               ''
                           );

INSERT INTO reciept_detail (
                               reciept_detail_id,
                               reciept_id,
                               product_id,
                               pd_id,
                               qty,
                               product_price,
                               total_price,
                               rd_discount,
                               rd_net,
                               product_name,
                               product_size
                           )
                           VALUES (
                               15,
                               47,
                               11,
                               NULL,
                               1,
                               50.0,
                               50.0,
                               NULL,
                               NULL,
                               'Baked Apple Croissant',
                               ''
                           );

INSERT INTO reciept_detail (
                               reciept_detail_id,
                               reciept_id,
                               product_id,
                               pd_id,
                               qty,
                               product_price,
                               total_price,
                               rd_discount,
                               rd_net,
                               product_name,
                               product_size
                           )
                           VALUES (
                               16,
                               48,
                               11,
                               NULL,
                               1,
                               50.0,
                               50.0,
                               NULL,
                               NULL,
                               'Baked Apple Croissant',
                               ''
                           );

INSERT INTO reciept_detail (
                               reciept_detail_id,
                               reciept_id,
                               product_id,
                               pd_id,
                               qty,
                               product_price,
                               total_price,
                               rd_discount,
                               rd_net,
                               product_name,
                               product_size
                           )
                           VALUES (
                               17,
                               49,
                               11,
                               NULL,
                               1,
                               50.0,
                               50.0,
                               NULL,
                               NULL,
                               'Baked Apple Croissant',
                               ''
                           );

INSERT INTO reciept_detail (
                               reciept_detail_id,
                               reciept_id,
                               product_id,
                               pd_id,
                               qty,
                               product_price,
                               total_price,
                               rd_discount,
                               rd_net,
                               product_name,
                               product_size
                           )
                           VALUES (
                               18,
                               50,
                               11,
                               NULL,
                               1,
                               50.0,
                               50.0,
                               NULL,
                               NULL,
                               'Baked Apple Croissant',
                               ''
                           );


-- Table: store
DROP TABLE IF EXISTS store;

CREATE TABLE IF NOT EXISTS store (
    store_id     INTEGER   PRIMARY KEY
                           UNIQUE
                           NOT NULL,
    store_name   TEXT (50) NOT NULL,
    store_tel    TEXT (10) NOT NULL,
    store_addres TEXT (50) NOT NULL
);

INSERT INTO store (
                      store_id,
                      store_name,
                      store_tel,
                      store_addres
                  )
                  VALUES (
                      1,
                      'cafe1',
                      '0898956220',
                      '900 ต.บางบัว อ.บางเตย จ.บางแสน'
                  );

INSERT INTO store (
                      store_id,
                      store_name,
                      store_tel,
                      store_addres
                  )
                  VALUES (
                      2,
                      'cafe2',
                      '0898956225',
                      '901 ต.บางบัว อ.บางเตย จ.บางแสน'
                  );

INSERT INTO store (
                      store_id,
                      store_name,
                      store_tel,
                      store_addres
                  )
                  VALUES (
                      3,
                      'cafe3',
                      '0898956226',
                      '902 ต.บางบัว อ.บางเตย จ.บางแสน'
                  );

INSERT INTO store (
                      store_id,
                      store_name,
                      store_tel,
                      store_addres
                  )
                  VALUES (
                      4,
                      'cafe4',
                      '0898956227',
                      '903 ต.บางบัว อ.บางเตย จ.บางแสน'
                  );

INSERT INTO store (
                      store_id,
                      store_name,
                      store_tel,
                      store_addres
                  )
                  VALUES (
                      5,
                      'cafe5',
                      '0898956228',
                      '904 ต.บางบัว อ.บางเตย จ.บางแสน'
                  );


-- Table: supplier
DROP TABLE IF EXISTS supplier;

CREATE TABLE IF NOT EXISTS supplier (
    supplier_id            INTEGER   PRIMARY KEY ASC AUTOINCREMENT
                                     NOT NULL
                                     UNIQUE,
    supplier_name          TEXT (50) NOT NULL,
    supplier_contact_name  TEXT (50) NOT NULL,
    supplier_contact_email TEXT (50) NOT NULL,
    supplier_contact_phone TEXT (20) NOT NULL,
    supplier_address       TEXT (50) NOT NULL
);

INSERT INTO supplier (
                         supplier_id,
                         supplier_name,
                         supplier_contact_name,
                         supplier_contact_email,
                         supplier_contact_phone,
                         supplier_address
                     )
                     VALUES (
                         1,
                         'บริษัท สรรพสินค้าจำกัด',
                         'นายสมชาย',
                         'somchai@example.com',
                         '02-123-4567',
                         '123 ถนนสุขุมวิท, กรุงเทพ'
                     );

INSERT INTO supplier (
                         supplier_id,
                         supplier_name,
                         supplier_contact_name,
                         supplier_contact_email,
                         supplier_contact_phone,
                         supplier_address
                     )
                     VALUES (
                         2,
                         'โรงนมในพื้นที่',
                         'นายวิชัย',
                         'wichai@example.com',
                         '02-987-6543',
                         '456 ถนนราชดำเนิน, กรุงเทพ'
                     );

INSERT INTO supplier (
                         supplier_id,
                         supplier_name,
                         supplier_contact_name,
                         supplier_contact_email,
                         supplier_contact_phone,
                         supplier_address
                     )
                     VALUES (
                         3,
                         'บริษัท น้ำตาลอุดร',
                         'นายอุดร',
                         'udon@example.com',
                         '042-123-7890',
                         '789 ถนนอุดรธานี, อุดรธานี'
                     );


-- Table: user
DROP TABLE IF EXISTS user;

CREATE TABLE IF NOT EXISTS user (
    user_id         INTEGER    PRIMARY KEY ASC AUTOINCREMENT
                               NOT NULL
                               UNIQUE,
    user_first_name TEXT (50)  NOT NULL,
    user_last_name  TEXT (50)  NOT NULL,
    user_gender     TEXT (1)   NOT NULL
                               CHECK (user_gender IN ('M', 'F') ),
    user_role       TEXT (10)  NOT NULL
                               CHECK (user_role IN ('Manager', 'Employee') ),
    user_email      TEXT (50)  UNIQUE
                               NOT NULL,
    user_addres     TEXT (150) NOT NULL,
    user_status     TEXT (10)  NOT NULL
                               CHECK (user_status IN ('Work', 'Rest') ),
    total_salary    INTEGER    NOT NULL,
    user_login      TEXT (50)  UNIQUE
                               NOT NULL,
    user_password   TEXT (50)  NOT NULL
);

INSERT INTO user (
                     user_id,
                     user_first_name,
                     user_last_name,
                     user_gender,
                     user_role,
                     user_email,
                     user_addres,
                     user_status,
                     total_salary,
                     user_login,
                     user_password
                 )
                 VALUES (
                     1,
                     'John',
                     'Doe',
                     'M',
                     'Manager',
                     'JohnDoe',
                     '899 ต.บางบัว อ.บางเตย จ.บางแสน',
                     'Work',
                     405,
                     'user1',
                     'password1'
                 );

INSERT INTO user (
                     user_id,
                     user_first_name,
                     user_last_name,
                     user_gender,
                     user_role,
                     user_email,
                     user_addres,
                     user_status,
                     total_salary,
                     user_login,
                     user_password
                 )
                 VALUES (
                     2,
                     'Alice',
                     'Smith',
                     'F',
                     'Manager',
                     'AliceSmith',
                     '899 ต.บางบัว อ.บางเตย จ.บางแสน',
                     'Work',
                     405,
                     'user2',
                     'password2'
                 );

INSERT INTO user (
                     user_id,
                     user_first_name,
                     user_last_name,
                     user_gender,
                     user_role,
                     user_email,
                     user_addres,
                     user_status,
                     total_salary,
                     user_login,
                     user_password
                 )
                 VALUES (
                     3,
                     'Bob',
                     'Johnson',
                     'M',
                     'Manager',
                     'BobJohnson',
                     '899 ต.บางบัว อ.บางเตย จ.บางแสน',
                     'Work',
                     405,
                     'user3',
                     'password3'
                 );

INSERT INTO user (
                     user_id,
                     user_first_name,
                     user_last_name,
                     user_gender,
                     user_role,
                     user_email,
                     user_addres,
                     user_status,
                     total_salary,
                     user_login,
                     user_password
                 )
                 VALUES (
                     4,
                     'Sarah',
                     'Williams',
                     'F',
                     'Manager',
                     'SarahWilliams',
                     '899 ต.บางบัว อ.บางเตย จ.บางแสน',
                     'Work',
                     405,
                     'user4',
                     'password4'
                 );

INSERT INTO user (
                     user_id,
                     user_first_name,
                     user_last_name,
                     user_gender,
                     user_role,
                     user_email,
                     user_addres,
                     user_status,
                     total_salary,
                     user_login,
                     user_password
                 )
                 VALUES (
                     5,
                     'Michael',
                     'Brown',
                     'M',
                     'Employee',
                     'MichaelBrown',
                     '899 ต.บางบัว อ.บางเตย จ.บางแสน',
                     'Work',
                     405,
                     'user5',
                     'password5'
                 );

INSERT INTO user (
                     user_id,
                     user_first_name,
                     user_last_name,
                     user_gender,
                     user_role,
                     user_email,
                     user_addres,
                     user_status,
                     total_salary,
                     user_login,
                     user_password
                 )
                 VALUES (
                     6,
                     'Emily',
                     'Davis',
                     'F',
                     'Manager',
                     'EmilyDavis',
                     '899 ต.บางบัว อ.บางเตย จ.บางแสน',
                     'Work',
                     405,
                     'user6',
                     'password6'
                 );

INSERT INTO user (
                     user_id,
                     user_first_name,
                     user_last_name,
                     user_gender,
                     user_role,
                     user_email,
                     user_addres,
                     user_status,
                     total_salary,
                     user_login,
                     user_password
                 )
                 VALUES (
                     7,
                     'David',
                     'Miller',
                     'M',
                     'Manager',
                     'DavidMiller',
                     '899 ต.บางบัว อ.บางเตย จ.บางแสน',
                     'Work',
                     405,
                     'user7',
                     'password7'
                 );

INSERT INTO user (
                     user_id,
                     user_first_name,
                     user_last_name,
                     user_gender,
                     user_role,
                     user_email,
                     user_addres,
                     user_status,
                     total_salary,
                     user_login,
                     user_password
                 )
                 VALUES (
                     8,
                     'Olivia',
                     'Lee',
                     'F',
                     'Manager',
                     'OliviaLee',
                     '899 ต.บางบัว อ.บางเตย จ.บางแสน',
                     'Rest',
                     405,
                     'user8',
                     'password8'
                 );

INSERT INTO user (
                     user_id,
                     user_first_name,
                     user_last_name,
                     user_gender,
                     user_role,
                     user_email,
                     user_addres,
                     user_status,
                     total_salary,
                     user_login,
                     user_password
                 )
                 VALUES (
                     9,
                     'James',
                     'Wilson',
                     'M',
                     'Employee',
                     'JamesWilson',
                     '899 ต.บางบัว อ.บางเตย จ.บางแสน',
                     'Work',
                     405,
                     'user9',
                     'password9'
                 );

INSERT INTO user (
                     user_id,
                     user_first_name,
                     user_last_name,
                     user_gender,
                     user_role,
                     user_email,
                     user_addres,
                     user_status,
                     total_salary,
                     user_login,
                     user_password
                 )
                 VALUES (
                     10,
                     'Ava',
                     'Anderson',
                     'F',
                     'Manager',
                     'AvaAnderson',
                     '899 ต.บางบัว อ.บางเตย จ.บางแสน',
                     'Work',
                     405,
                     'user10',
                     'password10'
                 );

INSERT INTO user (
                     user_id,
                     user_first_name,
                     user_last_name,
                     user_gender,
                     user_role,
                     user_email,
                     user_addres,
                     user_status,
                     total_salary,
                     user_login,
                     user_password
                 )
                 VALUES (
                     11,
                     'Ethan',
                     'Thomas',
                     'M',
                     'Manager',
                     'EthanThomas',
                     '899 ต.บางบัว อ.บางเตย จ.บางแสน',
                     'Work',
                     405,
                     'user11',
                     'password11'
                 );

INSERT INTO user (
                     user_id,
                     user_first_name,
                     user_last_name,
                     user_gender,
                     user_role,
                     user_email,
                     user_addres,
                     user_status,
                     total_salary,
                     user_login,
                     user_password
                 )
                 VALUES (
                     12,
                     'Sophia',
                     'Harris',
                     'F',
                     'Manager',
                     'SophiaHarris',
                     '899 ต.บางบัว อ.บางเตย จ.บางแสน',
                     'Work',
                     405,
                     'user12',
                     'password12'
                 );

INSERT INTO user (
                     user_id,
                     user_first_name,
                     user_last_name,
                     user_gender,
                     user_role,
                     user_email,
                     user_addres,
                     user_status,
                     total_salary,
                     user_login,
                     user_password
                 )
                 VALUES (
                     13,
                     'Liam',
                     'White',
                     'M',
                     'Manager',
                     'LiamWhite',
                     '899 ต.บางบัว อ.บางเตย จ.บางแสน',
                     'Work',
                     405,
                     'user13',
                     'password13'
                 );

INSERT INTO user (
                     user_id,
                     user_first_name,
                     user_last_name,
                     user_gender,
                     user_role,
                     user_email,
                     user_addres,
                     user_status,
                     total_salary,
                     user_login,
                     user_password
                 )
                 VALUES (
                     14,
                     'Mia',
                     'Walker',
                     'F',
                     'Manager',
                     'MiaWalker',
                     '899 ต.บางบัว อ.บางเตย จ.บางแสน',
                     'Work',
                     405,
                     'user14',
                     'password14'
                 );

INSERT INTO user (
                     user_id,
                     user_first_name,
                     user_last_name,
                     user_gender,
                     user_role,
                     user_email,
                     user_addres,
                     user_status,
                     total_salary,
                     user_login,
                     user_password
                 )
                 VALUES (
                     15,
                     'Noah',
                     'Green',
                     'M',
                     'Manager',
                     'NoahGreen',
                     '899 ต.บางบัว อ.บางเตย จ.บางแสน',
                     'Work',
                     405,
                     'user15',
                     'password15'
                 );

INSERT INTO user (
                     user_id,
                     user_first_name,
                     user_last_name,
                     user_gender,
                     user_role,
                     user_email,
                     user_addres,
                     user_status,
                     total_salary,
                     user_login,
                     user_password
                 )
                 VALUES (
                     16,
                     'Noah',
                     'Green',
                     'M',
                     'Employee',
                     '1',
                     '899 ต.บางบัว อ.บางเตย จ.บางแสน',
                     'Work',
                     405,
                     '1',
                     '1'
                 );


-- Table: working_hours
DROP TABLE IF EXISTS working_hours;

CREATE TABLE IF NOT EXISTS working_hours (
    wh_id          INTEGER PRIMARY KEY ASC
                           NOT NULL
                           UNIQUE,
    user_id        INTEGER NOT NULL,
    wh_login_Date  DATE    DEFAULT ( (date('now', 'localtime') ) ),
    wh_login_time  DATE    DEFAULT ( (time('now', 'localtime') ) ),
    wh_logout_time DATE    DEFAULT ( (time('now', 'localtime') ) ),
    wh_total_time  INTEGER,
    FOREIGN KEY (
        user_id
    )
    REFERENCES user (user_id) 
);

INSERT INTO working_hours (
                              wh_id,
                              user_id,
                              wh_login_Date,
                              wh_login_time,
                              wh_logout_time,
                              wh_total_time
                          )
                          VALUES (
                              1,
                              1,
                              '2023-10-01 13:42:52',
                              '2023-10-02 08:00:00',
                              '2023-10-02 17:00:00',
                              9
                          );

INSERT INTO working_hours (
                              wh_id,
                              user_id,
                              wh_login_Date,
                              wh_login_time,
                              wh_logout_time,
                              wh_total_time
                          )
                          VALUES (
                              2,
                              2,
                              '2023-10-02 13:42:52',
                              '2023-10-02 08:00:00',
                              '2023-10-02 17:00:00',
                              9
                          );

INSERT INTO working_hours (
                              wh_id,
                              user_id,
                              wh_login_Date,
                              wh_login_time,
                              wh_logout_time,
                              wh_total_time
                          )
                          VALUES (
                              3,
                              3,
                              '2023-10-02 13:42:52',
                              '2023-10-02 08:00:00',
                              '2023-10-02 17:00:00',
                              9
                          );

INSERT INTO working_hours (
                              wh_id,
                              user_id,
                              wh_login_Date,
                              wh_login_time,
                              wh_logout_time,
                              wh_total_time
                          )
                          VALUES (
                              4,
                              4,
                              '2023-10-02 13:42:52',
                              '2023-10-02 08:00:00',
                              '2023-10-02 17:00:00',
                              9
                          );

INSERT INTO working_hours (
                              wh_id,
                              user_id,
                              wh_login_Date,
                              wh_login_time,
                              wh_logout_time,
                              wh_total_time
                          )
                          VALUES (
                              5,
                              5,
                              '2023-10-01 13:42:52',
                              '2023-10-02 08:00:00',
                              '2023-10-02 17:00:00',
                              9
                          );

INSERT INTO working_hours (
                              wh_id,
                              user_id,
                              wh_login_Date,
                              wh_login_time,
                              wh_logout_time,
                              wh_total_time
                          )
                          VALUES (
                              6,
                              6,
                              '2023-10-01 13:42:52',
                              '2023-10-02 08:00:00',
                              '2023-10-02 17:00:00',
                              9
                          );

INSERT INTO working_hours (
                              wh_id,
                              user_id,
                              wh_login_Date,
                              wh_login_time,
                              wh_logout_time,
                              wh_total_time
                          )
                          VALUES (
                              7,
                              7,
                              '2023-10-01 13:42:52',
                              '2023-10-02 08:00:00',
                              '2023-10-02 17:00:00',
                              9
                          );

INSERT INTO working_hours (
                              wh_id,
                              user_id,
                              wh_login_Date,
                              wh_login_time,
                              wh_logout_time,
                              wh_total_time
                          )
                          VALUES (
                              8,
                              8,
                              '2023-10-01 13:42:52',
                              '2023-10-02 08:00:00',
                              '2023-10-02 17:00:00',
                              9
                          );

INSERT INTO working_hours (
                              wh_id,
                              user_id,
                              wh_login_Date,
                              wh_login_time,
                              wh_logout_time,
                              wh_total_time
                          )
                          VALUES (
                              9,
                              9,
                              '2023-10-01 13:42:52',
                              '2023-10-02 08:00:00',
                              '2023-10-02 18:00:00',
                              10
                          );

INSERT INTO working_hours (
                              wh_id,
                              user_id,
                              wh_login_Date,
                              wh_login_time,
                              wh_logout_time,
                              wh_total_time
                          )
                          VALUES (
                              10,
                              16,
                              '2023-10-21',
                              '23:55:40',
                              '23:55:43',
                              0
                          );


COMMIT TRANSACTION;
PRAGMA foreign_keys = on;
