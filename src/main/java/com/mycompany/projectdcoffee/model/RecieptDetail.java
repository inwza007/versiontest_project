/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.projectdcoffee.model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author werapan
 */
public class RecieptDetail {

    private int recieptDetaiId;
    private int productId;
    private String productName;
    private float productPrice;
    private int qty;
    private float totalPrice;
    private int recieptId;
    private String productSize;
    private String productSweet;
    private String productSubType;
    private Date re_date;
    private int product_qty_day;
    private int promotion_id;

    public RecieptDetail(int recieptDetaiId, int productId, String productName, float productPrice, int qty, float totalPrice, int recieptId, String productSize, String productSweet, String productType, int promotion_id) {
        this.recieptDetaiId = recieptDetaiId;
        this.productId = productId;
        this.productName = productName;
        this.productPrice = productPrice;
        this.qty = qty;
        this.totalPrice = totalPrice;
        this.recieptId = recieptId;
        this.productSize = productSize;
        this.productSweet = productSweet;
        this.productSubType = productType;
        this.promotion_id = promotion_id;
    }

    public RecieptDetail(int productId, String productName, float productPrice, int qty, float totalPrice, int recieptId, String productSize, String productSweet, String productType, int promotion_id) {
        this.recieptDetaiId = -1;
        this.productId = productId;
        this.productName = productName;
        this.productPrice = productPrice;
        this.qty = qty;
        this.totalPrice = totalPrice;
        this.recieptId = recieptId;
        this.productSize = productSize;
        this.productSweet = productSweet;
        this.productSubType = productType;
        this.promotion_id = promotion_id;
    }

    public RecieptDetail(int recieptDetaiId, int productId, String productName, float productPrice, int qty, float totalPrice, int recieptId, int promotion_id) {
        this.recieptDetaiId = recieptDetaiId;
        this.productId = productId;
        this.productName = productName;
        this.productPrice = productPrice;
        this.qty = qty;
        this.totalPrice = totalPrice;
        this.recieptId = recieptId;
        this.promotion_id = promotion_id;
    }

    public RecieptDetail() {
        this.recieptDetaiId = -1;
        this.productId = 0;
        this.productName = "";
        this.productPrice = 0;
        this.qty = 0;
        this.totalPrice = 0;
        this.recieptId = 0;
        this.productSize = "";
        this.promotion_id = 0;
    }

    public int getId() {
        return recieptDetaiId;
    }

    public void setId(int recieptDetaiId) {
        this.recieptDetaiId = recieptDetaiId;
    }

    public int getProductId() {
        return productId;
    }

    public void setProductId(int productId) {
        this.productId = productId;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public float getProductPrice() {
        return productPrice;
    }

    public void setProductPrice(float productPrice) {
        this.productPrice = productPrice;
    }

    public int getQty() {
        return qty;
    }

    public void setQty(int qty) {
        this.qty = qty;
        totalPrice = qty * productPrice;
    }

    public float getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(float totalPrice) {
        this.totalPrice = totalPrice;
    }

    public int getRecieptId() {
        return recieptId;
    }

    public void setRecieptId(int recieptId) {
        this.recieptId = recieptId;
    }

    public String getProductSize() {
        return productSize;
    }

    public void setProductSize(String productSize) {
        this.productSize = productSize;
    }

    public String getProductSweet() {
        return productSweet;
    }

    public void setProductSweet(String productSweet) {
        this.productSweet = productSweet;
    }

    public String getProductSubType() {
        return productSubType;
    }

    public void setProductSubType(String productSubType) {
        this.productSubType = productSubType;
    }

    public Date getRe_date() {
        return re_date;
    }

    public void setRe_date(Date re_date) {
        this.re_date = re_date;
    }

    public int getProduct_qty_day() {
        return product_qty_day;
    }

    public void setProduct_qty_day(int product_qty_day) {
        this.product_qty_day = product_qty_day;
    }

    public int getPromotion_id() {
        return promotion_id;
    }

    public void setPromotion_id(int promotion_id) {
        this.promotion_id = promotion_id;
    }

    @Override
    public String toString() {
        return "RecieptDetail{" + "recieptDetaiId=" + recieptDetaiId + ", productId=" + productId + ", productName=" + productName + ", productPrice=" + productPrice + ", qty=" + qty + ", totalPrice=" + totalPrice + ", recieptId=" + recieptId + ", productSize=" + productSize + ", productSweet=" + productSweet + ", productSubType=" + productSubType + ", promotion_id=" + promotion_id + '}';
    }

    public static RecieptDetail fromRS(ResultSet rs) {
        RecieptDetail recieptDetail = new RecieptDetail();
        try {
            recieptDetail.setId(rs.getInt("reciept_detail_id"));
            recieptDetail.setProductId(rs.getInt("product_id"));
            recieptDetail.setProductName(rs.getString("product_name"));
            recieptDetail.setProductPrice(rs.getFloat("product_price"));
            recieptDetail.setQty(rs.getInt("qty"));
            recieptDetail.setTotalPrice(rs.getFloat("total_price"));
            recieptDetail.setRecieptId(rs.getInt("reciept_id"));
            recieptDetail.setProductSize(rs.getString("product_size"));
            recieptDetail.setProductSubType(rs.getString("product_Sub_Type"));
            recieptDetail.setPromotion_id(rs.getInt("pd_id"));
            recieptDetail.setRe_date(rs.getTimestamp("re_date"));
            recieptDetail.setProduct_qty_day(rs.getInt("product_total_qty"));

        } catch (SQLException ex) {
            Logger.getLogger(RecieptDetail.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return recieptDetail;
    }
}
