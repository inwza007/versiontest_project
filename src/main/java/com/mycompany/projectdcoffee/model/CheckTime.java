/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.projectdcoffee.model;

import com.mycompany.projectdcoffee.dao.UserDao;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Phattharaphon
 */
public class CheckTime {

    private int whId;
    private int userId;
    private String whLoginDate;
    private String whLoginTime;
    private String whLogoutTime;
    private int whTotalTime;
    private User user;

    public CheckTime(int whId, int userId, String whLoginDate, String whLoginTime, String whLogoutTime, int whTotalTime) {
        this.whId = whId;
        this.userId = userId;
        this.whLoginDate = whLoginDate;
        this.whLoginTime = whLoginTime;
        this.whLogoutTime = whLogoutTime;
        this.whTotalTime = whTotalTime;
    }

    public CheckTime(int userId, String whLoginDate, String whLoginTime, String whLogoutTime, int whTotalTime) {
        this.whId = -1;
        this.userId = userId;
        this.whLoginDate = whLoginDate;
        this.whLoginTime = whLoginTime;
        this.whLogoutTime = whLogoutTime;
        this.whTotalTime = whTotalTime;
    }

    public CheckTime(int userId, int whTotalTime, String whStatus) {
        this.whId = -1;
        this.userId = userId;
        this.whLoginDate = null;
        this.whLoginTime = null;
        this.whLogoutTime = null;
        this.whTotalTime = whTotalTime;
    }

    public CheckTime() {
        this.whId = -1;
        this.userId = 0;
        this.whLoginDate = null;
        this.whLoginTime = null;
        this.whLogoutTime = null;
        this.whTotalTime = 0;
    }

    public int getWhId() {
        return whId;
    }

    public void setWhId(int whId) {
        this.whId = whId;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public String getWhLoginDate() {
        return whLoginDate;
    }

    public void setWhLoginDate(String whLoginDate) {
        this.whLoginDate = whLoginDate;
    }

    public String getWhLoginTime() {
        return whLoginTime;
    }

    public void setWhLoginTime(String whLoginTime) {
        this.whLoginTime = whLoginTime;
    }

    public String getWhLogoutTime() {
        return whLogoutTime;
    }

    public void setWhLogoutTime(String whLogoutTime) {
        this.whLogoutTime = whLogoutTime;
    }

    public int getWhTotalTime() {
        return whTotalTime;
    }

    public void setWhTotalTime(int whTotalTime) {
        this.whTotalTime = whTotalTime;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }


    @Override
    public String toString() {
        return "CheckTime{" + "whId=" + whId + ", userId=" + userId + ", whLoginDate=" + whLoginDate + ", whLoginTime=" + whLoginTime + ", whLogoutTime=" + whLogoutTime + ", whTotalTime=" + whTotalTime + '}';
    }
    
    public static CheckTime fromRS(ResultSet rs) {
        CheckTime checkTime = new CheckTime();
        try {
            checkTime.setWhId(rs.getInt("wh_id"));
            checkTime.setUserId(rs.getInt("user_id"));
            checkTime.setWhLoginDate(rs.getString("wh_login_date"));
            checkTime.setWhLoginTime(rs.getString("wh_login_time"));
            checkTime.setWhLogoutTime(rs.getString("wh_logout_time"));
            checkTime.setWhTotalTime(rs.getInt("wh_total_time"));
            //Population
            UserDao userDao = new UserDao();
            User user = userDao.get(checkTime.getUserId());
            checkTime.setUser(user);
        } catch (SQLException ex) {
            Logger.getLogger(CheckTime.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return checkTime;
    }
//    public static CheckTime fromRS(ResultSet rs) {
//        CheckTime checkTime = new CheckTime();
//        try {
//            checkTime.setWhId(rs.getInt("wh_id"));
//            checkTime.setUserId(rs.getInt("user_id"));
//
//            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd"); // Updated date format
//            Date loginDate = sdf.parse(rs.getString("wh_login_date"));
//            checkTime.setWhLoginDate(loginDate);
//
//            SimpleDateFormat timeFormat = new SimpleDateFormat("HH:mm:ss"); // Updated time format
//            Date loginTime = timeFormat.parse(rs.getString("wh_login_time"));
//            checkTime.setWhLoginTime(loginTime);
//
//            Date logoutTime = timeFormat.parse(rs.getString("wh_logout_time"));
//            checkTime.setWhLogoutTime(logoutTime);
//
//            checkTime.setWhTotalTime(rs.getInt("wh_total_time"));
//        } catch (SQLException ex) {
//            Logger.getLogger(CheckTime.class.getName()).log(Level.SEVERE, null, ex);
//        } catch (ParseException e) {// จัดการกับข้อผิดพลาดการแปลงรูปแบบวันเวลา
//            e.printStackTrace();
//        }
//        return checkTime;
//    }
}
