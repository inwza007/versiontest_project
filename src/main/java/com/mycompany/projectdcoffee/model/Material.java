/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.projectdcoffee.model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author test1
 */
public class Material {
    private int id;
    private String name;
    private int qty;
    private int minimum;
    private String unit;
    private String status;
    private float price ;

    public Material(int id, String name, int qty, int minimum, String unit, String status, float price) {
        this.id = id;
        this.name = name;
        this.qty = qty;
        this.minimum = minimum;
        this.unit = unit;
        this.status = status;
        this.price = price;
    }
    
        public Material(String name, int qty, int minimum, String unit, String status, float price) {
        this.id = -1;
        this.name = name;
        this.qty = qty;
        this.minimum = minimum;
        this.unit = unit;
        this.status = status;
        this.price = price;
    }
        
                public Material() {
        this.id = -1;
        this.name = "";
        this.qty = 0;
        this.minimum = 0;
        this.unit = "";
        this.status = "";
        this.price = 0;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getQty() {
        return qty;
    }

    public void setQty(int qty) {
        this.qty = qty;
    }

    public int getMinimum() {
        return minimum;
    }

    public void setMinimum(int minimum) {
        this.minimum = minimum;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public float getPrice() {
        return price;
    }

    public void setPrice(float price) {
        this.price = price;
    }

    @Override
    public String toString() {
        return "Material{" + "id=" + id + ", name=" + name + ", qty=" + qty + ", minimum=" + minimum + ", unit=" + unit + ", status=" + status + ", price=" + price + '}';
    }

    
                
          public static Material fromRS(ResultSet rs) {
        Material material = new Material();
        try {
            material.setId(rs.getInt("material_id"));
            material.setName(rs.getString("material_name"));
            material.setQty(rs.getInt("material_qty"));
            material.setMinimum(rs.getInt("material_min"));
            material.setUnit(rs.getString("material_unit"));
            material.setStatus(rs.getString("material_status"));
            material.setPrice(rs.getFloat("material_price"));

        } catch (SQLException ex) {
            Logger.getLogger(Material.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return material;
    }          
}
