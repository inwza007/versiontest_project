/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.projectdcoffee.model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Phattharaphon
 */
public class Store {
    private int storeId;
    private String storeName;
    private String storeTel;
    private String addres;

    public Store(int storeId, String storeName, String storeTel, String addres) {
        this.storeId = storeId;
        this.storeName = storeName;
        this.storeTel = storeTel;
        this.addres = addres;
    }
    
    public Store(String storeName, String storeTel, String addres) {
        this.storeId = -1;
        this.storeName = storeName;
        this.storeTel = storeTel;
        this.addres = addres;
    }
    
    public Store() {
        this.storeId = -1;
        this.storeName = "";
        this.storeTel = "";
        this.addres = "";
    }

    public int getStoreId() {
        return storeId;
    }

    public void setStoreId(int storeId) {
        this.storeId = storeId;
    }

    public String getStoreName() {
        return storeName;
    }

    public void setStoreName(String storeName) {
        this.storeName = storeName;
    }

    public String getStoreTel() {
        return storeTel;
    }

    public void setStoreTel(String storeTel) {
        this.storeTel = storeTel;
    }

    public String getAddres() {
        return addres;
    }

    public void setAddres(String addres) {
        this.addres = addres;
    }

    @Override
    public String toString() {
        return "store{" + "storeId=" + storeId + ", storeName=" + storeName + ", storeTel=" + storeTel + ", addres=" + addres + '}';
    }
    
    public static Store fromRS(ResultSet rs) {
        Store store = new Store();
        try {
            store.setStoreId(rs.getInt("store_id"));
            store.setStoreName(rs.getString("store_name"));
            store.setStoreTel(rs.getString("store_tel"));
            store.setAddres(rs.getString("store_addres"));
                        
        } catch (SQLException ex) {
            Logger.getLogger(Store.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return store;
    }
}
