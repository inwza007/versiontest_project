/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.projectdcoffee.model;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author zacop
 */
public class User {

    private int id;
    private String Fname;
    private String Lname;
    private String gender;
    private String role;
    private String email;
    private String addres;
    private String login;
    private String password;
    private int time;

    public int getTime() {
        return time;
    }

    public void setTime(int time) {
        this.time = time;
    }

    public User(int id, String Fname, String Lname, String gender, String role, String email, String addres, String login, String password) {
        this.id = id;
        this.Fname = Fname;
        this.Lname = Lname;
        this.gender = gender;
        this.role = role;
        this.email = email;
        this.addres = addres;
        this.login = login;
        this.password = password;
    }

    public User(String Fname, String Lname, String gender, String role, String email, String addres, String login, String password) {
        this.id = -1;
        this.Fname = Fname;
        this.Lname = Lname;
        this.gender = gender;
        this.role = role;
        this.email = email;
        this.addres = addres;
        this.login = login;
        this.password = password;
    }

    public User() {
        this.id = -1;
        this.Fname = "";
        this.Lname = "";
        this.gender = "";
        this.role = "";
        this.email = "";
        this.addres = "";
        this.login = "";
        this.password = "";
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFname() {
        return Fname;
    }

    public void setFname(String Fname) {
        this.Fname = Fname;
    }

    public String getLname() {
        return Lname;
    }

    public void setLname(String Lname) {
        this.Lname = Lname;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getAddres() {
        return addres;
    }

    public void setAddres(String addres) {
        this.addres = addres;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public String toString() {
        return "User{" + "id=" + id + ", Fname=" + Fname + ", Lname=" + Lname + ", gender=" + gender + ", role=" + role + ", email=" + email + ", addres=" + addres + ", login=" + login + ", password=" + password + '}';
    }



    public static User fromRS(ResultSet rs) {
        User user = new User();
        try {
            user.setId(rs.getInt("user_id"));
            user.setFname(rs.getString("user_first_name"));
            user.setLname(rs.getString("user_last_name"));
            user.setGender(rs.getString("user_gender"));
            user.setRole(rs.getString("user_role"));
            user.setEmail(rs.getString("user_email"));
            user.setAddres(rs.getString("user_addres"));
            user.setLogin(rs.getString("user_login"));
            user.setPassword(rs.getString("user_password"));
        } catch (SQLException ex) {
            Logger.getLogger(User.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return user;
    }

    public String getFullName() {
        return Fname + " " + Lname;
    }

    public boolean isValid() {

        //Business Rule
        //Name >=3
        //login >=3
        //password >=9
        return this.login.length() >= 3
                && this.getFullName().length() >= 3
                && this.password.length() >= 8;
    }
}
