/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.projectdcoffee.model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author GAMING PC
 */
public class Promotion {
    private int Pid;
    private String Pname;
    private int Pdiscount;
    private String Pstart;
    private String Pend;
    private String Pstatus;
    


    
    public Promotion (int Pid,String Pname,String Pstart,String Pend,String Pstatus,int Pdiscount){
        this.Pid = Pid;
        this.Pdiscount = Pdiscount;
        this.Pname = Pname;
        this.Pstart = Pstart;
        this.Pend = Pend;
        
    
    }
    
    public Promotion(String Pname,String Pstart,String Pend,String Pstatus,int Pdiscount){
        this.Pid = -1;
        this.Pname = Pname;
        this.Pstart = Pstart;
        this.Pend = Pend;
        this.Pdiscount = Pdiscount;        
    }
    
    public Promotion(){
        this.Pid = -1;
        this.Pname = "";
        this.Pstart = "";
        this.Pend = "";
        this.Pdiscount = 0;
    }

    public int getPid() {
        return Pid;
    }

    public void setPid(int Pid) {
        this.Pid = Pid;
    }

    public String getPname() {
        return Pname;
    }

    public void setPname(String Pname) {
        this.Pname = Pname;
    }

    public String getPstart() {
        return Pstart;
    }

    public void setPstart(String Pstart) {
        this.Pstart = Pstart;
    }

    public String getPend() {
        return Pend;
    }

    public void setPend(String Pend) {
        this.Pend = Pend;
    }

    public String getPstatus() {
        return Pstatus;
    }

    public void setPstatus(String Pstatus) {
        this.Pstatus = Pstatus;
    }

    public int getPdiscount() {
        return Pdiscount;
    }

    public void setPdiscount(int Pdiscount) {
        this.Pdiscount = Pdiscount;
    }

    @Override
    public String toString() {
        return "Promotion{" + "Pid=" + Pid + ", Pname=" + Pname + ", Pdiscount=" + Pdiscount + ", Pstart=" + Pstart + ", Pend=" + Pend + ", Pstatus=" + Pstatus + '}';
    }
    
    public static Promotion fromRS(ResultSet rs){
        Promotion promotion = new Promotion();
        try {
            promotion.setPid(rs.getInt("pr_id"));
            promotion.setPname(rs.getString("pr_name"));
            promotion.setPdiscount(rs.getInt("pr_discount"));
            promotion.setPstart(rs.getString("pr_start_date"));
            promotion.setPend(rs.getString("pr_end_date"));
            promotion.setPstatus(rs.getString("pr_status"));
            
        } catch (SQLException ex) {
            Logger.getLogger(Promotion.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return promotion;

        
    }
    
    
}
