/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.projectdcoffee.model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author test1
 */
public class MaterialRecieptDetail {

    private int id;
    private int materialRecieptId;
    private int materialId;
    private String materialName;
    private String materialUnit;
    private float price;
    private int qty;
    private float total;

    public MaterialRecieptDetail(int id, int materialRecieptId, int materialId, String materialName, String materialUnit, float price, int qty, float total) {
        this.id = id;
        this.materialRecieptId = materialRecieptId;
        this.materialId = materialId;
        this.materialName = materialName;
        this.materialUnit = materialUnit;
        this.price = price;
        this.qty = qty;
        this.total = total;
    }

    public MaterialRecieptDetail(int materialRecieptId, int materialId, String materialName, String materialUnit, float price, int qty, float total) {
        this.id = -1;
        this.materialRecieptId = materialRecieptId;
        this.materialId = materialId;
        this.materialName = materialName;
        this.materialUnit = materialUnit;
        this.price = price;
        this.qty = qty;
        this.total = total;
    }

    public MaterialRecieptDetail(int materialRecieptId, int materialId) {
        this.id = -1;
        this.materialRecieptId = materialRecieptId;
        this.materialId = materialId;
        this.materialName = "";
        this.materialUnit = "";
        this.price = 0;
        this.qty = 0;
        this.total = 0;
    }

    public MaterialRecieptDetail() {
        this.id = -1;
        this.materialRecieptId = 0;
        this.materialId = 0;
        this.materialName = "";
        this.materialUnit = "";
        this.price = 0;
        this.qty = 0;
        this.total = 0;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getMaterialRecieptId() {
        return materialRecieptId;
    }

    public void setMaterialRecieptId(int materialRecieptId) {
        this.materialRecieptId = materialRecieptId;
    }

    public int getMaterialId() {
        return materialId;
    }

    public void setMaterialId(int materialId) {
        this.materialId = materialId;
    }

    public String getMaterialName() {
        return materialName;
    }

    public void setMaterialName(String materialName) {
        this.materialName = materialName;
    }

    public String getMaterialUnit() {
        return materialUnit;
    }

    public void setMaterialUnit(String materialUnit) {
        this.materialUnit = materialUnit;
    }

    public float getPrice() {
        return price;
    }

    public void setPrice(float price) {
        this.price = price;
    }

    public int getQty() {
        return qty;
    }

    public void setQty(int qty) {
        this.qty = qty;
        total = qty * price;
    }

    public float getTotal() {
        return total;
    }

    public void setTotal(float total) {
        this.total = total;
    }

    @Override
    public String toString() {
        return "MaterialRecieptDetail{" + "id=" + id + ", materialRecieptId=" + materialRecieptId + ", materialId=" + materialId + ", materialName=" + materialName + ", materialUnit=" + materialUnit + ", price=" + price + ", qty=" + qty + ", total=" + total + '}';
    }

    public static MaterialRecieptDetail fromRS(ResultSet rs) {
        MaterialRecieptDetail recieptDetail = new MaterialRecieptDetail();
        try {
            recieptDetail.setId(rs.getInt("mrd_id"));
            recieptDetail.setMaterialRecieptId(rs.getInt("mr_id"));
            recieptDetail.setMaterialId(rs.getInt("material_id"));
            recieptDetail.setMaterialName(rs.getString("material_name"));
            recieptDetail.setMaterialUnit(rs.getString("material_unit"));
            recieptDetail.setPrice(rs.getFloat("mrd_price"));
            recieptDetail.setQty(rs.getInt("mrd_qty"));
            recieptDetail.setTotal(rs.getFloat("mrd_total"));
        } catch (SQLException ex) {
            Logger.getLogger(MaterialReciept.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return recieptDetail;
    }

}
