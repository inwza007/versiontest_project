/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.Ledger to edit this template
 */
package com.mycompany.projectdcoffee.model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Phattharaphon
 */
public class Ledger {
    private int seId;
    private int userId;
    private int storeId;
    private String seDate;
    private int seBillElectricity;
    private int seBillWater;
    private int seLandRent;
    private int seTotal;

    public Ledger(int seId, int userId, int storeId, String seDate, int seBillElectricity, int seBillWater, int seLandRent, int seTotal) {
        this.seId = seId;
        this.userId = userId;
        this.storeId = storeId;
        this.seDate = seDate;
        this.seBillElectricity = seBillElectricity;
        this.seBillWater = seBillWater;
        this.seLandRent = seLandRent;
        this.seTotal = seTotal;
    }
    
    public Ledger(int userId, int storeId, String seDate, int seBillElectricity, int seBillWater, int seLandRent, int seTotal) {
        this.seId = -1;
        this.userId = userId;
        this.storeId = storeId;
        this.seDate = seDate;
        this.seBillElectricity = seBillElectricity;
        this.seBillWater = seBillWater;
        this.seLandRent = seLandRent;
        this.seTotal = seTotal;
    }
    
//    public Ledger(int userId, int storeId, int seBillElectricity, int seBillWater, int seLandRent, int seTotal) {
//        this.seId = -1;
//        this.userId = userId;
//        this.storeId = storeId;
//        this.seDate = null;
//        this.seBillElectricity = seBillElectricity;
//        this.seBillWater = seBillWater;
//        this.seLandRent = seLandRent;
//        this.seTotal = seTotal;
//    }
    
    public Ledger() {
        this.seId = -1;
        this.userId = 0;
        this.storeId = 0;
        this.seDate = "";
        this.seBillElectricity = 0;
        this.seBillWater = 0;
        this.seLandRent = 0;
        this.seTotal = 0;
    }

    public int getSeId() {
        return seId;
    }

    public void setSeId(int seId) {
        this.seId = seId;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public int getStoreId() {
        return storeId;
    }

    public void setStoreId(int storeId) {
        this.storeId = storeId;
    }

    public String getSeDate() {
        return seDate;
    }

    public void setSeDate(String seDate) {
        this.seDate = seDate;
    }

    public int getSeBillElectricity() {
        return seBillElectricity;
    }

    public void setSeBillElectricity(int seBillElectricity) {
        this.seBillElectricity = seBillElectricity;
    }

    public int getSeBillWater() {
        return seBillWater;
    }

    public void setSeBillWater(int seBillWater) {
        this.seBillWater = seBillWater;
    }

    public int getSeLandRent() {
        return seLandRent;
    }

    public void setSeLandRent(int seLandRent) {
        this.seLandRent = seLandRent;
    }

    public int getSeTotal() {
        return seTotal;
    }

    public void setSeTotal(int seTotal) {
        this.seTotal = seTotal;
    }

    @Override
    public String toString() {
        return "Ledger{" + "seId=" + seId + ", userId=" + userId + ", storeId=" + storeId + ", seDate=" + seDate + ", seBillElectricity=" + seBillElectricity + ", seBillWater=" + seBillWater + ", seLandRent=" + seLandRent + ", seTotal=" + seTotal + '}';
    }
    
    public static Ledger fromRS(ResultSet rs) {
        Ledger ledger = new Ledger();
        try {
            ledger.setSeId(rs.getInt("se_id"));
            ledger.setUserId(rs.getInt("user_id"));
            ledger.setStoreId(rs.getInt("store_id"));
            ledger.setSeDate(rs.getString("se_date"));
            ledger.setSeBillElectricity(rs.getInt("se_bill_electricity"));
            ledger.setSeBillWater(rs.getInt("se_bill_water"));
            ledger.setSeLandRent(rs.getInt("se_land_rent"));
            ledger.setSeTotal(rs.getInt("se_total"));
                        
        } catch (SQLException ex) {
            Logger.getLogger(Ledger.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return ledger;
    }

    public void set(int id) {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }
}
