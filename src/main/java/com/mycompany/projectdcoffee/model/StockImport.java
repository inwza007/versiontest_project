/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.projectdcoffee.model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author gortorr
 */
public class StockImport {
    private int si_id;
    private int si_supplier_id;
    private String si_supplier_name;
    private String si_name;
    private String si_unit;
    private float si_price;
    private int user_id;
    private int material_id;
    private Date si_date_import;
    private float si_total;
    private int si_qty;

    public StockImport(int si_id,int si_qty, int si_supplier_id, String si_supplier_name, String si_name, String si_unit, float si_price, int user_id, int material_id, Date si_date_import, float si_total) {
        this.si_id = si_id;
        this.si_supplier_id = si_supplier_id;
        this.si_supplier_name = si_supplier_name;
        this.si_name = si_name;
        this.si_unit = si_unit;
        this.si_price = si_price;
        this.user_id = user_id;
        this.material_id = material_id;
        this.si_date_import = si_date_import;
        this.si_total = si_total;
        this.si_qty = si_qty;
    }
    public StockImport(int si_supplier_id,int si_qty, String si_supplier_name, String si_name, String si_unit, float si_price, int user_id, int material_id, Date si_date_import, float si_total) {
        this.si_id = -1;
        this.si_supplier_id = si_supplier_id;
        this.si_supplier_name = si_supplier_name;
        this.si_name = si_name;
        this.si_unit = si_unit;
        this.si_price = si_price;
        this.user_id = user_id;
        this.material_id = material_id;
        this.si_date_import = si_date_import;
        this.si_total = si_total;
        this.si_qty = si_qty;
    }
    
    public StockImport() {
        this.si_id = -1;
        this.si_supplier_id = 0;
        this.si_supplier_name = "";
        this.si_name = "";
        this.si_unit = "";
        this.si_price = 0;
        this.user_id = 0;
        this.material_id = 0;
        this.si_date_import = null;
        this.si_total = 0;
        this.si_qty = 0;
    }

    public int getSi_id() {
        return si_id;
    }
    
    public int getSi_qty() {
        return si_qty;
    }
    
    

    public int getSi_supplier_id() {
        return si_supplier_id;
    }

    public String getSi_supplier_name() {
        return si_supplier_name;
    }

    public String getSi_name() {
        return si_name;
    }

    public String getSi_unit() {
        return si_unit;
    }

    public float getSi_price() {
        return si_price;
    }

    public int getUser_id() {
        return user_id;
    }

    public int getMaterial_id() {
        return material_id;
    }

    public Date getSi_date_import() {
        return si_date_import;
    }

    public float getSi_total() {
        return si_total;
    }

    public void setSi_id(int si_id) {
        this.si_id = si_id;
    }

    public void setSi_qty(int si_qty) {
        this.si_qty = si_qty;
    }
    

    public void setSi_supplier_id(int si_supplier_id) {
        this.si_supplier_id = si_supplier_id;
    }

    public void setSi_supplier_name(String si_supplier_name) {
        this.si_supplier_name = si_supplier_name;
    }

    public void setSi_name(String si_name) {
        this.si_name = si_name;
    }

    public void setSi_unit(String si_unit) {
        this.si_unit = si_unit;
    }

    public void setSi_price(float si_price) {
        this.si_price = si_price;
    }

    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }

    public void setMaterial_id(int material_id) {
        this.material_id = material_id;
    }

    public void setSi_date_import(Date si_date_import) {
        this.si_date_import = si_date_import;
    }

    public void setSi_total(float si_total) {
        this.si_total = si_total;
    }

    @Override
    public String toString() {
        return "StockImport{" + "si_id=" + si_id + ", si_supplier_id=" + si_supplier_id + ", si_supplier_name=" + si_supplier_name + ", si_name=" + si_name + ", si_unit=" + si_unit + ", si_price=" + si_price + ", user_id=" + user_id + ", material_id=" + material_id + ", si_date_import=" + si_date_import + ", si_total=" + si_total + ", si_qty=" + si_qty + '}';
    }

    
    
    public static StockImport fromRS(ResultSet rs) {
        StockImport stockImport = new StockImport();
        try {
            stockImport.setSi_id(rs.getInt("si_id"));
            stockImport.setSi_supplier_id(rs.getInt("si_supplier_id"));
            stockImport.setSi_supplier_name(rs.getString("si_supplier_name"));
            stockImport.setSi_name(rs.getString("si_name"));
            stockImport.setSi_unit(rs.getString("Si_unit"));
            stockImport.setSi_price(rs.getFloat("Si_price"));
            stockImport.setUser_id(rs.getInt("user_id"));
            stockImport.setMaterial_id(rs.getInt("material_id"));
            stockImport.setSi_date_import(rs.getDate("Si_date_import"));
            stockImport.setSi_total(rs.getFloat("si_total"));
            stockImport.setSi_qty(rs.getInt("si_qty"));

        } catch (SQLException ex) {
            Logger.getLogger(Stock.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return stockImport;
    }
    
    
    
}
