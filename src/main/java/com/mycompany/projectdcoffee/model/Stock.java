/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.projectdcoffee.model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author test1
 */
public class Stock {
    
    private ArrayList<Stock> stock = new ArrayList();
    private int id;
    private String name;
    private int qty;
    private int qtyim;
    private int minimum;
    private String unit;
    private String status;

    public Stock(int id, String name, int qty, int minimum, String unit, String status,int qtyim) {
        this.id = id;
        this.name = name;
        this.qty = qty;
        this.minimum = minimum;
        this.unit = unit;
        this.status = status;
        this.qtyim = qtyim;
    }

    public Stock(String name,int qtyim, int qty, int minimum, String unit, String status) {
        this.id = -1;
        this.name = name;
        this.qty = qty;
        this.minimum = minimum;
        this.unit = unit;
        this.status = status;
        this.qtyim = qtyim;
    }

    public Stock() {
        this.id = -1;
        this.name = "";
        this.qty = 0;
        this.minimum = 0;
        this.unit = "";
        this.status = "";
        this.qtyim = 0;
    }

    @Override
    public String toString() {
        return "Stock{" + "id=" + id + ", name=" + name + ", qty=" + qty + ", qtyim=" + qtyim + ", minimum=" + minimum + ", unit=" + unit + ", status=" + status + '}';
    }

    public int getQtyim() {
        return qtyim;
    }

    public void setQtyim(int qtyim) {
        this.qtyim = qtyim;
    }

    

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getQty() {
        return qty;
    }

    public void setQty(int qty) {
        this.qty = qty;
    }

    public int getMinimum() {
        return minimum;
    }

    public void setMinimum(int minimum) {
        this.minimum = minimum;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
    
    

    public void setStock(ArrayList<Stock> stock) {
        this.stock = stock;
    }

    public ArrayList<Stock> getStock() {
        return stock;
    }

    public static Stock fromRS(ResultSet rs) {
        Stock stock = new Stock();
        try {
            stock.setId(rs.getInt("material_id"));
            stock.setName(rs.getString("material_name"));
            stock.setQty(rs.getInt("material_qty"));
            stock.setQtyim(rs.getInt("material_qtyim"));
            stock.setMinimum(rs.getInt("material_min"));
            stock.setUnit(rs.getString("material_unit"));
            stock.setStatus(rs.getString("material_status"));
            

        } catch (SQLException ex) {
            Logger.getLogger(Stock.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return stock;
    }

    
}
