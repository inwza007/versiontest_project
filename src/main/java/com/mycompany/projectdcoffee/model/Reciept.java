
/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.projectdcoffee.model;

import com.mycompany.projectdcoffee.dao.CustomerDao;
import com.mycompany.projectdcoffee.dao.RecieptDao;
import com.mycompany.projectdcoffee.dao.UserDao;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author werapan
 */
public class Reciept {
    private int reciept_id;
    private Date re_date;
    private float re_total;
    private float re_pay;
    private int re_total_qty;
    private int user_id;
    private int customer_id; 
    private float re_discount;
    private float re_net;
    private String re_pay_type;
    private float re_change;
    private int store_id;
    private User user;
    private Customer customer;
    private ArrayList<RecieptDetail> recieptDetails = new ArrayList();
    private int total;
    private int countReciept;
    private float total_price_day;

    public Reciept(int reciept_id, Date re_date, float re_total, float re_pay, int re_total_qty, int user_id, int customer_id, float re_discount, float re_net, String re_pay_type, float re_change, int store_id, User user, Customer customer) {
        this.reciept_id = reciept_id;
        this.re_date = re_date;
        this.re_total = re_total;
        this.re_pay = re_pay;
        this.re_total_qty = re_total_qty;
        this.user_id = user_id;
        this.customer_id = customer_id;
        this.re_discount = re_discount;
        this.re_net = re_net;
        this.re_pay_type = re_pay_type;
        this.re_change = re_change;
        this.store_id = store_id;
    }

    public Reciept(Date re_date, float re_total, float re_pay, int re_total_qty, int user_id, int customer_id, float re_discount, float re_net, String re_pay_type, float re_change, int store_id, User user, Customer customer) {
        this.reciept_id =-1;
        this.re_date = re_date;
        this.re_total = re_total;
        this.re_pay = re_pay;
        this.re_total_qty = re_total_qty;
        this.user_id = user_id;
        this.customer_id = customer_id;
        this.re_discount = re_discount;
        this.re_net = re_net;
        this.re_pay_type = re_pay_type;
        this.re_change = re_change;
        this.store_id = store_id;
    }

    public Reciept(float re_total, float re_pay, int re_total_qty, int user_id, int customer_id, float re_discount, float re_net, String re_pay_type, float re_change, int store_id, User user, Customer customer) {
        this.reciept_id =-1;
        this.re_date = null;
        this.re_total = re_total;
        this.re_pay = re_pay;
        this.re_total_qty = re_total_qty;
        this.user_id = user_id;
        this.customer_id = customer_id;
        this.re_discount = re_discount;
        this.re_net = re_net;
        this.re_pay_type = re_pay_type;
        this.re_change = re_change;
        this.store_id = store_id;
    }

    public Reciept(float re_pay, int user_id, int customer_id) {
        this.reciept_id =-1;
        this.re_date = null;
        this.re_total = 0;
        this.re_pay = re_pay;
        this.re_total_qty = 0;
        this.user_id = user_id;
        this.customer_id = customer_id;
        this.re_discount = 0;
        this.re_net = 0;
        this.re_pay_type = "Cash";
        this.re_change = 0;
        this.store_id = 0;
    }


    public Reciept() {
        this.reciept_id =-1;
        this.re_date = null;
        this.re_total = 0;
        this.re_pay = 0;
        this.re_total_qty = 0;
        this.user_id = 0;
        this.customer_id = 0;
        this.re_discount = 0;
        this.re_net = 0;
        this.re_pay_type ="";
        this.re_change = 0;
        this.store_id = 3;
    }

    public int getReciept_id() {
        return reciept_id;
    }

    public void setReciept_id(int reciept_id) {
        this.reciept_id = reciept_id;
    }

    public Date getRe_date() {
        return re_date;
    }

    public void setRe_date(Date re_date) {
        this.re_date = re_date;
    }

    public float getRe_total() {
        return re_total;
    }

    public void setRe_total(float re_total) {
        this.re_total = re_total;
    }

    public float getRe_pay() {
        return re_pay;
    }

    public void setRe_pay(float re_pay) {
        this.re_pay = re_pay;
    }

    public int getRe_total_qty() {
        return re_total_qty;
    }

    public void setRe_total_qty(int re_total_qty) {
        this.re_total_qty = re_total_qty;
    }

    public int getUser_id() {
        return user_id;
    }

    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }

    public int getCustomer_id() {
        return customer_id;
    }

    public void setCustomer_id(int customer_id) {
        this.customer_id = customer_id;
    }

    public float getRe_discount() {
        return re_discount;
    }

    public void setRe_discount(float re_discount) {
        this.re_discount = re_discount;
    }

    public float getRe_net() {
        return re_net;
    }

    public void setRe_net(float re_net) {
        this.re_net = re_net;
    }

    public String getRe_pay_type() {
        return re_pay_type;
    }

    public void setRe_pay_type(String re_pay_type) {
        this.re_pay_type = re_pay_type;
    }

    public float getRe_change() {
        return re_change;
    }

    public void setRe_change(float re_change) {
        this.re_change = re_change;
    }

    public int getStore_id() {
        return store_id;
    }

    public void setStore_id(int store_id) {
        this.store_id = store_id;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Customer getCustomer() {
        return customer;
    }

    public int getCountReciept() {
        return countReciept;
    }

    public void setCountReciept(int countReciept) {
        this.countReciept = countReciept;
    }
    
    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public float getTotal_price_day() {
        return total_price_day;
    }

    public void setTotal_price_day(float total_price_day) {
        this.total_price_day = total_price_day;
    }

    public ArrayList<RecieptDetail> getRecieptDetails() {
        return recieptDetails;
    }

    public void setRecieptDetails(ArrayList recieptDetails) {
        this.recieptDetails = recieptDetails;
    }
    
    public void addRecieptDetail(RecieptDetail recieptDetail){
        recieptDetails.add(recieptDetail);
        calculateTotal();
    }
    
    public void addRecieptDetail(Product product,String size, int qty){
        total = increaseTotalFormSize(product, qty);
        RecieptDetail rd = new RecieptDetail(product.getId(),product.getProductName(),total,qty,qty*total,-1,product.getProductSize(),product.getProductSweetLevel(),product.getProductSubType(),0);
        //RecieptDetail rd = new RecieptDetail(store_id, size, re_change, qty, re_total, reciept_id, size, size, re_pay_type);
        recieptDetails.add(rd);
        calculateTotal();
    }

    private int increaseTotalFormSize(Product product, int qty) {
        if(product.getProductSize().equals("M")){
            total = (int) (product.getProductPrice()+5);
            return total;
        }else if(product.getProductSize().equals("L")){
            total = (int) (product.getProductPrice()+10);
            return total;
        }else{
            total = (int) (product.getProductPrice());
            return total;
        }
    }
    
    public void delRecieptDetail(RecieptDetail recieptDetail){
        recieptDetails.remove(recieptDetail);
        calculateTotal();
    }
    
    public void calculateTotal() {
        int totalQty = 0;
        float total = 0.0f;
        for(RecieptDetail rd: recieptDetails) {
            total += rd.getTotalPrice();
            totalQty += rd.getQty();
        }
        this.re_total_qty = totalQty;
        this.re_total = total;
    }

    @Override
    public String toString() {
        return "Reciept{" + "reciept_id=" + reciept_id + ", re_date=" + re_date + ", re_total=" + re_total + ", re_pay=" + re_pay + ", re_total_qty=" + re_total_qty + ", user_id=" + user_id + ", customer_id=" + customer_id + ", re_discount=" + re_discount + ", re_net=" + re_net + ", re_pay_type=" + re_pay_type + ", re_change=" + re_change + ", store_id=" + store_id + ", user=" + user + ", customer=" + customer + ", recieptDetails=" + recieptDetails + '}';
    } 
    
    public static Reciept fromRS(ResultSet rs) {
        Reciept reciept = new Reciept();
        try {
            reciept.setReciept_id(rs.getInt("reciept_id"));
            reciept.setRe_date(rs.getTimestamp("re_date"));
            reciept.setRe_total(rs.getFloat("re_total"));
            reciept.setRe_pay(rs.getFloat("re_pay"));
            reciept.setRe_total_qty(rs.getInt("re_total_qty"));
            reciept.setStore_id(rs.getInt("store_id"));
            reciept.setRe_discount(rs.getFloat("re_discount"));
            reciept.setRe_net(rs.getFloat("re_net"));
            reciept.setRe_pay_type(rs.getString("re_pay_type"));
            reciept.setRe_change(rs.getFloat("re_change"));
            reciept.setUser_id(rs.getInt("user_id"));
            reciept.setCustomer_id(rs.getInt("customer_id"));
            reciept.setCountReciept(rs.getInt("count_reciept"));
            reciept.setTotal_price_day(rs.getFloat("total_price_day"));
            //Population
            CustomerDao customerDao = new CustomerDao();
            UserDao userDao = new UserDao();
            Customer customer = customerDao.get(reciept.getCustomer_id());
            User user = userDao.get(reciept.getUser_id());
            reciept.setCustomer(customer);
            reciept.setUser(user);
        } catch (SQLException ex) {
            Logger.getLogger(Reciept.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return reciept;
    }
}