/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.projectdcoffee.model;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author armme
 */
public class Salary {

    private int prId;
    private int userId;
    private int whId;
    private String prApprove;
    private Date prDate;
    private int whTotalTime;
    private int prPricePerHour;
    private BigDecimal prTotalSalary;
    private ArrayList<Salary> SalaryDetails = new ArrayList();
    private String fullname;

    public ArrayList<Salary> getSalaryDetails() {
        return SalaryDetails;
    }

    public void setSalaryDetails(ArrayList<Salary> SalaryDetails) {
        this.SalaryDetails = SalaryDetails;
    }

    public String getFullname() {
        return fullname;
    }

    public void setFullname(String fullname) {
        this.fullname = fullname;
    }

    public Salary(int prId, int userId, int whId, String prApprove, Date prDate, int whTotalTime, int prPricePerHour, BigDecimal prTotalSalary) {
        this.prId = prId;
        this.userId = userId;
        this.whId = whId;
        this.prApprove = prApprove;
        this.prDate = prDate;
        this.whTotalTime = whTotalTime;
        this.prPricePerHour = prPricePerHour;
        this.prTotalSalary = prTotalSalary;
    }

    public Salary(int userId, int whId, String prApprove, Date prDate, int whTotalTime, int prPricePerHour,
            BigDecimal prTotalSalary) {
        this.prId = -1;
        this.userId = userId;
        this.whId = whId;
        this.prApprove = prApprove;
        this.prDate = prDate;
        this.whTotalTime = whTotalTime;
        this.prPricePerHour = prPricePerHour;
        this.prTotalSalary = prTotalSalary;
    }

    public Salary(int userId, int whId, String prApprove, String prType, int whTotalTime, int prPricePerHour,
            BigDecimal prTotalSalary) {
        this.prId = -1;
        this.userId = userId;
        this.whId = whId;
        this.prApprove = prApprove;
        this.prDate = null;
        this.whTotalTime = whTotalTime;
        this.prPricePerHour = prPricePerHour;
        this.prTotalSalary = prTotalSalary;
    }

    public Salary(int prId, String fullName, int PrPricePerHour,int TotalTime, BigDecimal prTotalSalary) {
        this.prId = prId;
        this.fullname = fullName;
        this.prPricePerHour = PrPricePerHour;
        this.whTotalTime = TotalTime;
        this.prTotalSalary = prTotalSalary;
    }

    public Salary() {
        this.prId = -1;
        this.userId = 0;
        this.whId = 0;
        this.prDate = null;
        this.whTotalTime = 0;
        this.prPricePerHour = 0;
        this.prTotalSalary = BigDecimal.ZERO;
    }

    public int getPrId() {
        return prId;
    }

    public void setPrId(int prId) {
        this.prId = prId;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public int getWhId() {
        return whId;
    }

    public void setWhId(int whId) {
        this.whId = whId;
    }

    public String getPrApprove() {
        return prApprove;
    }

    public void setPrApprove(String prApprove) {
        this.prApprove = prApprove;
    }

    public Date getPrDate() {
        return prDate;
    }

    public void setPrDate(Date prDate) {
        this.prDate = prDate;
    }

    public int getWhTotalTime() {
        return whTotalTime;
    }

    public void setWhTotalTime(int whTotalTime) {
        this.whTotalTime = whTotalTime;
    }

    public int getPrPricePerHour() {
        return prPricePerHour;
    }

    public void setPrPricePerHour(int prPricePerHour) {
        this.prPricePerHour = prPricePerHour;
    }

    public BigDecimal getPrTotalSalary() {
        return prTotalSalary;
    }

    public void setPrTotalSalary(BigDecimal prTotalSalary) {
        this.prTotalSalary = prTotalSalary;
    }

    @Override
    public String toString() {
        return "Salary{" + "prId=" + prId + ", userId=" + userId + ", whId=" + whId + ", prApprove=" + prApprove
                + ", prDate=" + prDate + ", whTotalTime=" + whTotalTime + ", prPricePerHour=" + prPricePerHour
                + ", prTotalSalary=" + prTotalSalary + '}';
    }

    public void addSalaryDetail(Salary salary) {
        SalaryDetails.add(salary);
        calculateTotal();
    }

    public void calculateTotal() {
        BigDecimal total = BigDecimal.ZERO;
        for (Salary sl : SalaryDetails) {
            total = total.add(sl.getPrTotalSalary());
        }
        this.prTotalSalary = total;
    }

    public static Salary fromRS(ResultSet rs) {
        Salary salary = new Salary();
        try {
            salary.setPrId(rs.getInt("payroll_id"));
            salary.setUserId(rs.getInt("user_id"));
            salary.setWhId(rs.getInt("wh_id"));
            salary.setPrApprove(rs.getString("payroll_approve"));
            salary.setPrDate(rs.getDate("payroll_date"));
            salary.setWhTotalTime(rs.getInt("wh_total_time"));
            salary.setPrPricePerHour(rs.getInt("payroll_price_per_hour"));
            salary.setPrTotalSalary(rs.getBigDecimal("payroll_total_salary"));
        } catch (SQLException ex) {
            Logger.getLogger(Salary.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return salary;
    }
}
