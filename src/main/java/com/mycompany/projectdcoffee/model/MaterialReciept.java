/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.projectdcoffee.model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author test1
 */
public class MaterialReciept {

    private int id;
    private Date date;
    private int supplierId;
    private int totalQty;
    private float total;
    private int userId;
    private int storeId;
    private User user;
    private ArrayList<MaterialRecieptDetail> materialRecieptDetails = new ArrayList();

    public MaterialReciept(int id, Date date, int supplierId, int totalQty, float total, int userId, int storeId) {
        this.id = id;
        this.date = date;
        this.supplierId = supplierId;
        this.totalQty = totalQty;
        this.total = total;
        this.userId = userId;
        this.storeId = storeId;
    }

    public MaterialReciept(int id, int supplierId, int totalQty, float total, int userId, int storeId) {
        this.id = id;
        this.supplierId = supplierId;
        this.totalQty = totalQty;
        this.total = total;
        this.userId = userId;
        this.storeId = storeId;
    }

    public MaterialReciept(int supplierId, int totalQty, float total, int userId, int storeId) {
        this.id = -1;
        this.supplierId = supplierId;
        this.totalQty = totalQty;
        this.total = total;
        this.userId = userId;
        this.storeId = storeId;
    }

    public MaterialReciept(int totalQty, float total, int userId, int storeId) {
        this.id = -1;
        this.supplierId = 1;
        this.totalQty = totalQty;
        this.total = total;
        this.userId = userId;
        this.storeId = storeId;
    }

    public MaterialReciept() {
        this.id = -1;
        this.supplierId = 1;
        this.totalQty = 0;
        this.total = 0;
        this.userId = -1;
        this.storeId = -1;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public int getSupplierId() {
        return supplierId;
    }

    public void setSupplierId(int supplierId) {
        this.supplierId = supplierId;
    }

    public int getTotalQty() {
        return totalQty;
    }

    public void setTotalQty(int totalQty) {
        this.totalQty = totalQty;
    }

    public float getTotal() {
        return total;
    }

    public void setTotal(float total) {
        this.total = total;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public int getStoreId() {
        return storeId;
    }

    public void setStoreId(int storeId) {
        this.storeId = storeId;
    }

    public ArrayList<MaterialRecieptDetail> getMaterialRecieptDetails() {
        return materialRecieptDetails;
    }

    public void setMaterialRecieptDetails(ArrayList<MaterialRecieptDetail> materialRecieptDetails) {
        this.materialRecieptDetails = materialRecieptDetails;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
        this.userId = user.getId();
    }

    public void addMaterialRecieptDetail(MaterialRecieptDetail materialRecieptDetail) {
        materialRecieptDetails.add(materialRecieptDetail);
        calculateTotal();
    }

    public void addMaterialRecieptDetail(Material material, int qty) {
        MaterialRecieptDetail rd = new MaterialRecieptDetail(-1, material.getId(),
                material.getName(), material.getUnit(), material.getPrice(),
                qty, qty * material.getPrice());
        materialRecieptDetails.add(rd);
        calculateTotal();
    }

    public void delRecieptDetail(MaterialRecieptDetail materailRecieptDetail) {
        materialRecieptDetails.remove(materailRecieptDetail);
        calculateTotal();
    }

    public void calculateTotal() {
        int totalQty = 0;
        float total = 0.0f;
        for (MaterialRecieptDetail rd : materialRecieptDetails) {
            total += rd.getTotal();
            totalQty += rd.getQty();
        }
        this.totalQty = totalQty;
        this.total = total;
    }

    @Override
    public String toString() {
        return "MaterialReciept{" + "id=" + id + ", date=" + date + ", supplierId=" + supplierId + ", totalQty=" + totalQty + ", total=" + total + ", userId=" + userId + ", storeId=" + storeId + ", user=" + user + ", materialRecieptDetails=" + materialRecieptDetails + '}';
    }
    

    public static MaterialReciept fromRS(ResultSet rs) {
        MaterialReciept obj = new MaterialReciept();
        try {
            obj.setId(rs.getInt("mr_id"));
            obj.setDate(rs.getDate("mr_date"));
            obj.setSupplierId(rs.getInt("supplier_id"));
            obj.setTotalQty(rs.getInt("mr_total_qty"));
            obj.setTotal(rs.getFloat("mr_total"));
            obj.setUserId(rs.getInt("user_id"));
            obj.setStoreId(rs.getInt("store_id"));
//            obj.setId(rs.getInt("mr_recived_date"));
            // Population
//            CustomerDao customerDao = new CustomerDao();
//            UserDao userDao = new UserDao();
//            Customer customer = customerDao.get(obj.getCustomerId());
//            User user = userDao.get(obj.getUserId());
//            obj.setCustomer(customer);
//            obj.setUser(user);
        } catch (SQLException ex) {
            Logger.getLogger(MaterialReciept.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return obj;
    }
}
