/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.projectdcoffee.model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author werapan
 */
public class Customer {

    private int id;
    private String firstName;
    private String lastName;
    private String tel;
    private int point;
    private String birthDay;
    private int usedPoint;

    public Customer(int id, String firstName, String lastName, String tel, int point, int usedPoint) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.tel = tel;
        this.point = point;
        this.birthDay = null;
        this.usedPoint = usedPoint;
    }

    public Customer(String firstName, String lastName, String tel, int point, String birthDay, int usedPoint) {
        this.id = -1;
        this.firstName = firstName;
        this.lastName = lastName;
        this.tel = tel;
        this.point = point;
        this.birthDay = birthDay;
        this.usedPoint = usedPoint;
    }

    public Customer() {
        this.id = -1;
        this.firstName = "";
        this.tel = "";
        this.lastName = "";
        this.point = 0;
        this.birthDay = null;
        this.usedPoint = 0;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getTel() {
        return tel;
    }

    public void setTel(String tel) {
        this.tel = tel;
    }

    public int getPoint() {
        return point;
    }

    public void setPoint(int point) {
        this.point = point;
    }

    public String getBirthDay() {
        return birthDay;
    }

    public void setBirthDay(String birthDay) {
        this.birthDay = birthDay;
    }

    public int getUsedPoint() {
        return usedPoint;
    }

    public void setUsedPoint(int userdPoint) {
        this.usedPoint = userdPoint;
    }

    @Override
    public String toString() {
        return "Customer{" + "id=" + id + ", firstName=" + firstName + ", lastName=" + lastName + ", tel=" + tel + ", point=" + point + ", birthDay=" + birthDay + ", usedPoint=" + usedPoint + '}';
    }

    public static Customer fromRS(ResultSet rs) {
        Customer customer = new Customer();
        try {
            customer.setId(rs.getInt("customer_id"));
            customer.setFirstName(rs.getString("customer_firstname"));
            customer.setLastName(rs.getString("customer_lastname"));
            customer.setTel(rs.getString("customer_tel"));
            customer.setPoint(rs.getInt("customer_point"));
            customer.setBirthDay(rs.getString("customer_birthdate"));
            customer.setUsedPoint(rs.getInt("customer_usedPoint"));
        } catch (SQLException ex) {
            Logger.getLogger(Customer.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return customer;
    }

    public Object getFname() {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }
}
