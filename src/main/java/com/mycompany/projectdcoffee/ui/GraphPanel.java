/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/GUIForms/JPanel.java to edit this template
 */
package com.mycompany.projectdcoffee.ui;

import com.mycompany.projectdcoffee.JDate.DateLabelFormatter;
import com.mycompany.projectdcoffee.dao.RecieptDao;
import com.mycompany.projectdcoffee.model.Customer;
import com.mycompany.projectdcoffee.model.Reciept;
import com.mycompany.projectdcoffee.model.RecieptDetail;
import com.mycompany.projectdcoffee.service.CustomerService;
import com.mycompany.projectdcoffee.service.RecieptService;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Properties;
import javax.swing.table.AbstractTableModel;
import org.jdatepicker.impl.JDatePanelImpl;
import org.jdatepicker.impl.JDatePickerImpl;
import org.jdatepicker.impl.UtilDateModel;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.data.category.DefaultCategoryDataset;
import org.jfree.data.general.DefaultPieDataset;

/**
 *
 * @author zacop
 */
public class GraphPanel extends javax.swing.JPanel {

    private CustomerService customerService;
    private List<Customer> customerList;

    private RecieptService recieptService;
    private List<RecieptDetail> recieptDetailList;
    private List<Reciept> recieptList;
    private RecieptDao recieptDao;
    private AbstractTableModel model;
    private UtilDateModel model1;
    private UtilDateModel model2;
    private DefaultPieDataset peiDataset;
    private DefaultPieDataset pieDataset;
    private DefaultCategoryDataset barDataset;

    private boolean PS, CP, TP, RP = false;

    public GraphPanel() {
        initComponents();
        cmbSell.setVisible(true);
        initDatePicker();
        recieptService = new RecieptService();
        recieptDetailList = recieptService.getProductDay();
        ProductPerDay("Product Sales Per Day");
        PS = true;
    }

    private void TotalPricePerDay(String Date) {
        initTableForTotalPrice();
        initPieChartForTotalPrice(Date);
        loadPieDatasetForTotalPrice();
        initBarChartForTotalPrice(Date);
        loadBarDatasetForTotalPrice();
    }

    private void TopTenCustomer() {
        initTableForCustomer();
        initPieChartForCustomer();
        loadPieDatasetForCustomer();
        initBarChartForCustomer();
        loadBarDatasetForCustomer();
    }

    private void RecieptPerDay(String Date) {
        initTableForDailySale();
        initPieChartForRecieptPerMonth(Date);
        loadPieDatasetForRecieptPerMonth();
        initBarChartForRecieptPerMonth(Date);
        loadBarDatasetForRecieptPerMonth();
    }

    private void ProductPerDay(String Date) {
        initTableForProduct();
        initPieChartForProduct(Date);
        loadPieDatasetForProduct();
        initBarChartForProduct(Date);
        loadBarDatasetForProduct();
    }

    private void initDatePicker() {
        model1 = new UtilDateModel();
        Properties p1 = new Properties();
        p1.put("text.day", "Today");
        p1.put("text.month", "Month");
        p1.put("text.year", "Year");

        JDatePanelImpl datePanel1 = new JDatePanelImpl(model1, p1);
        JDatePickerImpl datePicker1 = new JDatePickerImpl(datePanel1, new DateLabelFormatter());
        pnlDatePicker1.add(datePicker1);
        model1.setSelected(true);

        model2 = new UtilDateModel();
        Properties p2 = new Properties();
        p2.put("text.day", "Today");
        p2.put("text.month", "Month");
        p2.put("text.year", "Year");

        JDatePanelImpl datePanel2 = new JDatePanelImpl(model2, p2);
        JDatePickerImpl datePicker2 = new JDatePickerImpl(datePanel2, new DateLabelFormatter());
        pnlDatePicker2.add(datePicker2);
        model2.setSelected(true);
        tblShowfiltr.setRowHeight(50);
    }

    private void initTableForCustomer() {
        model = new AbstractTableModel() {
            String[] colNames = {"ID", "Full Name", "Used Point"};

            @Override
            public String getColumnName(int column) {
                return colNames[column]; // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/OverriddenMethodBody
            }

            @Override
            public int getRowCount() {
                return customerList.size();
            }

            @Override
            public int getColumnCount() {
                return 3;
            }

            @Override
            public Object getValueAt(int rowIndex, int columnIndex) {
                Customer customer = customerList.get(rowIndex);
                switch (columnIndex) {
                    case 0:
                        return customer.getId();
                    case 1:
                        return customer.getFirstName() + " " + customer.getLastName();
                    case 2:
                        return customer.getUsedPoint();
                    default:
                        return "";
                }
            }
        };
        tblShowfiltr.setModel(model);
    }

    private void initTableForTotalPrice() {
        model = new AbstractTableModel() {
            String[] colNames = {"DATE", "Total Price"};

            @Override
            public String getColumnName(int column) {
                return colNames[column]; // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/OverriddenMethodBody
            }

            @Override
            public int getRowCount() {
                return recieptList.size();
            }

            @Override
            public int getColumnCount() {
                return 2;
            }

            @Override
            public Object getValueAt(int rowIndex, int columnIndex) {
                Reciept Reciept = recieptList.get(rowIndex);
                switch (columnIndex) {
                    case 0:
                        SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
                        return dateFormat.format(Reciept.getRe_date());
                    case 1:
                        return Reciept.getTotal_price_day();
                    default:
                        return "";
                }
            }
        };
        tblShowfiltr.setModel(model);
    }

    private void initTableForDailySale() {
        model = new AbstractTableModel() {
            String[] colNames = {"DATE", "QTY"};

            @Override
            public String getColumnName(int column) {
                return colNames[column];
            }

            @Override
            public int getRowCount() {
                return recieptList.size();
            }

            @Override
            public int getColumnCount() {
                return 2;
            }

            @Override
            public Object getValueAt(int rowIndex, int columnIndex) {
                Reciept Reciept = recieptList.get(rowIndex);
                switch (columnIndex) {
                    case 0:
                        SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
                        return dateFormat.format(Reciept.getRe_date());
                    case 1:
                        return Reciept.getCountReciept();
                    default:
                        return "";
                }
            }
        };
        tblShowfiltr.setModel(model);
    }

    private void initTableForProduct() {
        model = new AbstractTableModel() {
            String[] colNames = {"Name", "Date", "QTY"};

            @Override
            public String getColumnName(int column) {
                return colNames[column];
            }

            @Override
            public int getRowCount() {
                return recieptDetailList.size();
            }

            @Override
            public int getColumnCount() {
                return 3;
            }

            @Override
            public Object getValueAt(int rowIndex, int columnIndex) {
                RecieptDetail recieptDetail = recieptDetailList.get(rowIndex);
                switch (columnIndex) {
                    case 0:
                        return recieptDetail.getProductName();
                    case 1:
                        SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
                        return dateFormat.format(recieptDetail.getRe_date());
                    case 2:
                        return recieptDetail.getProduct_qty_day();
                    default:
                        return "";
                }
            }
        };
        tblShowfiltr.setModel(model);
    }

    private void initPieChartForCustomer() {
        pieDataset = new DefaultPieDataset();
        JFreeChart chart = ChartFactory.createPieChart("Top Ten Customer Used Point", pieDataset, true, true, false);
        ChartPanel chartPanel = new ChartPanel(chart);
        chartPanel.setPreferredSize(new java.awt.Dimension(400, 350));

        pnlPieGraph.removeAll();
        pnlPieGraph.add(chartPanel);
        pnlPieGraph.revalidate();
        pnlPieGraph.repaint();
    }

    private void initPieChartForTotalPrice(String Date) {
        pieDataset = new DefaultPieDataset();
        JFreeChart chart = ChartFactory.createPieChart(Date, // chart title
                pieDataset, // data
                true, // include legend
                true,
                false);
        ChartPanel chartPanel = new ChartPanel(chart);
        chartPanel.setPreferredSize(new java.awt.Dimension(400, 350));
        pnlPieGraph.removeAll();
        pnlPieGraph.add(chartPanel);
        pnlPieGraph.revalidate();
        pnlPieGraph.repaint();
    }

    private void initPieChartForRecieptPerMonth(String Date) {
        pieDataset = new DefaultPieDataset();
        JFreeChart chart = ChartFactory.createPieChart(Date, // chart title
                pieDataset, // data
                true, // include legend
                true,
                false);
        ChartPanel chartPanel = new ChartPanel(chart);
        chartPanel.setPreferredSize(new java.awt.Dimension(400, 350));
        pnlPieGraph.removeAll();
        pnlPieGraph.add(chartPanel);
        pnlPieGraph.revalidate();
        pnlPieGraph.repaint();
    }

    private void initPieChartForProduct(String Date) {
        pieDataset = new DefaultPieDataset();
        JFreeChart chart = ChartFactory.createPieChart(Date, // chart title
                pieDataset, // data
                true, // include legend
                true,
                false);
        ChartPanel chartPanel = new ChartPanel(chart);
        chartPanel.setPreferredSize(new java.awt.Dimension(400, 350));
        pnlPieGraph.removeAll();
        pnlPieGraph.add(chartPanel);
        pnlPieGraph.revalidate();
        pnlPieGraph.repaint();
    }

    private void loadPieDatasetForCustomer() {
        pieDataset.clear();
        for (Customer a : customerList) {
            pieDataset.setValue(a.getFirstName(), a.getPoint());
        }
    }

    private void loadPieDatasetForTotalPrice() {
        pieDataset.clear();
        for (Reciept a : recieptList) {
            SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
            String x = dateFormat.format(a.getRe_date());
            pieDataset.setValue(x, a.getTotal_price_day());
        }
    }

    private void loadPieDatasetForRecieptPerMonth() {
        pieDataset.clear();
        for (Reciept a : recieptList) {
            SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
            String x = dateFormat.format(a.getRe_date());
            pieDataset.setValue(x, a.getCountReciept());
        }
    }

    private void loadPieDatasetForProduct() {
        pieDataset.clear();
        for (RecieptDetail a : recieptDetailList) {
            SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
            String x = dateFormat.format(a.getRe_date());
            pieDataset.setValue(a.getProductName(), a.getProduct_qty_day());
        }
    }

    private void initBarChartForCustomer() {
        barDataset = new DefaultCategoryDataset();
        JFreeChart chart = ChartFactory.createBarChart(
                "Top Ten Total Customer Used Point",
                "Customer",
                "Used Point",
                barDataset,
                PlotOrientation.VERTICAL,
                true, true, false);
        ChartPanel chartPanel = new ChartPanel(chart);
        chartPanel.setPreferredSize(new java.awt.Dimension(600, 350));
        pnlBarGraph.removeAll();
        pnlBarGraph.add(chartPanel);
        pnlPieGraph.revalidate();
        pnlPieGraph.repaint();
    }

    private void initBarChartForTotalPrice(String Date) {
        barDataset = new DefaultCategoryDataset();
        JFreeChart chart = ChartFactory.createBarChart(
                Date,
                "Date",
                "Total Price",
                barDataset,
                PlotOrientation.VERTICAL,
                true, true, false);
        ChartPanel chartPanel = new ChartPanel(chart);
        chartPanel.setPreferredSize(new java.awt.Dimension(600, 350));
        pnlBarGraph.removeAll();
        pnlBarGraph.add(chartPanel);
        pnlPieGraph.revalidate();
        pnlPieGraph.repaint();
    }

    private void initBarChartForRecieptPerMonth(String Date) {
        barDataset = new DefaultCategoryDataset();
        JFreeChart chart = ChartFactory.createBarChart(
                Date,
                "Date",
                "Total Reciept",
                barDataset,
                PlotOrientation.VERTICAL,
                true, true, false);
        ChartPanel chartPanel = new ChartPanel(chart);
        chartPanel.setPreferredSize(new java.awt.Dimension(600, 350));
        pnlBarGraph.removeAll();
        pnlBarGraph.add(chartPanel);
        pnlPieGraph.revalidate();
        pnlPieGraph.repaint();
    }

    private void initBarChartForProduct(String Date) {
        barDataset = new DefaultCategoryDataset();
        JFreeChart chart = ChartFactory.createBarChart(
                Date,
                "Date",
                "Total Reciept",
                barDataset,
                PlotOrientation.VERTICAL,
                true, true, false);
        ChartPanel chartPanel = new ChartPanel(chart);
        chartPanel.setPreferredSize(new java.awt.Dimension(600, 350));
        pnlBarGraph.removeAll();
        pnlBarGraph.add(chartPanel);
        pnlPieGraph.revalidate();
        pnlPieGraph.repaint();
    }

    private void loadBarDatasetForCustomer() {
        barDataset.clear();
        for (Customer c : customerList) {
            barDataset.setValue(c.getPoint(), "Point Now", c.getFirstName());
        }
        for (Customer a : customerList) {
            barDataset.setValue(a.getUsedPoint(), "Used Point", a.getFirstName());
        }
    }

    private void loadBarDatasetForTotalPrice() {
        barDataset.clear();
        for (Reciept r : recieptList) {
            SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
            String x = dateFormat.format(r.getRe_date());
            barDataset.setValue(r.getRe_total(), "Total Price", x);
        }
    }

    private void loadBarDatasetForRecieptPerMonth() {
        barDataset.clear();
        for (Reciept r : recieptList) {
            SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
            String x = dateFormat.format(r.getRe_date());
            barDataset.setValue(r.getCountReciept(), "Receipt", x);
        }
    }

    private void loadBarDatasetForProduct() {
        barDataset.clear();
        for (RecieptDetail r : recieptDetailList) {
            SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
            String x = dateFormat.format(r.getRe_date());
            barDataset.setValue(r.getProduct_qty_day(), "Product Sales", r.getProductName());
        }
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel2 = new javax.swing.JPanel();
        jPanel1 = new javax.swing.JPanel();
        jPanel3 = new javax.swing.JPanel();
        btnProduct = new javax.swing.JButton();
        btnCustomer = new javax.swing.JButton();
        btnTotalPrice = new javax.swing.JButton();
        btnReceipt = new javax.swing.JButton();
        jLabel2 = new javax.swing.JLabel();
        jPanel4 = new javax.swing.JPanel();
        pnlPieGraph = new javax.swing.JPanel();
        pnlBarGraph = new javax.swing.JPanel();
        pnlDatePicker1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        pnlDatePicker2 = new javax.swing.JPanel();
        btnProcess = new javax.swing.JButton();
        cmbSell = new javax.swing.JComboBox<>();
        jPanel5 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        tblShowfiltr = new javax.swing.JTable();

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 100, Short.MAX_VALUE)
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 100, Short.MAX_VALUE)
        );

        jPanel1.setBackground(new java.awt.Color(71, 49, 49));

        jPanel3.setBackground(new java.awt.Color(30, 57, 50));

        btnProduct.setText("Product Sales");
        btnProduct.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnProductActionPerformed(evt);
            }
        });

        btnCustomer.setText("Customer Point");
        btnCustomer.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCustomerActionPerformed(evt);
            }
        });

        btnTotalPrice.setText("Total Price");
        btnTotalPrice.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnTotalPriceActionPerformed(evt);
            }
        });

        btnReceipt.setText("Receipt Per Day");
        btnReceipt.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnReceiptActionPerformed(evt);
            }
        });

        jLabel2.setFont(new java.awt.Font("Sitka Text", 0, 48)); // NOI18N
        jLabel2.setForeground(new java.awt.Color(255, 255, 204));
        jLabel2.setText("Circulation");

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel3Layout.createSequentialGroup()
                .addGap(143, 143, 143)
                .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 254, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(150, 150, 150)
                .addComponent(btnReceipt, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnTotalPrice, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnCustomer, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnProduct, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel3Layout.createSequentialGroup()
                .addContainerGap(45, Short.MAX_VALUE)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnProduct)
                    .addComponent(btnCustomer)
                    .addComponent(btnTotalPrice)
                    .addComponent(btnReceipt))
                .addContainerGap())
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel3Layout.createSequentialGroup()
                .addGap(0, 0, Short.MAX_VALUE)
                .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 60, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        jPanel4.setBackground(new java.awt.Color(255, 255, 255));

        pnlPieGraph.setBackground(new java.awt.Color(255, 255, 255));

        pnlBarGraph.setBackground(new java.awt.Color(255, 255, 255));

        pnlDatePicker1.setBackground(new java.awt.Color(255, 255, 255));

        jLabel1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel1.setText("To");

        pnlDatePicker2.setBackground(new java.awt.Color(255, 255, 255));

        btnProcess.setText("Process");
        btnProcess.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnProcessActionPerformed(evt);
            }
        });

        cmbSell.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Per Day", "Per Month", "Per Year" }));
        cmbSell.setToolTipText("");
        cmbSell.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cmbSellActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel4Layout.createSequentialGroup()
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(pnlDatePicker1, javax.swing.GroupLayout.PREFERRED_SIZE, 206, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(pnlDatePicker2, javax.swing.GroupLayout.PREFERRED_SIZE, 202, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnProcess, javax.swing.GroupLayout.PREFERRED_SIZE, 91, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(cmbSell, javax.swing.GroupLayout.PREFERRED_SIZE, 90, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addGap(27, 27, 27)
                        .addComponent(pnlBarGraph, javax.swing.GroupLayout.PREFERRED_SIZE, 569, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(pnlPieGraph, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                .addContainerGap())
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(pnlDatePicker1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(pnlDatePicker2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(btnProcess, javax.swing.GroupLayout.DEFAULT_SIZE, 34, Short.MAX_VALUE)
                    .addComponent(cmbSell))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(pnlPieGraph, javax.swing.GroupLayout.DEFAULT_SIZE, 333, Short.MAX_VALUE)
                    .addComponent(pnlBarGraph, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap(22, Short.MAX_VALUE))
        );

        jPanel5.setBackground(new java.awt.Color(30, 57, 50));

        tblShowfiltr.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane1.setViewportView(tblShowfiltr);

        javax.swing.GroupLayout jPanel5Layout = new javax.swing.GroupLayout(jPanel5);
        jPanel5.setLayout(jPanel5Layout);
        jPanel5Layout.setHorizontalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane1)
                .addGap(369, 369, 369))
        );
        jPanel5Layout.setVerticalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 132, Short.MAX_VALUE)
                .addContainerGap())
        );

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(0, 0, 0)
                .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGap(0, 0, 0))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void btnProcessActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnProcessActionPerformed
        String pattern = "yyyy-MM-dd";
        SimpleDateFormat formater = new SimpleDateFormat(pattern);
        System.out.println("" + formater.format(model1.getValue()) + " " + formater.format(model2.getValue()));
        String begin = formater.format(model1.getValue());
        String end = formater.format(model2.getValue());
        setProcess(begin, end);
    }//GEN-LAST:event_btnProcessActionPerformed

    private void setProcess(String begin, String end) {
        if (cmbSell.getSelectedItem() == "Per Day") {
            if (RP == true) {
                recieptService = new RecieptService();
                recieptList = recieptService.getDailySale(begin, end);
                RecieptPerDay("Total Receipt Per Day");
                model.fireTableDataChanged();
                loadBarDatasetForRecieptPerMonth();
                loadPieDatasetForRecieptPerMonth();
            } else if (PS == true) {
                recieptService = new RecieptService();
                recieptDetailList = recieptService.getProductDay(begin, end);
                ProductPerDay("Product Sales Per Day");
                model.fireTableDataChanged();
                loadBarDatasetForProduct();
                loadPieDatasetForProduct();
            } else if (TP == true) {
                recieptService = new RecieptService();
                recieptList = recieptService.getTotalPrice(begin, end);
                TotalPricePerDay("Total Price Per Day");
                model.fireTableDataChanged();
                loadBarDatasetForTotalPrice();
                loadPieDatasetForTotalPrice();
            }
        } else if (cmbSell.getSelectedItem() == "Per Month") {
            if (RP == true) {
                recieptService = new RecieptService();
                recieptList = recieptService.getMonthSale(begin, end);
                RecieptPerDay("Total Receipt Per Month");
                model.fireTableDataChanged();
                loadBarDatasetForRecieptPerMonth();
                loadPieDatasetForRecieptPerMonth();
            } else if (PS == true) {
                recieptService = new RecieptService();
                recieptDetailList = recieptService.getProductMonth(begin, end);
                ProductPerDay("Product Sales Per Month");
                model.fireTableDataChanged();
                loadBarDatasetForProduct();
                loadPieDatasetForProduct();
            } else if (TP == true) {
                recieptService = new RecieptService();
                recieptList = recieptService.getTotalPricePerMonth(begin, end);
                TotalPricePerDay("Total Price Per Month");
                model.fireTableDataChanged();
                loadBarDatasetForTotalPrice();
                loadPieDatasetForTotalPrice();
            }
        } else if (cmbSell.getSelectedItem() == "Per Year") {
            if (RP == true) {
                recieptService = new RecieptService();
                recieptList = recieptService.getYearSale(begin, end);
                RecieptPerDay("Total Receipt Per Year");
                model.fireTableDataChanged();
                loadBarDatasetForRecieptPerMonth();
                loadPieDatasetForRecieptPerMonth();
            } else if (PS == true) {
                recieptService = new RecieptService();
                recieptDetailList = recieptService.getProductYear(begin, end);
                ProductPerDay("Product Sales Per Year");
                model.fireTableDataChanged();
                loadBarDatasetForProduct();
                loadPieDatasetForProduct();
            } else if (TP == true) {
                recieptService = new RecieptService();
                recieptList = recieptService.getTotalPricePerYear(begin, end);
                TotalPricePerDay("Total Price Per Year");
                model.fireTableDataChanged();
                loadBarDatasetForTotalPrice();
                loadPieDatasetForTotalPrice();
            }
        }
    }

    private void btnTotalPriceActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnTotalPriceActionPerformed
        enableForm(true);
        recieptService = new RecieptService();
        recieptList = recieptService.getDailySale();
        TotalPricePerDay("Total Price Per Day");
        TP = true;
        CP = false;
        RP = false;
        PS = false;
    }//GEN-LAST:event_btnTotalPriceActionPerformed

    private void btnCustomerActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCustomerActionPerformed
        enableForm(false);
        customerService = new CustomerService();
        customerList = customerService.getTopTenCustomerByuserdPoint();
        TopTenCustomer();
        CP = true;
        TP = false;
        RP = false;
        PS = false;
    }//GEN-LAST:event_btnCustomerActionPerformed

    private void btnReceiptActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnReceiptActionPerformed
        enableForm(true);
        recieptService = new RecieptService();
        recieptList = recieptService.getDailySale();
        RecieptPerDay("Total Receipt Per Day");
        CP = false;
        TP = false;
        RP = true;
        PS = false;
    }//GEN-LAST:event_btnReceiptActionPerformed

    private void cmbSellActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cmbSellActionPerformed
        ActivityForCmb();
    }//GEN-LAST:event_cmbSellActionPerformed

    private void ActivityForCmb() {
        if (cmbSell.getSelectedItem() == "Per Day") {
            if (RP == true) {
                recieptService = new RecieptService();
                recieptList = recieptService.getDailySale();
                RecieptPerDay("Total Receipt Per Day");
            } else if (PS == true) {
                recieptService = new RecieptService();
                recieptDetailList = recieptService.getProductDay();
                ProductPerDay("Product Sales Per Day");
            } else if (TP == true) {
                recieptService = new RecieptService();
                recieptList = recieptService.getTotalPrice();
                TotalPricePerDay("Total Price Per Day");
            } else if (CP == true) {
                customerService = new CustomerService();
                customerList = customerService.getTopTenCustomerByuserdPoint();
                TopTenCustomer();
            }
        } else if (cmbSell.getSelectedItem() == "Per Month") {
            if (RP == true) {
                recieptService = new RecieptService();
                recieptList = recieptService.getMonthSale();
                RecieptPerDay("Total Receipt Per Month");
            } else if (PS == true) {
                recieptService = new RecieptService();
                recieptDetailList = recieptService.getProductMonth();
                ProductPerDay("Product Sales Per Month");
            } else if (TP == true) {
                recieptService = new RecieptService();
                recieptList = recieptService.getTotalPricePerMonth();
                TotalPricePerDay("Total Price Per Month");
            } else if (CP == true) {
                customerService = new CustomerService();
                customerList = customerService.getTopTenCustomerByuserdPoint();
                TopTenCustomer();
            }
        } else if (cmbSell.getSelectedItem() == "Per Year") {
            if (RP == true) {
                recieptService = new RecieptService();
                recieptList = recieptService.getYearSale();
                RecieptPerDay("Total Receipt Per Year");
            } else if (PS == true) {
                recieptService = new RecieptService();
                recieptDetailList = recieptService.getProductYear();
                ProductPerDay("Product Sales Per Year");
            } else if (TP == true) {
                recieptService = new RecieptService();
                recieptList = recieptService.getTotalPricePerYear();
                TotalPricePerDay("Total Price Per Year");
            } else if (CP == true) {
                customerService = new CustomerService();
                customerList = customerService.getTopTenCustomerByuserdPoint();
                TopTenCustomer();
            }
        }
    }

    private void btnProductActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnProductActionPerformed
        enableForm(true);
        CP = false;
        TP = false;
        RP = false;
        PS = true;
        recieptService = new RecieptService();
        recieptDetailList = recieptService.getProductDay();
        ProductPerDay("Product Sales Per Day");
    }//GEN-LAST:event_btnProductActionPerformed

    public void enableForm(boolean status) {
        btnProcess.setEnabled(status);
        cmbSell.setVisible(status);
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnCustomer;
    private javax.swing.JButton btnProcess;
    private javax.swing.JButton btnProduct;
    private javax.swing.JButton btnReceipt;
    private javax.swing.JButton btnTotalPrice;
    private javax.swing.JComboBox<String> cmbSell;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JPanel pnlBarGraph;
    private javax.swing.JPanel pnlDatePicker1;
    private javax.swing.JPanel pnlDatePicker2;
    private javax.swing.JPanel pnlPieGraph;
    private javax.swing.JTable tblShowfiltr;
    // End of variables declaration//GEN-END:variables
}
