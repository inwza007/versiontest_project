/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/GUIForms/JPanel.java to edit this template
 */
package com.mycompany.projectdcoffee.ui.stock;

import com.mycompany.projectdcoffee.ui.stock.StockPrint;
import com.mycompany.projectdcoffee.service.StockService;
import com.mycompany.projectdcoffee.model.Stock;
import java.awt.GridLayout;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.ArrayList;
import java.util.List;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.SwingUtilities;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;

/**
 *
 * @author test1
 */
public class StockPanel extends javax.swing.JPanel {

    private StockService stockService;
    public TableModel model;
    public List<Stock> stockList;
    private Stock Stock;
    /**
     * Creates new form StockPenel
     */
    public StockPanel() {

        initComponents();
        
        
        
        
        stockService = new StockService();
        stockList = stockService.getStocks();
        tblStock.setRowHeight(30);
        model = new AbstractTableModel() {
            String[] colNames = {"ID","Name","Quantity","Minimum","Unit","Status"};
            @Override
            public String getColumnName(int column) {
                return colNames[column];
            }
            
            @Override
            public int getRowCount() {
                return stockList.size();
            }

            @Override
            public int getColumnCount() {
                return 6;
            }

            @Override
            public Object getValueAt(int rowIndex, int columnIndex) {
                Stock artist = stockList.get(rowIndex);
                switch(columnIndex){
                    case 0 :
                        return artist.getId();
                    case 1 :
                        return artist.getName();
                    case 2 :
                        return artist.getQty();
                    case 3 :
                        return artist.getMinimum();
                    case 4 :
                        return artist.getUnit();   
                    case 5 :
                        return artist.getStatus();
                    default:
                        return "" ;
                }
            }
        };
        tblStock.setModel(model);
        
        
        
        
        
        
        
        
        
        
//        userService = new UserService();
//        tblUser.setRowHeight(100);
//        tblUser.setModel(new AbstractTableModel() {
//            String[] columnNames = {"ID", "Login", "Name", "Password", "Gender", "Role"};
//
//            @Override
//            public String getColumnName(int column) {
//                return columnNames[column];
//            }
//
//            @Override
//            public int getRowCount() {
//                return list.size();
//            }
//
//            @Override
//            public int getColumnCount() {
//                return 6;
//            }
//
//            @Override
//            public Class<?> getColumnClass(int columnIndex) {
//                switch (columnIndex) {
//                    case 0:
//                        return ImageIcon.class;
//                    default:
//                        return String.class;
//                }
//            }
//
//            @Override
//            public Object getValueAt(int rowIndex, int columnIndex) {
//                User user = list.get(rowIndex);
//                switch (columnIndex) {
//                    case 0:
//                        ImageIcon icon = new ImageIcon("./user" + user.getId() + ".png");
//                        Image image = icon.getImage();
//                        int width = image.getWidth(null);
//                        int height = image.getHeight(null);
//                        Image newImage = image.getScaledInstance((int) (100 * (float) width / height), 100, Image.SCALE_SMOOTH);
//                        icon.setImage(newImage);
//                        return icon;
//                    case 1:
//                        return user.getId();
//                    case 2:
//                        return user.getLogin();
//                    case 3:
//                        return user.getName();
//                    case 4:
//                        return user.getPassword();
//                    case 5:
//                        return user.getGender();
//                    case 6:
//                        return user.getRole();
//                    default:
//                        return "Unknown";
//                }
//            }
//        });///////////////////////////////////////////////////

//        tblUser.setRowHeight(100);
//        Stock newStock = new Stock(1, "Med Coffee", 20, 10, "Kg", "Full");
//        stockservice.addStock(newStock);
//        tableStock.setModel(new AbstractTableModel() {
//            @Override
//            public int getRowCount() {
//                return list.size();
//            }
//
//            @Override
//            public int getColumnCount() {
//                return 6;
//            }
//            
//            @Override
//            public Object getValueAt(int rowIndex, int columnIndex) {
//                Stock stock = list.get(rowIndex);
//                if(columnIndex==0){
//                    return stock.getId();
//                }else if(columnIndex==1){
//                    return stock.getName();   
//                }else if(columnIndex==2){
//                    return stock.getQty();   
//                }else if(columnIndex==3){
//                    return stock.getMinimum();   
//                }else if(columnIndex==4){
//                    return stock.getUnit();   
//                }else if(columnIndex==5){
//                    return stock.getStatus();   
//                }
//                return "";
//               
//            }
//            String[] columnNames = {"ID", "Login", "Name", "Password", "Gender", "Role"};
//            @Override
//            public String getColumnName(int column) {
//                switch (column) {
//                    case 0:
//                        return "ID";
//                    case 1:
//                        return "NAME";
//                    case 2:
//                        return "QTY"; 
//                    case 3:
//                        return "MINIMUM";
//                    case 4:
//                        return "UNIT";
//                    case 5:
//                        return "STATUS";
//                    default:
//                        break;
//                }
//                return "";
//            }
//            
//        });
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel2 = new javax.swing.JPanel();
        jLabel5 = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        tblStock = new javax.swing.JTable();
        jPanel1 = new javax.swing.JPanel();
        btnPrint = new javax.swing.JButton();
        btnEdit = new javax.swing.JButton();
        btnRefresh = new javax.swing.JButton();

        setBackground(new java.awt.Color(71, 49, 49));

        jPanel2.setBackground(new java.awt.Color(30, 57, 50));

        jLabel5.setFont(new java.awt.Font("Sitka Text", 0, 48)); // NOI18N
        jLabel5.setForeground(new java.awt.Color(255, 255, 204));
        jLabel5.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel5.setText("Stock Management");

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel5, javax.swing.GroupLayout.DEFAULT_SIZE, 970, Short.MAX_VALUE)
                .addContainerGap())
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGap(21, 21, 21)
                .addComponent(jLabel5, javax.swing.GroupLayout.PREFERRED_SIZE, 60, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(21, Short.MAX_VALUE))
        );

        tblStock.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        tblStock.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4", "Title5", "Title6", "Title7"
            }
        ));
        jScrollPane1.setViewportView(tblStock);

        jPanel1.setBackground(new java.awt.Color(30, 57, 50));

        btnPrint.setText("Print");
        btnPrint.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnPrintActionPerformed(evt);
            }
        });

        btnEdit.setText("Edit");
        btnEdit.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnEditActionPerformed(evt);
            }
        });

        btnRefresh.setText("Refresh");
        btnRefresh.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnRefreshActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(btnRefresh, javax.swing.GroupLayout.PREFERRED_SIZE, 127, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(btnEdit, javax.swing.GroupLayout.PREFERRED_SIZE, 127, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnPrint, javax.swing.GroupLayout.PREFERRED_SIZE, 127, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap(44, Short.MAX_VALUE)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnPrint, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnEdit, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnRefresh, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 982, Short.MAX_VALUE)
                    .addComponent(jPanel2, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 432, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
    }// </editor-fold>//GEN-END:initComponents

    private void btnEditActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnEditActionPerformed
        JFrame frame = (JFrame) SwingUtilities.getRoot(this);
        StockEditDialog stockEditDialog = new StockEditDialog(frame, true);
//        stockEditDialog.addWindowListener(new WindowAdapter() {
//        });
        stockEditDialog.setVisible(true);
    }//GEN-LAST:event_btnEditActionPerformed

    private void btnPrintActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnPrintActionPerformed
        JFrame frame = (JFrame) SwingUtilities.getRoot(this);
        StockPrint stockPrint = new StockPrint(frame, true);
//        stockPrint.addWindowListener(new WindowAdapter() {
//        });
        stockPrint.setVisible(true);
    }//GEN-LAST:event_btnPrintActionPerformed

    private void btnRefreshActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnRefreshActionPerformed
        ((AbstractTableModel) tblStock.getModel()).fireTableStructureChanged();    
        tblStock.revalidate();
        tblStock.repaint();
        stockService = new StockService();
        stockList = stockService.getStocks();
        tblStock.setRowHeight(30);
        model = new AbstractTableModel() {
            String[] colNames = {"ID","Name","Quantity","Minimum","Unit","Status"};
            @Override
            public String getColumnName(int column) {
                return colNames[column];
            }
            
            @Override
            public int getRowCount() {
                return stockList.size();
            }

            @Override
            public int getColumnCount() {
                return 6;
            }

            @Override
            public Object getValueAt(int rowIndex, int columnIndex) {
                Stock artist = stockList.get(rowIndex);
                switch(columnIndex){
                    case 0 :
                        return artist.getId();
                    case 1 :
                        return artist.getName();
                    case 2 :
                        return artist.getQty();
                    case 3 :
                        return artist.getMinimum();
                    case 4 :
                        return artist.getUnit();   
                    case 5 :
                        return artist.getStatus();
                    default:
                        return "" ;
                }
            }
        };
        tblStock.setModel(model);

    }//GEN-LAST:event_btnRefreshActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnEdit;
    private javax.swing.JButton btnPrint;
    private javax.swing.JButton btnRefresh;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTable tblStock;
    // End of variables declaration//GEN-END:variables


}
