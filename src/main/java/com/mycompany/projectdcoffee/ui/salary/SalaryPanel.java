/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/GUIForms/JPanel.java to edit this template
 */
package com.mycompany.projectdcoffee.ui.salary;

import com.mycompany.projectdcoffee.dao.SalaryDao;
import com.mycompany.projectdcoffee.model.CheckTime;
import com.mycompany.projectdcoffee.service.SalaryService;
import com.mycompany.projectdcoffee.service.UserService;
import com.mycompany.projectdcoffee.model.Salary;
import com.mycompany.projectdcoffee.model.User;
import com.mycompany.projectdcoffee.service.CheckTimeService;
import java.awt.Font;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import static java.lang.Thread.sleep;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;
import javax.swing.table.TableRowSorter;

/**
 *
 * @author Admin
 */
public class SalaryPanel extends javax.swing.JPanel {

    ArrayList<Salary> salaries;
    ArrayList<CheckTime> checktimes;
    private TableModel model;
    private List<User> users;
    SalaryService salaryService = new SalaryService();
    UserService userService = new UserService();
    CheckTimeService chacktimeservice = new CheckTimeService();
    User user;
    Salary salary;
    CheckTime checktime;
    private ArrayList<User> list;

    /**
     * Creates new form SalaryPanel
     */
    public SalaryPanel() {
        initComponents();
        initUserTable();
        initSalarytable();

    }

    private void initUserTable() {
        users = userService.getUserId();
        salaries = salaryService.getSalarys();
        checktimes = chacktimeservice.getCheckTimeOrderByName();

        tblUser.getTableHeader().setFont(new Font("TH Sarabun New", Font.PLAIN, 18));
        tblUser.setRowHeight(30);
        tblUser.setModel(new AbstractTableModel() {
            String[] columnNames = {"ID", "First Name", "LastName", "Date", "Approve"};

            @Override
            public String getColumnName(int column) {
                return columnNames[column];
            }

            @Override
            public int getRowCount() {
                return users.size();
            }

            @Override
            public int getColumnCount() {
                return 5;
            }

            @Override
            public Object getValueAt(int rowIndex, int columnIndex) {
                User user = users.get(rowIndex);
                Salary sala = salaries.get(rowIndex);
                switch (columnIndex) {
                    case 0:
                        return user.getId();
                    case 1:
                        return user.getFname();
                    case 2:
                        return user.getLname();
                    case 3:
                        return sala.getPrDate();
                    case 4:
                        return sala.getPrApprove();
                    default:
                        return "";
                }
            }
        });
        Set<Integer> clickedRows = new HashSet<>();

        tblUser.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                int row = tblUser.rowAtPoint(e.getPoint());
                int col = tblUser.columnAtPoint(e.getPoint());
                if (!clickedRows.contains(row)) {
                    clickedRows.add(row);
                    User user = users.get(row);
                    Salary sala = salaries.get(row);
                    CheckTime ct = checktimes.get(row);
                    Salary sl = new Salary(sala.getPrId(), user.getFullName(), sala.getPrPricePerHour(),
                            sala.getWhTotalTime(), sala.getPrTotalSalary());
                    salary.addSalaryDetail(sl);
                    tblSalary.revalidate();
                    tblSalary.repaint();
                    lblTotalSalary.setText("TotalSalary: " + salary.getPrTotalSalary());
                    salaryService.updateTotalTime(sala.getPrId());
                }

            }

        });
    }

    private void initSalarytable() {
        salary = new Salary();
        tblSalary.setRowHeight(30);
        tblSalary.setModel(new AbstractTableModel() {
            String[] headers = {"Payroll ID", "Name", "PricePerHour", "Total Time Work", "Salary"};

            @Override
            public String getColumnName(int column) {
                return headers[column];
            }

            @Override
            public int getRowCount() {
                return salary.getSalaryDetails().size();
            }

            @Override
            public int getColumnCount() {
                return 5;
            }

            @Override
            public Object getValueAt(int rowIndex, int columnIndex) {
                ArrayList<Salary> salaryDetails = salary.getSalaryDetails();
                Salary salaryDetail = salaryDetails.get(rowIndex);
                switch (columnIndex) {
                    case 0:
                        return salaryDetail.getPrId();
                    case 1:
                        return salaryDetail.getFullname();
                    case 2:
                        return salaryDetail.getPrPricePerHour();
                    case 3:
                        return salaryDetail.getWhTotalTime();
                    case 4:
                        return salaryDetail.getPrTotalSalary();
                    default:
                        return "";

                }
            }

        });
        tblSalary.revalidate();
        tblSalary.repaint();
    }

    
    private void refreshTable() {
        users = userService.getUserId();
        salaries = salaryService.getSalarys();       
        tblUser.revalidate();
        tblUser.repaint();
        lblTotalSalary.setText("TotalSalary: 0");
    }

    private void openDialog() {
        JFrame frame = (JFrame) SwingUtilities.getRoot(this);
        SalaryDialog salaryDialog = new SalaryDialog(frame, true);
        salaryDialog.setLocationRelativeTo(this);
        salaryDialog.setVisible(true);
    }
    
    private void openInvoice(int i) {
        JFrame frame = (JFrame) SwingUtilities.getRoot(this);
        SalaryInvoice salaryInvoice = new SalaryInvoice(frame, i);
        salaryInvoice.setLocationRelativeTo(this);
        salaryInvoice.setVisible(true);
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated
    // <editor-fold defaultstate="collapsed" desc="Generated
    // Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jPanel2 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        tblUser = new javax.swing.JTable();
        jScrollPane2 = new javax.swing.JScrollPane();
        tblSalary = new javax.swing.JTable();
        jPanel3 = new javax.swing.JPanel();
        btnSave = new javax.swing.JButton();
        lblTotalSalary = new javax.swing.JLabel();
        jPanel4 = new javax.swing.JPanel();
        jLabel2 = new javax.swing.JLabel();
        btnProflie = new javax.swing.JButton();

        setBackground(new java.awt.Color(71, 49, 49));

        jPanel1.setBackground(new java.awt.Color(30, 57, 50));

        jPanel2.setBackground(new java.awt.Color(30, 57, 50));

        tblUser.setModel(new javax.swing.table.DefaultTableModel(
                new Object[][] {
                        { null, null, null, null },
                        { null, null, null, null },
                        { null, null, null, null },
                        { null, null, null, null }
                },
                new String[] {
                        "Title 1", "Title 2", "Title 3", "Title 4"
                }));
        jScrollPane1.setViewportView(tblUser);

        tblSalary.setModel(new javax.swing.table.DefaultTableModel(
                new Object[][] {
                        { null, null, null, null },
                        { null, null, null, null },
                        { null, null, null, null },
                        { null, null, null, null }
                },
                new String[] {
                        "Title 1", "Title 2", "Title 3", "Title 4"
                }));
        jScrollPane2.setViewportView(tblSalary);

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
                jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(jPanel2Layout.createSequentialGroup()
                                .addContainerGap()
                                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 428,
                                        javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 415, Short.MAX_VALUE)
                                .addContainerGap()));
        jPanel2Layout.setVerticalGroup(
                jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                                .addContainerGap()
                                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                        .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 442,
                                                Short.MAX_VALUE)
                                        .addComponent(jScrollPane1))
                                .addContainerGap()));

        jPanel3.setBackground(new java.awt.Color(30, 57, 50));

        btnSave.setFont(new java.awt.Font("Segoe UI", 0, 20)); // NOI18N
        btnSave.setText("Pay");
        btnSave.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSaveActionPerformed(evt);
            }
        });

        lblTotalSalary.setBackground(new java.awt.Color(255, 255, 204));
        lblTotalSalary.setFont(new java.awt.Font("TH Sarabun New", 0, 24)); // NOI18N
        lblTotalSalary.setForeground(new java.awt.Color(255, 255, 204));
        lblTotalSalary.setText("TotalSalary : 0");

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
                jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(jPanel3Layout.createSequentialGroup()
                                .addContainerGap()
                                .addComponent(lblTotalSalary)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED,
                                        javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(btnSave, javax.swing.GroupLayout.PREFERRED_SIZE, 103,
                                        javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addContainerGap()));
        jPanel3Layout.setVerticalGroup(
                jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(jPanel3Layout.createSequentialGroup()
                                .addGroup(jPanel3Layout
                                        .createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                        .addGroup(jPanel3Layout.createSequentialGroup()
                                                .addComponent(btnSave, javax.swing.GroupLayout.DEFAULT_SIZE,
                                                        javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                                .addGap(5, 5, 5))
                                        .addGroup(javax.swing.GroupLayout.Alignment.LEADING,
                                                jPanel3Layout.createSequentialGroup()
                                                        .addContainerGap()
                                                        .addComponent(lblTotalSalary)))
                                .addContainerGap(31, Short.MAX_VALUE)));

        jPanel4.setBackground(new java.awt.Color(30, 57, 50));

        jLabel2.setFont(new java.awt.Font("Sitka Text", 0, 48)); // NOI18N
        jLabel2.setForeground(new java.awt.Color(255, 255, 204));
        jLabel2.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel2.setText("List of employees");

        btnProflie.setText("Profile");
        btnProflie.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnProflieActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
                jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(jLabel2, javax.swing.GroupLayout.Alignment.TRAILING,
                                javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE,
                                Short.MAX_VALUE)
                        .addGroup(jPanel4Layout.createSequentialGroup()
                                .addContainerGap()
                                .addComponent(btnProflie)
                                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)));
        jPanel4Layout.setVerticalGroup(
                jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(jPanel4Layout.createSequentialGroup()
                                .addGap(24, 24, 24)
                                .addComponent(jLabel2)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED,
                                        javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(btnProflie)
                                .addContainerGap()));

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
                jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(jPanel1Layout.createSequentialGroup()
                                .addContainerGap()
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addComponent(jPanel4, javax.swing.GroupLayout.DEFAULT_SIZE,
                                                javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                        .addComponent(jPanel2, javax.swing.GroupLayout.Alignment.TRAILING,
                                                javax.swing.GroupLayout.DEFAULT_SIZE,
                                                javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                        .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE,
                                                javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                                .addContainerGap()));
        jPanel1Layout.setVerticalGroup(
                jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(jPanel1Layout.createSequentialGroup()
                                .addContainerGap()
                                .addComponent(jPanel4, javax.swing.GroupLayout.PREFERRED_SIZE,
                                        javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE,
                                        javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE,
                                        javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addContainerGap()));

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
                layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(layout.createSequentialGroup()
                                .addContainerGap()
                                .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE,
                                        javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addContainerGap()));
        layout.setVerticalGroup(
                layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(layout.createSequentialGroup()
                                .addContainerGap()
                                .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE,
                                        javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addContainerGap()));
    }// </editor-fold>//GEN-END:initComponents

    private void btnProflieActionPerformed(java.awt.event.ActionEvent evt) {// GEN-FIRST:event_btnProflieActionPerformed
        openDialog();
    }// GEN-LAST:event_btnProflieActionPerformed

    private void btnSaveActionPerformed(java.awt.event.ActionEvent evt) {// GEN-FIRST:event_btnSaveActionPerformed
        int response = JOptionPane.showConfirmDialog(this, "Do you want to continue ?", "Confirm",
                JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
        if (response == JOptionPane.YES_OPTION) {
            for (int i = 0; i < tblSalary.getRowCount(); i++) {
                //ssalaryService.updateAfpay((salaries.get(i)).getPrId());
                salaryService.updateAfpay(salary.getSalaryDetails().get(i).getPrId());
                openInvoice(i);
            }
            initSalarytable();
            refreshTable();
        } else if (response == JOptionPane.CLOSED_OPTION) {
            refreshTable();
        }

    }// GEN-LAST:event_btnSaveActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnProflie;
    private javax.swing.JButton btnSave;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JLabel lblTotalSalary;
    private javax.swing.JTable tblSalary;
    private javax.swing.JTable tblUser;
    // End of variables declaration//GEN-END:variables
}
