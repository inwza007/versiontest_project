/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/GUIForms/JPanel.java to edit this template
 */
package com.mycompany.projectdcoffee.ui.salary;

import com.mycompany.projectdcoffee.dao.UserDao;
import com.mycompany.projectdcoffee.model.CheckTime;
import com.mycompany.projectdcoffee.model.User;
import com.mycompany.projectdcoffee.service.CheckTimeService;
import com.mycompany.projectdcoffee.service.UserService;
import com.mycompany.projectdcoffee.ui.LoginCheckTimeDialog;
import com.mycompany.projectdcoffee.ui.LoginCheckTimeDialog;
import com.mycompany.projectdcoffee.ui.MainMenuManager;
import com.mycompany.projectdcoffee.ui.MainMenuManager;
import java.awt.Font;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import static java.lang.Thread.sleep;
import java.sql.Time;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;
import static javax.swing.WindowConstants.DISPOSE_ON_CLOSE;
import static javax.swing.WindowConstants.DO_NOTHING_ON_CLOSE;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author Phattharaphon
 */
public class AttendancePanel extends javax.swing.JPanel {

    private MainMenuManager mm;
    public final User user;
    ArrayList<CheckTime> checkTimes;
    private CheckTime editedCheckTime;
    CheckTimeService checkTimeService = new CheckTimeService();
    CheckTime checkTime;
    UserDao userDao = new UserDao();
    UserService userService = new UserService();
    Date currentDate = new Date();
    public String date;
    public String timeIn;
    public String timeOut;
    public CheckTime newCheckTime;
    public boolean status = false;

    private User getCurrentUser() {
        return user;
    }

    public void clock() {
        Thread clock = new Thread() {
            public void run() {
                try {
                    for (;;) {
                        Calendar cal = new GregorianCalendar();
                        Date currentDate = cal.getTime(); // This line gets the current date and time as a Date object

                        // Format the date and time if needed
                        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
                        SimpleDateFormat timeFormat = new SimpleDateFormat("HH:mm:ss");
                        String formattedDate = dateFormat.format(currentDate);
                        String formattedTime = timeFormat.format(currentDate);
                        setD(formattedDate);
                        setT(formattedTime);
                        setTimeOut(formattedTime);

                        lblClock.setText("Date : " + formattedDate);
                        lblClock1.setText("Time : " + formattedTime);
                        sleep(1000);
                    }

                } catch (InterruptedException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            }
        };
        clock.start();
    }

    public String getD() {
        return date;
    }

    public void setD(String d) {
        this.date = d;
    }

    public String getT() {
        return timeIn;
    }

    public void setT(String t) {
        this.timeIn = t;
    }

    public String getTimeOut() {
        return timeOut;
    }

    public void setTimeOut(String timeOut) {
        this.timeOut = timeOut;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    /**
     * Creates new form AttendancePanel
     */
    public AttendancePanel(User user) {
//        super(parent, modal);
        initComponents();
        this.user = user;
        clock();
        lblFullName.setText("User : " + getCurrentUser().getFullName());
        lblRole.setText("Role : " + getCurrentUser().getRole());
        checkTimes = checkTimeService.getCheckTimeOrderByName();
        tblChecktime.setRowHeight(50);
        tblChecktime.getTableHeader().setFont(new Font("TH Sarabun New", Font.PLAIN, 18));
        tblChecktime.setModel(new AbstractTableModel() {
            String[] headers = {"ID", "User Name", "Date", "TimeIn", "TimeOut", "TotalTime"};

            @Override
            public String getColumnName(int column) {
                return headers[column];
            }

            @Override
            public int getRowCount() {
                return checkTimes.size();
            }

            @Override
            public int getColumnCount() {
                return 6;
            }

            @Override
            public Object getValueAt(int rowIndex, int columnIndex) {
                CheckTime checkTime = checkTimes.get(rowIndex);
                User user = userDao.get(checkTime.getUserId());
                switch (columnIndex) {
                    case 0:
                        return checkTime.getUserId();
                    case 1:
                        return user.getFullName();
                    case 2:
                        return checkTime.getWhLoginDate();
                    case 3:
                        return checkTime.getWhLoginTime();
                    case 4:
                        return checkTime.getWhLogoutTime();
                    case 5:
                        return checkTime.getWhTotalTime();
                    default:
                        return "";
                }
            }
        });
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jPanel2 = new javax.swing.JPanel();
        lblClock = new javax.swing.JLabel();
        btnCheckIn = new javax.swing.JButton();
        btnCheckOut = new javax.swing.JButton();
        lblClock1 = new javax.swing.JLabel();
        lblClock2 = new javax.swing.JLabel();
        lblFullName = new javax.swing.JLabel();
        lblRole = new javax.swing.JLabel();
        jPanel4 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        tblChecktime = new javax.swing.JTable();
        jPanel6 = new javax.swing.JPanel();
        jLabel7 = new javax.swing.JLabel();

        jPanel1.setBackground(new java.awt.Color(71, 49, 49));

        jPanel2.setBackground(new java.awt.Color(255, 255, 255));

        lblClock.setFont(new java.awt.Font("TH Sarabun New", 0, 24)); // NOI18N
        lblClock.setText("Date : ");

        btnCheckIn.setBackground(new java.awt.Color(102, 255, 102));
        btnCheckIn.setText("Check in");
        btnCheckIn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCheckInActionPerformed(evt);
            }
        });

        btnCheckOut.setBackground(new java.awt.Color(255, 102, 102));
        btnCheckOut.setText("Check out");
        btnCheckOut.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCheckOutActionPerformed(evt);
            }
        });

        lblClock1.setFont(new java.awt.Font("TH Sarabun New", 0, 24)); // NOI18N
        lblClock1.setText("Clock : ");

        lblClock2.setFont(new java.awt.Font("TH Sarabun New", 0, 24)); // NOI18N
        lblClock2.setText(" Work attendance : ");

        lblFullName.setText("User :");

        lblRole.setText("Status :");

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(lblClock2)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnCheckIn, javax.swing.GroupLayout.PREFERRED_SIZE, 124, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnCheckOut, javax.swing.GroupLayout.PREFERRED_SIZE, 109, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 221, Short.MAX_VALUE)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(lblFullName, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(lblRole, javax.swing.GroupLayout.DEFAULT_SIZE, 171, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(lblClock, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(lblClock1, javax.swing.GroupLayout.DEFAULT_SIZE, 184, Short.MAX_VALUE))
                .addContainerGap())
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(btnCheckOut, javax.swing.GroupLayout.PREFERRED_SIZE, 37, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(btnCheckIn, javax.swing.GroupLayout.PREFERRED_SIZE, 37, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(lblClock2, javax.swing.GroupLayout.PREFERRED_SIZE, 29, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(12, 12, 12))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(lblClock, javax.swing.GroupLayout.PREFERRED_SIZE, 29, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(lblFullName, javax.swing.GroupLayout.PREFERRED_SIZE, 29, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(lblClock1, javax.swing.GroupLayout.PREFERRED_SIZE, 29, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(lblRole, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
        );

        jPanel4.setBackground(new java.awt.Color(255, 255, 255));

        jScrollPane1.setBackground(new java.awt.Color(255, 255, 255));

        tblChecktime.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane1.setViewportView(tblChecktime);

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane1)
                .addContainerGap())
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 435, Short.MAX_VALUE)
                .addContainerGap())
        );

        jPanel6.setBackground(new java.awt.Color(30, 57, 50));

        jLabel7.setFont(new java.awt.Font("Sitka Text", 0, 48)); // NOI18N
        jLabel7.setForeground(new java.awt.Color(255, 255, 204));
        jLabel7.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel7.setText("Working Attendance ");

        javax.swing.GroupLayout jPanel6Layout = new javax.swing.GroupLayout(jPanel6);
        jPanel6.setLayout(jPanel6Layout);
        jPanel6Layout.setHorizontalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jLabel7, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        jPanel6Layout.setVerticalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel6Layout.createSequentialGroup()
                .addContainerGap(23, Short.MAX_VALUE)
                .addComponent(jLabel7, javax.swing.GroupLayout.PREFERRED_SIZE, 60, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(19, 19, 19))
        );

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel6, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel6, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 994, Short.MAX_VALUE)
            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(layout.createSequentialGroup()
                    .addGap(0, 0, 0)
                    .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGap(0, 0, 0)))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 649, Short.MAX_VALUE)
            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(layout.createSequentialGroup()
                    .addGap(0, 0, 0)
                    .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGap(0, 0, 0)))
        );
    }// </editor-fold>//GEN-END:initComponents

    private boolean hasUserCheckedIn() {
        for (CheckTime ct : checkTimes) {
            if (ct.getUserId() == getCurrentUser().getId() && ct.getWhLoginDate().equals(date)) {
                return true; // User has checked in for today
            }
        }
        return false; // User hasn't checked in for today
    }

    private boolean userAlreadyCheckedIn(CheckTime checkTime) {
        for (CheckTime ct : checkTimes) {
            if (ct.getUserId() == checkTime.getUserId() && ct.getWhLoginDate().equals(checkTime.getWhLoginDate())) {
                return true;
            }
        }
        return false;
    }

    private void btnCheckInActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCheckInActionPerformed
        openLoginCheckTimeDialog(user);
        checkTime = new CheckTime();
        checkTime.setUserId(getCurrentUser().getId());
        checkTime.setWhLoginDate(date);
        checkTime.setWhLoginTime(timeIn);
        if (userAlreadyCheckedIn(checkTime)) {
            JOptionPane.showMessageDialog(this, "You've checked your name.", "Working attendance",
                    JOptionPane.WARNING_MESSAGE);
        } else {
            newCheckTime = checkTimeService.addNew(checkTime);
            checkTimes.add(newCheckTime);
refreshTable();
            System.out.println("Test");
            JOptionPane.showMessageDialog(this, "You have successfully checked your entry name.");
        }

    }//GEN-LAST:event_btnCheckInActionPerformed

    private void btnCheckOutActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCheckOutActionPerformed
        int selectedRow = tblChecktime.getSelectedRow();
        if (selectedRow >= 0 && selectedRow < checkTimes.size()) {
            CheckTime selectedCheckTime = checkTimes.get(selectedRow);
            openLoginCheckTimeDialog(user);
            if (selectedCheckTime.getUserId() == getCurrentUser().getId()) {
                processCheckOut(selectedCheckTime);
            } else {
                // แจ้งเตือนว่าไม่สามารถ Check Out ได้
                JOptionPane.showMessageDialog(this, "You can't Check Out for another user.", "Invalid Check Out",
                        JOptionPane.ERROR_MESSAGE);
            }
        } else {
            System.err.println("No row selected or invalid selection.");
        }
    }

    private void openLoginCheckTimeDialog(User user) {
        JFrame frame = (JFrame) SwingUtilities.getRoot(this);
        LoginCheckTimeDialog loginCheckTimeDialog = new LoginCheckTimeDialog(frame, true, this);
        loginCheckTimeDialog.setLocationRelativeTo(this);
        loginCheckTimeDialog.setVisible(true);
    }

    private void refreshTable() {
        tblChecktime.revalidate();
        tblChecktime.repaint();
        checkTimes = checkTimeService.getCheckTimeOrderByName();
        tblChecktime.setRowHeight(50);
        tblChecktime.getTableHeader().setFont(new Font("TH Sarabun New", Font.PLAIN, 18));
        tblChecktime.setModel(new AbstractTableModel() {
            String[] headers = {"ID", "User Name", "Date", "TimeIn", "TimeOut", "TotalTime"};

            @Override
            public String getColumnName(int column) {
                return headers[column];
            }

            @Override
            public int getRowCount() {
                return checkTimes.size();
            }

            @Override
            public int getColumnCount() {
                return 6;
            }

            @Override
            public Object getValueAt(int rowIndex, int columnIndex) {
                CheckTime checkTime = checkTimes.get(rowIndex);
                User user = userDao.get(checkTime.getUserId());
                switch (columnIndex) {
                    case 0:
                        return checkTime.getUserId();
                    case 1:
                        return user.getFullName();
                    case 2:
                        return checkTime.getWhLoginDate();
                    case 3:
                        return checkTime.getWhLoginTime();
                    case 4:
                        return checkTime.getWhLogoutTime();
                    case 5:
                        return checkTime.getWhTotalTime();
                    default:
                        return "";
                }
            }
        });
    }

    private void processCheckOut(CheckTime selectedCheckTime) {
        String timeOut = getT();

        selectedCheckTime.setWhLogoutTime(timeOut);

        Time timeIn = Time.valueOf(selectedCheckTime.getWhLoginTime());
        Time timeOutValue = Time.valueOf(selectedCheckTime.getWhLogoutTime());
        long totalTimeMillis = timeOutValue.getTime() - timeIn.getTime();
        int totalTimeMinutes = (int) (totalTimeMillis / 60000); // นาที

        int totalTimeHours = totalTimeMinutes / 60;

        selectedCheckTime.setWhTotalTime(totalTimeHours);

        checkTimeService.update(selectedCheckTime);
        refreshTable();
        JOptionPane.showMessageDialog(this, "You have successfully verified your work name.");

    }//GEN-LAST:event_btnCheckOutActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnCheckIn;
    private javax.swing.JButton btnCheckOut;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel6;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JLabel lblClock;
    private javax.swing.JLabel lblClock1;
    private javax.swing.JLabel lblClock2;
    private javax.swing.JLabel lblFullName;
    private javax.swing.JLabel lblRole;
    private javax.swing.JTable tblChecktime;
    // End of variables declaration//GEN-END:variables
}
