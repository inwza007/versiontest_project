/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/GUIForms/JPanel.java to edit this template
 */
package com.mycompany.projectdcoffee.ui.POS;

import com.mycompany.databaseproject.component.BuyProductable;
import com.mycompany.databaseproject.component.AllProductListPanel;
import com.mycompany.databaseproject.component.ProductitemPanel;
import com.mycompany.projectdcoffee.dao.CustomerDao;
import static com.mycompany.projectdcoffee.dao.CustomerDao.getByTelandPoint;
import com.mycompany.projectdcoffee.dao.PromotionDao;
import static com.mycompany.projectdcoffee.dao.PromotionDao.fetchOtherData;

import com.mycompany.projectdcoffee.dao.UserDao;
import com.mycompany.projectdcoffee.model.Customer;
import com.mycompany.projectdcoffee.model.Product;
import com.mycompany.projectdcoffee.model.Promotion;
import com.mycompany.projectdcoffee.model.Reciept;
import com.mycompany.projectdcoffee.model.RecieptDetail;
import com.mycompany.projectdcoffee.model.User;
import com.mycompany.projectdcoffee.service.CustomerService;
import com.mycompany.projectdcoffee.service.ProductService;
import com.mycompany.projectdcoffee.service.PromotionService;
import com.mycompany.projectdcoffee.service.RecieptDetailService;
import com.mycompany.projectdcoffee.service.RecieptService;
import com.mycompany.projectdcoffee.service.UserService;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import static java.lang.Thread.sleep;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import static java.util.Collections.list;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Map;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;

/**
 *
 * @author GAMING PC
 */
public class POSPanel extends javax.swing.JPanel implements BuyProductable {

    ArrayList<Product> products;
    ProductService productService = new ProductService();
    RecieptService recieptService = new RecieptService();
    CustomerService customerService = new CustomerService();
    private ArrayList<BuyProductable> subscribers = new ArrayList<>();
    public int usePoint;
    public float s;
    public int sum = 0;
    public int rowP = 1;
    public int rowM = 1;
    public boolean StatusMember = false;
    public boolean StatusPromotion = false;
    public int totalDiscount = 0;
    public int totalDiscountPromotion = 0;
    public int promotion_id;

    public int idR;
    public int totalDiscountMember = 0;
    public int a = -1;
    public float totalPrice;
    Reciept reciept;
    DefaultTableModel tblModel;
    Customer customer;
    User user;
    public float totalprice;
    private AllProductListPanel allproductListPanel;
    private String payType;
    public String productSubType;
    public String time;
    public String date;
    public int day;
    public int month;
    public int year;
    public int second, minute, hour;
    public String selectvalue;
    private int totalPriceUsePoint;
    public int Totalnet = 0;
    public String Promotionname;
    public int PromotionDiscount;
    public int totalNotDiscound;
     public int cusid;
    public boolean back = false;
    public DefaultTableModel model = new DefaultTableModel();

    private User getCurrentUser() {
        return user;
    }

    /**
     * Creates new form testPOSPanel
     */
    public POSPanel(User user) {
        this.user = user;
        initComponents();
        tblRecieptDetail.setModel(model);
        clock();
        tblModel = (DefaultTableModel) tblDiscount.getModel();
        tblModel.setRowCount(0);
        tblModel.addRow(new Object[]{"ToTal", sum, "",});
        reciept = new Reciept();
        reciept.setUser(getCurrentUser());
        reciept.setUser_id(getCurrentUser().getId());
        reciept.setCustomer(customerService.getByTel("0887679156"));
        customer = customerService.getByTel("0887679156");
        enableForm(false);
        rdbPromotion.setEnabled(false);
        cmdPromotion.setEnabled(false);

        JComboBox<String> comboBox = cmdPromotion;
        PromotionDao.populateComboBox(comboBox);

        tblRecieptDetail.setRowHeight(50);
        tblRecieptDetail.setModel(new AbstractTableModel() {
            String[] headers = {"Name", "Size", "Sweet Level", "Type", "Price", "Qty", "Total"};

            @Override
            public String getColumnName(int column) {
                return headers[column];
            }

            @Override
            public int getRowCount() {
                return reciept.getRecieptDetails().size();
            }

            @Override
            public int getColumnCount() {
                return 7;
            }

            @Override
            public Object getValueAt(int rowIndex, int columnIndex) {
                ArrayList<RecieptDetail> recieptDetails = reciept.getRecieptDetails();
                RecieptDetail recieptDetail = recieptDetails.get(rowIndex);
                switch (columnIndex) {
                    case 0:

                        return recieptDetail.getProductName();
                    case 2:
                        return recieptDetail.getProductSweet();
                    case 1:
                        return recieptDetail.getProductSize();
                    case 3:
                        return recieptDetail.getProductSubType();
                    case 4:
                        return recieptDetail.getProductPrice();
                    case 5:
                        return recieptDetail.getQty();
                    case 6:
                        totalprice = recieptDetail.getTotalPrice();
                        return recieptDetail.getTotalPrice();
                    default:
                        return "";

                }

            }

            @Override
            public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
                ArrayList<RecieptDetail> recieptDetails = reciept.getRecieptDetails();
                RecieptDetail recieptDetail = recieptDetails.get(rowIndex);

                if (columnIndex == 5) {
                    int qty = Integer.parseInt((String) aValue);
                    if (qty < 1) {
                        return;
                    }
                    recieptDetail.setQty(qty);
                    reciept.calculateTotal();
                    refreshReciept();

                }

            }

            @Override
            public boolean isCellEditable(int rowIndex, int columnIndex) {
                switch (columnIndex) {
                    case 5:
                        return true;
                    default:
                        return false;
                }
            }

        });
        allproductListPanel = new AllProductListPanel(this, "all");
        allproductListPanel.addOnBuyProduct(this);
        ProductList.setViewportView(allproductListPanel);

    }

    public void clock() {
        Thread clock = new Thread() {
            public void run() {
                try {
                    for (;;) {
                        Calendar cal = new GregorianCalendar();

                        int day = cal.get(Calendar.DAY_OF_MONTH);
                        setDay(day);
                        int month = cal.get(Calendar.MONTH) + 1;
                        setMonth(month);
                        int year = cal.get(Calendar.YEAR);
                        setYear(year);

                        int second = cal.get(Calendar.SECOND);
                        setSecond(second);
                        int minute = cal.get(Calendar.MINUTE);
                        setMinute(minute);
                        int hour = cal.get(Calendar.HOUR_OF_DAY);
                        setHour(hour);

                        String time = String.format("%02d:%02d:%02d", hour, minute, second);
                        sleep(1000);
                    }
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        };
        clock.start();
    }

    private void refreshReciept() {
        a = a + 1;
        Object value = tblRecieptDetail.getValueAt(a, 5);
        sum = sum + ((Number) value).intValue();
        totalNotDiscound = (int) reciept.getRe_total();
        tblRecieptDetail.revalidate();
        tblRecieptDetail.repaint();
        Totalnet = (int) reciept.getRe_total();
        lblTotal.setText("Total: " + Totalnet);
        System.out.println(s);
        tblModel.setRowCount(0);
        tblModel.addRow(new Object[]{"ToTal", sum, "", Totalnet});
        tblDiscount.repaint();

    }

    private void clearReciept() {
        reciept = new Reciept();
        refreshReciept();
    }

    public void addOnBuyProduct(BuyProductable subscriber) {
        subscribers.add(subscriber);

    }

    public String checkMember() {
        String tel = txtPhone.getText();
        String point = Integer.toString(CustomerDao.getByTelandPoint(tel));
        cusid = CustomerDao.getIDByTel(tel);

        boolean checkPhone = CustomerDao.getByLoginByTel(tel);
        {
            if (checkPhone) {
                JOptionPane.showMessageDialog(btnSendPoint, tel);
                ShowPoint.setText(point);
                rdbPromotion.setEnabled(true);

            } else {
                JOptionPane.showMessageDialog(btnSendPoint, "Not found");
            }
        }
        return tel;
    }

    public void usePointMember() {

        if (txtPoint.getText().equals("")) {
            JOptionPane.showMessageDialog(null, "Please ");

        } else {

            String textPoint = txtPoint.getText();
            String tel = txtPhone.getText();
            int Point = CustomerDao.getByTelandPoint(tel);
            int usePoint = Integer.parseInt(textPoint);
            int total = Point - usePoint;
            if (total < 0) {
                JOptionPane.showMessageDialog(null, "Error");

            } else {
                CustomerDao customerDao = new CustomerDao();
                int choice = JOptionPane.showConfirmDialog(null, "Do you want to proceed?", "Confirmation", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
                if (choice == JOptionPane.YES_OPTION) {
                    Customer updatedCustomer = customerDao.updatePoint(tel, total);
                    ShowPoint.setText(Integer.toString(total));
                    txtPoint.setText("");
                    txtPoint.setEnabled(false);
                    btnSendPoint.setEnabled(false);
                    btnSendPhone.setEnabled(false);
                    totalDiscountMember = (usePoint / 10);
                    reciept.setRe_discount(totalDiscount);
                    Totalnet = (int) (Totalnet - totalDiscountMember);
                    this.totalPrice = totalPriceUsePoint;
                    totalDiscount = totalDiscount + totalDiscountMember;
                    Object scount[] = {"Member Point", usePoint, totalDiscount, Totalnet};
                    tblModel.addRow(scount);

                    reciept.setRe_net(Totalnet);

                    customerDao.updatePoint(tel, total);

                    lblTotal.setText("Total: " + Totalnet);
                    if (rowM == 1) {
                        rowP = 2;
                    } else {
                        rowM = 2;
                    }
                    StatusMember = true;

                } else {
                    System.out.println("User chose No.");
                }
            }

        }

    }

    public int getIdR() {
        return reciept.getReciept_id();
    }

    public void setIdR(int idR) {
        this.idR = idR;
    }

    public int getTotalDiscountMember() {
        return totalDiscountMember;
    }

    public void setTotalDiscountMember(int totalDiscountMember) {
        this.totalDiscountMember = totalDiscountMember;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public int getDay() {
        return day;
    }

    public void setDay(int day) {
        this.day = day;
    }

    public int getMonth() {
        return month;
    }

    public void setMonth(int month) {
        this.month = month;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public int getSecond() {
        return second;
    }

    public void setSecond(int second) {
        this.second = second;
    }

    public int getMinute() {
        return minute;
    }

    public void setMinute(int minute) {
        this.minute = minute;
    }

    public int getHour() {
        return hour;
    }

    public void setHour(int hour) {
        this.hour = hour;
    }

    public String getPromotionname() {
        return Promotionname;
    }

    public void setPromotionname(String Promotionname) {
        this.Promotionname = Promotionname;
    }

    public int getPromotionDiscount() {
        return PromotionDiscount;
    }

    public void setPromotionDiscount(int PromotionDiscount) {
        this.PromotionDiscount = PromotionDiscount;
    }

    public int getTotalnet() {
        return Totalnet;
    }

    public void setTotalnet(int Totalnet) {
        this.Totalnet = Totalnet;
    }

    public int getTotalNotDiscound() {
        return totalNotDiscound;
    }

    public void setTotalNotDiscound(int totalNotDiscound) {
        this.totalNotDiscound = totalNotDiscound;
    }

    public int getCusid() {
        return cusid;
    }

    public void setCusid(int cusid) {
        this.cusid = cusid;
    }
    

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        ProductList = new javax.swing.JScrollPane();
        pnlProductList = new javax.swing.JPanel();
        tblProductList = new javax.swing.JScrollPane();
        tblRecieptDetail = new javax.swing.JTable();
        jPanel4 = new javax.swing.JPanel();
        jLabel2 = new javax.swing.JLabel();
        jPanel5 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        cmbFillter = new javax.swing.JComboBox<>();
        jPanel2 = new javax.swing.JPanel();
        jButton2 = new javax.swing.JButton();
        jButton1 = new javax.swing.JButton();
        lblTotal = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        tblDiscount = new javax.swing.JTable();
        jPanel6 = new javax.swing.JPanel();
        rdbMember = new javax.swing.JRadioButton();
        txtPhone = new javax.swing.JTextField();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        btnSendPhone = new javax.swing.JButton();
        rdbPromotion = new javax.swing.JRadioButton();
        cmdPromotion = new javax.swing.JComboBox<>();
        jLabel5 = new javax.swing.JLabel();
        txtPoint = new javax.swing.JTextField();
        btnSendPoint = new javax.swing.JButton();
        btnDelete = new javax.swing.JButton();
        ShowPoint = new javax.swing.JLabel();
        jButton3 = new javax.swing.JButton();

        jPanel1.setBackground(new java.awt.Color(71, 49, 49));

        ProductList.setBackground(new java.awt.Color(255, 255, 255));

        pnlProductList.setBackground(new java.awt.Color(255, 255, 255));

        javax.swing.GroupLayout pnlProductListLayout = new javax.swing.GroupLayout(pnlProductList);
        pnlProductList.setLayout(pnlProductListLayout);
        pnlProductListLayout.setHorizontalGroup(
            pnlProductListLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 588, Short.MAX_VALUE)
        );
        pnlProductListLayout.setVerticalGroup(
            pnlProductListLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 642, Short.MAX_VALUE)
        );

        ProductList.setViewportView(pnlProductList);

        tblRecieptDetail.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        tblProductList.setViewportView(tblRecieptDetail);

        jPanel4.setBackground(new java.awt.Color(30, 57, 50));

        jLabel2.setFont(new java.awt.Font("Segoe UI", 0, 18)); // NOI18N
        jLabel2.setForeground(new java.awt.Color(255, 255, 255));
        jLabel2.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel2.setText("Order");

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel4Layout.createSequentialGroup()
                .addContainerGap(16, Short.MAX_VALUE)
                .addComponent(jLabel2)
                .addContainerGap())
        );

        jPanel5.setBackground(new java.awt.Color(30, 57, 50));

        jLabel1.setFont(new java.awt.Font("Segoe UI", 0, 18)); // NOI18N
        jLabel1.setForeground(new java.awt.Color(255, 255, 255));
        jLabel1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel1.setText("Product");

        cmbFillter.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "All", "Drink", "Dessert" }));
        cmbFillter.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cmbFillterActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel5Layout = new javax.swing.GroupLayout(jPanel5);
        jPanel5.setLayout(jPanel5Layout);
        jPanel5Layout.setHorizontalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(cmbFillter, javax.swing.GroupLayout.PREFERRED_SIZE, 106, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 311, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel5Layout.setVerticalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(cmbFillter, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(14, Short.MAX_VALUE))
        );

        jPanel2.setBackground(new java.awt.Color(30, 57, 50));

        jButton2.setBackground(new java.awt.Color(102, 255, 102));
        jButton2.setText("Confirm");
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });

        jButton1.setBackground(new java.awt.Color(255, 102, 102));
        jButton1.setText("Cancel");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        lblTotal.setBackground(new java.awt.Color(255, 255, 255));
        lblTotal.setFont(new java.awt.Font("Segoe UI", 0, 24)); // NOI18N
        lblTotal.setForeground(new java.awt.Color(255, 255, 255));
        lblTotal.setText("TOTAL :");

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(lblTotal, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGap(56, 56, 56)
                .addComponent(jButton1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jButton2)
                .addContainerGap())
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE, false)
                    .addComponent(lblTotal, javax.swing.GroupLayout.DEFAULT_SIZE, 45, Short.MAX_VALUE)
                    .addComponent(jButton2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jButton1))
                .addContainerGap())
        );

        tblDiscount.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Topic", "Qty", "Discount", "Total"
            }
        ));
        jScrollPane1.setViewportView(tblDiscount);

        jPanel6.setBackground(new java.awt.Color(255, 255, 255));

        rdbMember.setText("Member");
        rdbMember.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                rdbMemberActionPerformed(evt);
            }
        });

        jLabel3.setText("Phone : ");

        jLabel4.setText("Point : ");

        btnSendPhone.setText("Send");
        btnSendPhone.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSendPhoneActionPerformed(evt);
            }
        });

        rdbPromotion.setText("Promotion");
        rdbPromotion.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                rdbPromotionActionPerformed(evt);
            }
        });

        cmdPromotion.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "None" }));
        cmdPromotion.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cmdPromotionActionPerformed(evt);
            }
        });

        jLabel5.setText("Use point : ");

        btnSendPoint.setText("Send");
        btnSendPoint.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSendPointActionPerformed(evt);
            }
        });

        btnDelete.setText("Delete");
        btnDelete.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnDeleteActionPerformed(evt);
            }
        });

        ShowPoint.setBackground(new java.awt.Color(255, 255, 255));
        ShowPoint.setText("0");
        ShowPoint.setOpaque(true);

        jButton3.setText("+ Sing Up");
        jButton3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton3ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel6Layout = new javax.swing.GroupLayout(jPanel6);
        jPanel6.setLayout(jPanel6Layout);
        jPanel6Layout.setHorizontalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel6Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel6Layout.createSequentialGroup()
                        .addComponent(jLabel5)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtPoint, javax.swing.GroupLayout.PREFERRED_SIZE, 104, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnSendPoint, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel6Layout.createSequentialGroup()
                        .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(jLabel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jLabel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel6Layout.createSequentialGroup()
                                .addComponent(ShowPoint, javax.swing.GroupLayout.PREFERRED_SIZE, 101, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(0, 0, Short.MAX_VALUE))
                            .addGroup(jPanel6Layout.createSequentialGroup()
                                .addComponent(txtPhone, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(btnSendPhone, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
                    .addGroup(jPanel6Layout.createSequentialGroup()
                        .addComponent(rdbMember)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jButton3, javax.swing.GroupLayout.PREFERRED_SIZE, 95, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(rdbPromotion)
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel6Layout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(cmdPromotion, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel6Layout.createSequentialGroup()
                        .addGap(69, 69, 69)
                        .addComponent(btnDelete)))
                .addContainerGap())
        );
        jPanel6Layout.setVerticalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel6Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(rdbMember)
                    .addComponent(jButton3)
                    .addComponent(rdbPromotion)
                    .addComponent(cmdPromotion, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(10, 10, 10)
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtPhone, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel3)
                    .addComponent(btnSendPhone))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel4, javax.swing.GroupLayout.PREFERRED_SIZE, 22, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(ShowPoint))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(txtPoint, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(btnSendPoint)
                        .addComponent(btnDelete))
                    .addGroup(jPanel6Layout.createSequentialGroup()
                        .addComponent(jLabel5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGap(1, 1, 1)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(ProductList, javax.swing.GroupLayout.DEFAULT_SIZE, 568, Short.MAX_VALUE)
                    .addComponent(jPanel5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel2, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel6, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                    .addComponent(tblProductList, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                    .addComponent(jPanel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jPanel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(ProductList, javax.swing.GroupLayout.DEFAULT_SIZE, 579, Short.MAX_VALUE)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(tblProductList, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 79, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jPanel6, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGap(0, 0, 0))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jButton1ActionPerformed

    private void cmdPromotionActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cmdPromotionActionPerformed
        if (rdbPromotion.isSelected()) {
            String selectedName = (String) cmdPromotion.getSelectedItem();
            if (selectedName != null) {
                checkTimePromotion();
                if (checkTimePromotion() == true) {
                    Promotion otherData = fetchOtherData(selectedName);
                    System.out.println("Selected Name: " + selectedName);
                    System.out.println("Other Data: " + PromotionDao.fetchOtherData(selectedName));
                    totalDiscountPromotion = PromotionDao.fetchOtherData(selectedName).getPdiscount();
                    setName(selectedName);
                    Totalnet = Totalnet - totalDiscountPromotion;
                    totalDiscount = totalDiscount + totalDiscountPromotion;

                    Object scount[] = {PromotionDao.fetchOtherData(selectedName).getPname(), 1, PromotionDao.fetchOtherData(selectedName).getPdiscount(), Totalnet};
                    tblModel.addRow(scount);

                    setTotalNotDiscound(totalNotDiscound);
                    setPromotionname(selectedName);
                    setPromotionDiscount(totalDiscountPromotion);
                    lblTotal.setText("Total: " + Totalnet);
                    cmdPromotion.setEnabled(false);
                    StatusPromotion = true;
                    if (rowP == 1) {
                        rowM = 2;
                    } else {
                        rowP = 2;
                    }

                } else {
                    JOptionPane.showMessageDialog(ShowPoint, "ERROR");
                    cmdPromotion.setSelectedIndex(0);
                }
            }
        }
    }//GEN-LAST:event_cmdPromotionActionPerformed
    private boolean checkTimePromotion() {
        String selectedName = (String) cmdPromotion.getSelectedItem();
        System.out.println(selectedName);
        int startdate1 = 0;
        int startdate2 = 0;
        int startmonth1 = 0;
        int startmonth2 = 0;
        int enddate1 = 0;
        int enddate2 = 0;
        int endmonth1 = 0;
        int endmonth2 = 0;

        String datestart = PromotionDao.fetchOtherData(selectedName).getPstart();
        String dateend = PromotionDao.fetchOtherData(selectedName).getPend();

        if (selectedName.equals("Birthday")) {
            String tel = txtPhone.getText();
            String birthday = CustomerDao.getByTelandBirthday(tel);
            System.out.println(birthday);

            for (int i = 0; i < birthday.length(); i++) {
                char ch = birthday.charAt(i); // ดึงอักษรที่ตำแหน่ง i
                if (i == 9) {
                    startdate1 = Character.getNumericValue(ch);
                } else if (i == 8) {
                    startdate2 = Character.getNumericValue(ch);
                } else if (i == 6) {
                    startmonth1 = Character.getNumericValue(ch);
                } else if (i == 5) {
                    startmonth2 = Character.getNumericValue(ch);
                }

            }
            String dateB1 = String.valueOf(startdate1);
            String dateB2 = String.valueOf(startdate2);
            String monthB1 = String.valueOf(startmonth1);
            String monthB2 = monthB1.valueOf(startmonth2);
            String Birthdate = dateB2 + dateB1;
            String Birthmonth = monthB2 + monthB1;
            int Birthdates = Integer.parseInt(Birthdate);
            int Birthmonths = Integer.parseInt(Birthmonth);
            System.out.println(Birthdates);
            System.out.println(month);

            if (Birthdates == day && Birthmonths == month) {
                System.out.println("OK");
                return true;
            } else {
                System.out.println("NOT");
                return false;
            }

        } else if (!selectedName.equals("Birthday")) {
            for (int i = 0; i < datestart.length(); i++) {
                char ch = datestart.charAt(i);
                if (i == 9) {
                    startdate1 = Character.getNumericValue(ch);
                } else if (i == 8) {
                    startdate2 = Character.getNumericValue(ch);
                } else if (i == 6) {
                    startmonth1 = Character.getNumericValue(ch);
                } else if (i == 5) {
                    startmonth2 = Character.getNumericValue(ch);
                }

            }
            String dateB1 = String.valueOf(startdate1);
            String dateB2 = String.valueOf(startdate2);
            String monthB1 = String.valueOf(startmonth1);
            String monthB2 = monthB1.valueOf(startmonth2);
            String Startdate = dateB2 + dateB1;
            String Startmonth = monthB2 + monthB1;
            int Startdates = Integer.parseInt(Startdate);
            int Startmonths = Integer.parseInt(Startmonth);

            for (int i = 0; i < dateend.length(); i++) {
                char ch = dateend.charAt(i); // ดึงอักษรที่ตำแหน่ง i
                if (i == 9) {
                    enddate1 = Character.getNumericValue(ch);
                } else if (i == 8) {
                    enddate2 = Character.getNumericValue(ch);
                } else if (i == 6) {
                    endmonth1 = Character.getNumericValue(ch);
                } else if (i == 5) {
                    endmonth2 = Character.getNumericValue(ch);
                }

            }
            String dateEB1 = String.valueOf(enddate1);
            String dateEB2 = String.valueOf(enddate2);
            String monthEB1 = String.valueOf(endmonth1);
            String monthEB2 = monthB1.valueOf(endmonth2);
            String Edate = dateEB1 + dateEB2;
            String Emonth = monthEB1 + monthEB2;
            int Enddates = Integer.parseInt(Edate);
            int Endmonth = Integer.parseInt(Emonth);

            if (day >= Startdates && month >= Startmonths && day <= Enddates && month <= Endmonth) {
                System.out.println("OK");
                return true;
            } else {
                System.out.println("NOT");
                return false;
            }

        }
        return false;

    }

    private void rdbMemberActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_rdbMemberActionPerformed

        enableForm(true);
        if (!rdbMember.isSelected()) {
            clearMember();
        }
    }//GEN-LAST:event_rdbMemberActionPerformed

    private void clearMember() {

        if (StatusMember == true) {
            CustomerDao customerDao = new CustomerDao();
            Customer updatedCustomer = customerDao.updatePoint(txtPhone.getText(), Integer.parseInt(ShowPoint.getText()) + (totalDiscount * 10));
            tblModel.removeRow(rowM);

        }

        txtPhone.setText("");
        txtPoint.setText("");
        ShowPoint.setText("");
        enableForm(false);
        rdbPromotion.setEnabled(false);
        Totalnet = Totalnet + totalDiscount;
        reciept.setRe_net(Totalnet);
        lblTotal.setText("Total: " + Totalnet);

    }

    public void setPayType(String payType) {
        this.payType = payType;
    }

    private void rdbPromotionActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_rdbPromotionActionPerformed

        cmdPromotion.setEnabled(true);
        if (!rdbPromotion.isSelected()) {
            cmdPromotion.setEnabled(false);
            if (StatusPromotion = true) {
                tblModel.removeRow(rowP);
                Totalnet = Totalnet + totalDiscountPromotion;
                reciept.setRe_net(Totalnet);
                lblTotal.setText("Total: " + Totalnet);
                cmdPromotion.setEnabled(true);

            }
        }

    }//GEN-LAST:event_rdbPromotionActionPerformed

    private void btnSendPhoneActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSendPhoneActionPerformed
        checkMember();
        String tel = txtPhone.getText();
        reciept.setCustomer_id(CustomerDao.getIDByTel(tel));
        
    }//GEN-LAST:event_btnSendPhoneActionPerformed

    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed

        setPromotionId();
        if (reciept.getRe_total() <= 0) {
            JOptionPane.showMessageDialog(this, "Please Add Product!!");
        } else {
            System.out.println(reciept.getRe_total());
            if (!rdbMember.isSelected()) {
                reciept.setRe_net(reciept.getRe_total());

            }

            setTotalNotDiscound(Totalnet + totalDiscount);
            reciept.setRe_discount(totalDiscount);
            reciept.setRe_net(Totalnet);
            JFrame frame = (JFrame) SwingUtilities.getRoot(this);
            PayTypeDialog payTypeDialog = new PayTypeDialog(frame, totalprice, this);
            payTypeDialog.setLocationRelativeTo(this);
            System.out.println(totalDiscount);
            
            payTypeDialog.addWindowListener(new WindowAdapter() {
                @Override
                public void windowClosed(WindowEvent e) {
                    System.out.println(back);
                    if (back == true) {
                        return;
                    } else if (back == false) {
                        System.out.println(payType);

                        reciept.setRe_pay_type(payType);
                        System.out.println("" + reciept);
                        recieptService.addNew(reciept,promotion_id,productSubType);
                        openReceiptDialog(reciept.getReciept_id());
                    }
                }
            });
            payTypeDialog.setVisible(true);
            
        }


    }//GEN-LAST:event_jButton2ActionPerformed

    private void setPromotionId() {
        switch (cmdPromotion.getSelectedIndex()) {
            case 1:
                promotion_id = 1;
                break;
            case 2:
                promotion_id = 2;
                break;
            case 3:
                promotion_id = 3;
                break;
            case 4:
                promotion_id = 4;
                break;
            case 5:
                promotion_id = 5;
                break;
            default:
                break;
        }
    }
    private void openReceiptDialog(int id) {
        JFrame frame = (JFrame) SwingUtilities.getRoot(this);
        ReceiptDialog receiptDialog = new ReceiptDialog(frame, true, this, id);
        receiptDialog.setLocationRelativeTo(this);
        receiptDialog.setVisible(true);
    }

    private void btnSendPointActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSendPointActionPerformed
        usePointMember();
        totalPrice = Totalnet;
    }//GEN-LAST:event_btnSendPointActionPerformed

    private void btnDeleteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnDeleteActionPerformed
        int selectedIndex = tblRecieptDetail.getSelectedRow();

        if (selectedIndex >= 0) {
            ArrayList<RecieptDetail> recieptDetails = reciept.getRecieptDetails();
            RecieptDetail recieptDetail = recieptDetails.get(selectedIndex);
            double productPrice = recieptDetail.getProductPrice();
            int input = JOptionPane.showConfirmDialog(this, "Do you want to proceed?", "Select an Option", JOptionPane.YES_NO_OPTION, JOptionPane.ERROR_MESSAGE);

            if (input == 0) {
                Totalnet = (int) (Totalnet - productPrice);
                recieptDetails.remove(selectedIndex);
                tblRecieptDetail.revalidate();
                tblRecieptDetail.repaint();

                tblDiscount.revalidate();
                tblDiscount.repaint();
                sum = sum - 1;
                tblModel.setRowCount(0);
                tblModel.addRow(new Object[]{"ToTal", sum, "", Totalnet});
                tblDiscount.repaint();
                reciept.setRe_total(Totalnet);
                reciept.setRe_total_qty(sum);
                this.totalPrice = Totalnet;

                System.out.println(reciept);

                lblTotal.setText("Total: " + Totalnet);
            }
        }
    }//GEN-LAST:event_btnDeleteActionPerformed

    private void jButton3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton3ActionPerformed
        JFrame frame = (JFrame) SwingUtilities.getRoot(this);
        SignUpMemberDialog singUpMemberDialog = new SignUpMemberDialog(frame, totalprice, this);
        singUpMemberDialog.setLocationRelativeTo(this);
        singUpMemberDialog.addWindowListener(new WindowAdapter() {

        });
        singUpMemberDialog.setVisible(true);
    }//GEN-LAST:event_jButton3ActionPerformed

    private void cmbFillterActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cmbFillterActionPerformed
        if (cmbFillter.getSelectedIndex() == 0) {
            allproductListPanel = new AllProductListPanel(this, "all");
            allproductListPanel.addOnBuyProduct(this);
            ProductList.setViewportView(allproductListPanel);
        } else if (cmbFillter.getSelectedIndex() == 1) {
            allproductListPanel = new AllProductListPanel(this, "drink");
            allproductListPanel.addOnBuyProduct(this);
            ProductList.setViewportView(allproductListPanel);
        } else if (cmbFillter.getSelectedIndex() == 2) {
            allproductListPanel = new AllProductListPanel(this, "dessert");
            allproductListPanel.addOnBuyProduct(this);
            ProductList.setViewportView(allproductListPanel);;
        }
    }//GEN-LAST:event_cmbFillterActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JScrollPane ProductList;
    private javax.swing.JLabel ShowPoint;
    private javax.swing.JButton btnDelete;
    private javax.swing.JButton btnSendPhone;
    private javax.swing.JButton btnSendPoint;
    private javax.swing.JComboBox<String> cmbFillter;
    private javax.swing.JComboBox<String> cmdPromotion;
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton2;
    private javax.swing.JButton jButton3;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JPanel jPanel6;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JLabel lblTotal;
    private javax.swing.JPanel pnlProductList;
    private javax.swing.JRadioButton rdbMember;
    private javax.swing.JRadioButton rdbPromotion;
    private javax.swing.JTable tblDiscount;
    private javax.swing.JScrollPane tblProductList;
    private javax.swing.JTable tblRecieptDetail;
    private javax.swing.JTextField txtPhone;
    private javax.swing.JTextField txtPoint;
    // End of variables declaration//GEN-END:variables

    @Override
    public void buy(Product product, String size, int qty, String subStype) {
        reciept.addRecieptDetail(product, size, qty);
        refreshReciept();
    }

    public void enableForm(boolean status) {
        txtPhone.setEnabled(status);
        txtPoint.setEnabled(status);
        btnSendPhone.setEnabled(status);
        btnSendPoint.setEnabled(status);

    }
}
