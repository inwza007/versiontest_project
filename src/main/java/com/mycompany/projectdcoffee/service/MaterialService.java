/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.projectdcoffee.service;

import com.mycompany.projectdcoffee.dao.MaterialDao;
import com.mycompany.projectdcoffee.model.Material;
import com.mycompany.projectdcoffee.model.Material;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author test1
 */
public class MaterialService {

    private MaterialDao materialDao = new MaterialDao();
    public ArrayList<Material> getProductsOrderByName() {
        return (ArrayList<Material>) materialDao.getAll(" material_name ASC");
    }
    
    public Material getById(int id) {
        MaterialDao materialDao = new MaterialDao();
        return materialDao.get(id);
    }

    public List<Material> getMaterials() {
        MaterialDao materialDao = new MaterialDao();
        return materialDao.getAll(" material_id asc");
    }

    public Material addNew(Material editedMaterial) {
        MaterialDao materialDao = new MaterialDao();
//        MaterialDetailDao stockDetailDao = new MaterialDetailDao();
        Material stock = materialDao.save(editedMaterial);
//        for (MaterialDetail rd : editedMaterial.getMaterialDetails()) {
//            rd.setMaterialId(stock.getId());
//            stockDetailDao.save(rd);
//        }
        return stock;
    }

    public Material update(Material editedMaterial) {
        MaterialDao materialDao = new MaterialDao();
        return materialDao.update(editedMaterial);
    }

    public int delete(Material editedMaterial) {
        MaterialDao materialDao = new MaterialDao();
        return materialDao.delete(editedMaterial);
    }
}
