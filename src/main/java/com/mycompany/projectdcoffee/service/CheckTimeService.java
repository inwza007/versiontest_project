/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.projectdcoffee.service;
import com.mycompany.projectdcoffee.dao.CheckTimeDao;
import com.mycompany.projectdcoffee.model.CheckTime;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 *
 * @author Phattharaphon
 */
public class CheckTimeService {

    private final CheckTimeDao checkTimeDao = new CheckTimeDao();
//    public List<CheckTime> getCheckTimes() {

    public ArrayList<CheckTime> getCheckTimeOrderByName() {
        return (ArrayList<CheckTime>) checkTimeDao.getAll(" wh_id desc");
    }
    public ArrayList<CheckTime> getCheckTimeForToday() {
    String today = getCurrentDate(); // อ่านวันที่ปัจจุบัน
    return (ArrayList<CheckTime>) checkTimeDao.getAll(today," wh_id asc");
}

private String getCurrentDate() {
    SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
    return dateFormat.format(new Date());
}

        public CheckTime addNew(CheckTime editedCheckTime) {
        CheckTimeDao checkTimeDao = new CheckTimeDao();
        return checkTimeDao.save(editedCheckTime);
    }
       
    public CheckTime update(CheckTime editedCheckTime) {
        CheckTimeDao checkTimeDao = new CheckTimeDao();
        return checkTimeDao.update(editedCheckTime);
    }
}
