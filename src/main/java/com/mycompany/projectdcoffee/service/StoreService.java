/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.projectdcoffee.service;

import com.mycompany.projectdcoffee.dao.StoreDao;
import com.mycompany.projectdcoffee.model.Store;
import java.util.ArrayList;

/**
 *
 * @author Phattharaphon
 */
public class StoreService {
    public ArrayList<Store> getStores() {
        StoreDao storeDao = new StoreDao();
        return (ArrayList<Store>) storeDao.getAll(" se_id asc");
    }
    
    public Store addNew(Store editedStore) {
        StoreDao storeDao = new StoreDao();
        return storeDao.save(editedStore);
    }

    public Store update(Store editedStore) {
        StoreDao storeDao = new StoreDao();
        return storeDao.update(editedStore);
    }

    public int delete(Store editedStore) {
        StoreDao storeDao = new StoreDao();
        return storeDao.delete(editedStore);
    }
}
