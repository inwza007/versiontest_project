/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.projectdcoffee.service;

import com.mycompany.projectdcoffee.dao.CustomerDao;
import com.mycompany.projectdcoffee.dao.ProductDao;
import com.mycompany.projectdcoffee.model.Customer;
import com.mycompany.projectdcoffee.model.Product;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author zacop
 */
public class ProductService {
    private ProductDao productDao = new ProductDao();
    public ArrayList<Product> getProductsOrderByName(){
        return (ArrayList<Product>) productDao.getAll(" product_id ASC ");
    }
    
    public ArrayList<Product> getProductsOrderByDrink(){
        return (ArrayList<Product>) productDao.getDrink(" product_name ASC ","drink");
    }
    
    public ArrayList<Product> getProductsOrderByDessert(){
        return (ArrayList<Product>) productDao.getDessert(" product_name ASC ","dessert");
    }
    public List<Product> getProducts(){
        ProductDao productDao = new ProductDao();
        return productDao.getAll(" product_id asc");
    }
    public Product addNew(Product editedProduct) {
        ProductDao productDao = new ProductDao();
        return productDao.save(editedProduct);
    }

    public Product update(Product editedProduct) {
        ProductDao productDao = new ProductDao();
        return productDao.update(editedProduct);
    }

    public int delete(Product editedProduct) {
        ProductDao productDao = new ProductDao();
        return productDao.delete(editedProduct);
    }   
}
