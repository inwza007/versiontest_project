/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.projectdcoffee.service;

import com.mycompany.projectdcoffee.dao.MaterialRecieptDao;
import com.mycompany.projectdcoffee.dao.MaterialRecieptDetailDao;
import com.mycompany.projectdcoffee.model.MaterialReciept;
import com.mycompany.projectdcoffee.model.MaterialRecieptDetail;
import java.util.List;

/**
 *
 * @author test1
 */
public class MaterialRecieptService {
   public MaterialReciept getById(int id) {
        MaterialRecieptDao recieptDao = new MaterialRecieptDao();
        return recieptDao.get(id);
    }

    public List<MaterialReciept> getReciepts() {
        MaterialRecieptDao recieptDao = new MaterialRecieptDao();
        return recieptDao.getAll(" mr_id asc");
    }

    public MaterialReciept addNew(MaterialReciept editedReciept) {
        MaterialRecieptDao recieptDao = new MaterialRecieptDao();
        MaterialRecieptDetailDao recieptDetailDao = new MaterialRecieptDetailDao();
        MaterialReciept reciept = recieptDao.save(editedReciept);
        for (MaterialRecieptDetail rd : editedReciept.getMaterialRecieptDetails()) {
            rd.setMaterialRecieptId(reciept.getId());
            recieptDetailDao.save(rd);
        }
        return reciept;
    }

    public MaterialReciept update(MaterialReciept editedReciept) {
        MaterialRecieptDao recieptDao = new MaterialRecieptDao();
        return recieptDao.update(editedReciept);
    }

    public int delete(MaterialReciept editedReciept) {
        MaterialRecieptDao recieptDao = new MaterialRecieptDao();
        return recieptDao.delete(editedReciept);
    }
}
