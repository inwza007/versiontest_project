/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.projectdcoffee.service;

import com.mycompany.projectdcoffee.dao.StockDao;
import com.mycompany.projectdcoffee.dao.StockImportDao;
import com.mycompany.projectdcoffee.model.Material;
import com.mycompany.projectdcoffee.model.Stock;
import com.mycompany.projectdcoffee.model.StockImport;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author gortorr
 */
public class StockImportService {
    
    public StockImport getById(int id) {
        StockImportDao stockImportDao = new StockImportDao();
        return stockImportDao.get(id);
    }
    
    public List<StockImport> getStockImport() {
        StockImportDao stockImportDao = new StockImportDao();
        return stockImportDao.getAll(" si_id asc");
    }
    
    public StockImport addNew(StockImport editedStockImport) {
        StockImportDao stockImportDao = new StockImportDao();
        StockImport stockImport = stockImportDao.save(editedStockImport);
        return stockImport;
    }
    
    public StockImport update(StockImport editedStockImport) {
        StockImportDao stockImportDao = new StockImportDao();
        return stockImportDao.update(editedStockImport);
    }
    
    public int delete(StockImport editedStockImport) {
        StockImportDao stockImportDao = new StockImportDao();
        return stockImportDao.delete(editedStockImport);
    }
}
