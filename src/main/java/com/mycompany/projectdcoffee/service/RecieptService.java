/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.projectdcoffee.service;

import com.mycompany.projectdcoffee.dao.RecieptDao;
import com.mycompany.projectdcoffee.dao.RecieptDetailDao;
import com.mycompany.projectdcoffee.model.Reciept;
import com.mycompany.projectdcoffee.model.RecieptDetail;
import java.util.List;

/**
 *
 * @author werapan
 */
public class RecieptService {

    public Reciept getById(int id) {
        RecieptDao recieptDao = new RecieptDao();
        return recieptDao.get(id);
    }

    public List<Reciept> getReciept() {
        RecieptDao recieptDao = new RecieptDao();
        return recieptDao.getAll(" reciept_id asc");
    }

    public Reciept addNew(Reciept editedReciept,int promotion_id,String subType) {
        RecieptDao recieptDao = new RecieptDao();
        RecieptDetailDao recieptDetailDao = new RecieptDetailDao();
        Reciept reciept = recieptDao.save(editedReciept);
        for (RecieptDetail rd : editedReciept.getRecieptDetails()) {
            rd.setRecieptId(reciept.getReciept_id());
            rd.setPromotion_id(promotion_id);
            rd.setProductSubType(subType);
            recieptDetailDao.save(rd);
        }
        return reciept;
    }

    public Reciept update(Reciept editedReciept) {
        RecieptDao recieptDao = new RecieptDao();
        return recieptDao.update(editedReciept);
    }

    public int delete(Reciept editedReciept) {
        RecieptDao recieptDao = new RecieptDao();
        return recieptDao.delete(editedReciept);
    }

    public List<RecieptDetail> getProductDay() {
        RecieptDetailDao recieptDetailDao = new RecieptDetailDao();
        return recieptDetailDao.getProductDay();
    }

    public List<RecieptDetail> getProductDay(String begin, String end) {
        RecieptDetailDao recieptDetailDao = new RecieptDetailDao();
        return recieptDetailDao.getProductDay(begin, end);
    }

    public List<RecieptDetail> getProductMonth() {
        RecieptDetailDao recieptDetailDao = new RecieptDetailDao();
        return recieptDetailDao.getProductMonth();
    }

    public List<RecieptDetail> getProductMonth(String begin, String end) {
        RecieptDetailDao recieptDetailDao = new RecieptDetailDao();
        return recieptDetailDao.getProductMonth(begin, end);
    }

    public List<RecieptDetail> getProductYear() {
        RecieptDetailDao recieptDetailDao = new RecieptDetailDao();
        return recieptDetailDao.getProductYear();
    }

    public List<RecieptDetail> getProductYear(String begin, String end) {
        RecieptDetailDao recieptDetailDao = new RecieptDetailDao();
        return recieptDetailDao.getProductYear(begin, end);
    }

    public List<Reciept> getMonthSale() {
        RecieptDao recieptDao = new RecieptDao();
        return recieptDao.getMonthSale(12);
    }

    public List<Reciept> getMonthSale(String begin, String end) {
        RecieptDao recieptDao = new RecieptDao();
        return recieptDao.getMonthSale(begin, end, 12);
    }

    public List<Reciept> getYearSale() {
        RecieptDao recieptDao = new RecieptDao();
        return recieptDao.getYearSale(10);
    }

    public List<Reciept> getYearSale(String begin, String end) {
        RecieptDao recieptDao = new RecieptDao();
        return recieptDao.getYearSale(begin, end, 10);
    }

    public List<Reciept> getDailySale() {
        RecieptDao recieptDao = new RecieptDao();
        return recieptDao.getDailySale();
    }

    public List<Reciept> getDailySale(String begin, String end) {
        RecieptDao recieptDao = new RecieptDao();
        return recieptDao.getDailySale(begin, end, 10);
    }

    public List<Reciept> getTotalPrice() {
        RecieptDao recieptDao = new RecieptDao();
        return recieptDao.getTotalPricePerDay();
    }

    public List<Reciept> getTotalPrice(String begin, String end) {
        RecieptDao recieptDao = new RecieptDao();
        return recieptDao.getTotalPricePerDay(begin, end);
    }

    public List<Reciept> getTotalPricePerMonth() {
        RecieptDao recieptDao = new RecieptDao();
        return recieptDao.getTotalPricePerMonth();
    }

    public List<Reciept> getTotalPricePerMonth(String begin, String end) {
        RecieptDao recieptDao = new RecieptDao();
        return recieptDao.getTotalPricePerMonth(begin, end);
    }

    public List<Reciept> getTotalPricePerYear() {
        RecieptDao recieptDao = new RecieptDao();
        return recieptDao.getTotalPricePerYear();
    }

    public List<Reciept> getTotalPricePerYear(String begin, String end) {
        RecieptDao recieptDao = new RecieptDao();
        return recieptDao.getTotalPricePerYear(begin, end);
    }
}
