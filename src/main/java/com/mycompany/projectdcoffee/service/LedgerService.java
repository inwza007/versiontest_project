/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.projectdcoffee.service;

import com.mycompany.projectdcoffee.dao.LedgerDao;
import com.mycompany.projectdcoffee.model.Ledger;
import java.util.ArrayList;

/**
 *
 * @author Phattharaphon
 */
public class LedgerService {
    public ArrayList<Ledger> getLedgers() {
        LedgerDao ledgerDao = new LedgerDao();
        return (ArrayList<Ledger>) ledgerDao.getAll(" se_id desc");
    }
    
    public Ledger addNew(Ledger editedLedger) {
        LedgerDao ledgerDao = new LedgerDao();
        return ledgerDao.save(editedLedger);
    }

    public Ledger update(Ledger editedLedger) {
        LedgerDao ledgerDao = new LedgerDao();
        return ledgerDao.update(editedLedger);
    }

    public int delete(Ledger editedLedger) {
        LedgerDao ledgerDao = new LedgerDao();
        return ledgerDao.delete(editedLedger);
    }
}
