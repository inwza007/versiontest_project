/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.projectdcoffee.service;

import com.mycompany.projectdcoffee.dao.StockDao;
import com.mycompany.projectdcoffee.model.Material;
import com.mycompany.projectdcoffee.model.Stock;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author test1
 */
public class StockService {


//    private ArrayList<Stock> stockList;
//    private int lastId = 1;
//
//    public StockService() {
//        stockList  = new ArrayList<Stock>();
//    }
//    public Stock addStock(Stock newStock){
//        newStock.setId(lastId++);
//        stockList.add(newStock);
//        return newStock;
//    }
//    
//    public Stock getStock(int index){
//        return stockList.get(index);
//    }
//    public int getSize(){
//        return stockList.size();
//    }
    ///////////////////////////////////////////////////////////////
    public Stock getById(int id) {
        StockDao stockDao = new StockDao();
        return stockDao.get(id);
    }

    public List<Stock> getStocks() {
        StockDao stockDao = new StockDao();
        return stockDao.getAll(" material_id asc");
    }

    public Stock addNew(Stock editedStock) {
        StockDao stockDao = new StockDao();
//        StockDetailDao stockDetailDao = new StockDetailDao();
        Stock stock = stockDao.save(editedStock);
//        for (StockDetail rd : editedStock.getStockDetails()) {
//            rd.setStockId(stock.getId());
//            stockDetailDao.save(rd);
//        }
        return stock;
    }

    public Stock update(Stock editedStock) {
        StockDao stockDao = new StockDao();
        return stockDao.update(editedStock);
    }

    public int delete(Stock editedStock) {
        StockDao stockDao = new StockDao();
        return stockDao.delete(editedStock);
    }
}
