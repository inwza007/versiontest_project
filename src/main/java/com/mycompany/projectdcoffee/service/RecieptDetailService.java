/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.projectdcoffee.service;

import com.mycompany.projectdcoffee.dao.RecieptDao;
import com.mycompany.projectdcoffee.dao.RecieptDetailDao;
import com.mycompany.projectdcoffee.model.Reciept;
import com.mycompany.projectdcoffee.model.RecieptDetail;
import java.util.List;

/**
 *
 * @author werapan
 */
public class RecieptDetailService {
    public RecieptDetail getId(int id) {
        RecieptDetailDao recieptDetailDao = new RecieptDetailDao();
        return recieptDetailDao.get(id);
    }
    
    public List<RecieptDetail> getRecieptDetail(){
        RecieptDetailDao recieptDetailDao = new RecieptDetailDao();
        return recieptDetailDao.getAll(" reciept_detail_id asc");
    }

    public int delete(RecieptDetail editedrecieptDetail) {
        RecieptDetailDao recieptDetailDao = new RecieptDetailDao();
        return recieptDetailDao.delete(editedrecieptDetail);
    }
}

