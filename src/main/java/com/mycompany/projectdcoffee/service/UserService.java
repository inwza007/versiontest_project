/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.projectdcoffee.service;

import com.mycompany.projectdcoffee.dao.UserDao;
import com.mycompany.projectdcoffee.model.User;
import java.util.List;

/**
 *
 * @author werapan
 */
public class UserService {
    
   public User getUserByLoginAndPassword(String login, String password) {
    UserDao userDao = new UserDao();
    return userDao.get(login, password);
}

    public List<User> getUsers() {
        UserDao userDao = new UserDao();
        return userDao.getAll(" user_login asc");
    }
    public List<User> getUserId() {
        UserDao userDao = new UserDao();
        return userDao.getTable(" user_id asc");
   }
    
    public User addNew(User editedUser) {
        UserDao userDao = new UserDao();
        return userDao.save(editedUser);
    }

    public User update(User editedUser) {
        UserDao userDao = new UserDao();
        return userDao.update(editedUser);
    }

    public int delete(User editedUser) {
        UserDao userDao = new UserDao();
        return userDao.delete(editedUser);
    }
    
        public static User getCurrentUser() {
        return new User();
    }
}
