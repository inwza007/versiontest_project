/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.projectdcoffee.dao;

import com.mycompany.databaseproject.helper.DatabaseHelper;
import com.mycompany.projectdcoffee.model.Promotion;
import com.mycompany.projectdcoffee.model.User;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author zacop
 */
public class UserDao implements Dao<User> {

    @Override
    public User get(int id) {
        User user = null;
        String sql = "SELECT * FROM user WHERE user_id=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                user = User.fromRS(rs);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return user;
    }

    public User get(String login, String password) {
        User user = null;
        String sql = "SELECT * FROM user WHERE user_login = ? AND user_password = ?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, login);
            stmt.setString(2, password);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                user = User.fromRS(rs);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return user;
    }

    public static boolean getByLogin(String login, String password, String role) {
        User user = null;
        String sql = "SELECT * FROM user WHERE user_login=? AND user_password = ? AND user_role = ? ";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, login);
            stmt.setString(2, password);
            stmt.setString(3, role);
            ResultSet rs = stmt.executeQuery();

            return rs.next();  // If a result is found, authentication successful

        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

//    public static boolean getByLogin(String login, String password) {
//        User user = null;
//        String sql = "SELECT * FROM user WHERE user_login=? AND user_password = ?";
//        Connection conn = DatabaseHelper.getConnect();
//        try {
//            PreparedStatement stmt = conn.prepareStatement(sql);
//            stmt.setString(1, login);
//            stmt.setString(2, password);
//            ResultSet rs = stmt.executeQuery();
//
//            return rs.next();  // If a result is found, authentication successful
//
//        } catch (Exception e) {
//            e.printStackTrace();
//            return false;
//        }
//    }
    public List<User> getAll() {
        ArrayList<User> list = new ArrayList();
        String sql = "SELECT * FROM user";
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                User user = User.fromRS(rs);
                list.add(user);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    @Override
    public List<User> getAll(String where, String order) {
        ArrayList<User> list = new ArrayList();
        String sql = "SELECT * FROM user where " + where + " ORDER BY" + order;
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                User user = User.fromRS(rs);
                list.add(user);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }
//    public List<User> getAll(String order) {
//        ArrayList<User> list = new ArrayList();
//        String sql = "SELECT customer_firstname,customer_lastname,customer_tel,customer_birthdate FROM user  ORDER BY" + order;
//        Connection conn = DatabaseHelper.getConnect();
//        try {
//            Statement stmt = conn.createStatement();
//            ResultSet rs = stmt.executeQuery(sql);
//
//            while (rs.next()) {
//                User user = User.fromRS(rs);
//                list.add(user);
//
//            }
//
//        } catch (SQLException ex) {
//            System.out.println(ex.getMessage());
//        }
//        return list;
//    }
//, user_status, total_salary

    public List<User> getTable(String order) {
        ArrayList<User> list = new ArrayList();
        String sql = "SELECT * FROM user  ORDER BY" + order;
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                User user = User.fromRS(rs);
                list.add(user);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    @Override
    public User save(User obj) {
        String sql = "INSERT INTO user (user_first_name, user_last_name, user_gender, user_role, user_email, user_addres, user_login, user_password)"
                + "VALUES(?, ?, ?, ?, ?, ?, ?, ?)";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, obj.getFname()); // ใช้ getFname() แทน getLogin()
            stmt.setString(2, obj.getLname()); // ใช้ getLname() แทน getFullName()
            stmt.setString(3, obj.getGender());
            stmt.setString(4, obj.getRole());
            stmt.setString(5, obj.getEmail()); // เพิ่มข้อมูลอีเมล
            stmt.setString(6, obj.getAddres()); // เพิ่มข้อมูลที่อยู่
            stmt.setString(7, obj.getLogin());
            stmt.setString(8, obj.getPassword());
            stmt.executeUpdate();
            int id = DatabaseHelper.getInsertedId(stmt);
            obj.setId(id);
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
        return obj;
    }
//user_status = ?, total_salary = ?,

    @Override
    public User update(User obj) {
        String sql = "UPDATE user"
                + " SET user_first_name = ?, user_last_name = ?, user_gender = ?, user_role = ?, user_email = ?, user_addres = ?, user_login = ?, user_password = ?"
                + " WHERE user_id = ?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, obj.getFname());
            stmt.setString(2, obj.getLname());
            stmt.setString(3, obj.getGender());
            stmt.setString(4, obj.getRole());
            stmt.setString(5, obj.getEmail());
            stmt.setString(6, obj.getAddres());
            stmt.setString(7, obj.getLogin());
            stmt.setString(8, obj.getPassword());
            stmt.setInt(9, obj.getId()); // แก้ไขเพิ่มการระบุ user_id ใน WHERE clause

            int ret = stmt.executeUpdate();
            System.out.println(ret);
            if (ret > 0) {
                return obj;
            } else {
                return null; // คืนค่า null หากไม่มีการอัปเดตเกิดขึ้น
            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
    }

    @Override
    public int delete(User obj) {
        String sql = "DELETE FROM user WHERE user_id=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getId());
            int ret = stmt.executeUpdate();
            return ret;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return -1;
    }

    @Override
    public List<User> getAll(String id) {
        ArrayList<User> list = new ArrayList();
        String sql = "SELECT * FROM user";
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                User user = User.fromRS(rs);
                list.add(user);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());

        }
        return list;

    }

}
