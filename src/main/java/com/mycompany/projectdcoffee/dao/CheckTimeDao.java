/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.projectdcoffee.dao;

import com.mycompany.databaseproject.helper.DatabaseHelper;
import com.mycompany.projectdcoffee.model.CheckTime;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Time;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Phattharaphon
 */
public class CheckTimeDao implements Dao<CheckTime> {

    @Override
    public CheckTime get(int id) {
        CheckTime checkTime = null;
        String sql = "SELECT * FROM working_hours WHERE wh_id=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                checkTime = CheckTime.fromRS(rs);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return checkTime;
    }

    public List<CheckTime> getAll() {
        ArrayList<CheckTime> list = new ArrayList();
        String sql = "SELECT * FROM working_hours";
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                CheckTime checkTime = CheckTime.fromRS(rs);
                list.add(checkTime);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    @Override
    public List<CheckTime> getAll(String where, String order) {
       ArrayList<CheckTime> list = new ArrayList();
    String sql = "SELECT * FROM working_hours WHERE wh_login_Date = ? ORDER BY " + order;
    Connection conn = DatabaseHelper.getConnect();
    try {
        PreparedStatement pstmt = conn.prepareStatement(sql);
        pstmt.setString(1, where); 
        ResultSet rs = pstmt.executeQuery();

        while (rs.next()) {
            CheckTime checkTime = CheckTime.fromRS(rs);
            list.add(checkTime);
        }

    } catch (SQLException ex) {
        System.out.println(ex.getMessage());
    }
    return list;
}

    public List<CheckTime> getAll(String order) {
        ArrayList<CheckTime> list = new ArrayList();
        String sql = "SELECT * FROM working_hours ORDER BY " + order;
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                CheckTime checkTime = CheckTime.fromRS(rs);
                list.add(checkTime);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    @Override
public CheckTime save(CheckTime obj) {
    String sql = "INSERT INTO working_hours (user_id, wh_login_date, wh_login_time, wh_logout_time, wh_total_time, wh_status) "
            + "VALUES (?, ?, ?, ?, ?, ?)";
    Connection conn = DatabaseHelper.getConnect();
    try {
        PreparedStatement stmt = conn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
        stmt.setInt(1, obj.getUserId());
        stmt.setString(2, obj.getWhLoginDate());
        stmt.setString(3, obj.getWhLoginTime());
        stmt.setString(4, obj.getWhLogoutTime());
        stmt.setInt(5, obj.getWhTotalTime());
        stmt.setString(6, "unprocess");

        int rowsAffected = stmt.executeUpdate();
        if (rowsAffected == 1) {
            ResultSet generatedKeys = stmt.getGeneratedKeys();
            if (generatedKeys.next()) {
                obj.setWhId(generatedKeys.getInt(1));
            }
        } else {
            return null;
        }
    } catch (SQLException ex) {
        System.out.println(ex.getMessage());
        return null;
    }
    return obj;
}

    @Override
    public CheckTime update(CheckTime obj) {
        String sql = "UPDATE working_hours SET wh_logout_time = ?, wh_total_time = ? WHERE wh_id = ?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, obj.getWhLogoutTime());
            stmt.setInt(2, obj.getWhTotalTime());
            stmt.setInt(4, obj.getWhId());

            int rowsAffected = stmt.executeUpdate();
            if (rowsAffected == 1) {
                return obj;
            } else {
                return null;
            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
    }

    @Override
    public int delete(CheckTime obj) {
        String sql = "DELETE FROM working_hours WHERE wh_id = ?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getWhId());
            int rowsAffected = stmt.executeUpdate();
            return rowsAffected;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return -1;
    }
}
