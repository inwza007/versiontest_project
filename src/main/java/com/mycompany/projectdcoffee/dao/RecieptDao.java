/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.projectdcoffee.dao;

import com.mycompany.databaseproject.helper.DatabaseHelper;
import com.mycompany.projectdcoffee.model.Reciept;
import com.mycompany.projectdcoffee.model.RecieptDetail;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author werapan
 */
public class RecieptDao implements Dao<Reciept> {

    @Override
    public Reciept get(int id) {
        Reciept reciept = null;
        String sql = "SELECT * FROM reciept WHERE reciept_id=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                reciept = Reciept.fromRS(rs);
                RecieptDetailDao rdd = new RecieptDetailDao();
                ArrayList<RecieptDetail> recieptDetails = (ArrayList<RecieptDetail>) rdd.getAll("reciept_id=" + reciept.getReciept_id(), " reciept_detail_id ");
                reciept.setRecieptDetails(recieptDetails);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return reciept;
    }

    public List<Reciept> getAll() {
        ArrayList<Reciept> list = new ArrayList();
        String sql = "SELECT * FROM reciept";
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                Reciept reciept = Reciept.fromRS(rs);
                list.add(reciept);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    @Override
    public List<Reciept> getAll(String where, String order) {
        ArrayList<Reciept> list = new ArrayList();
        String sql = "SELECT * FROM reciept WHERE " + where + " ORDER BY " + order;
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                Reciept reciept = Reciept.fromRS(rs);
                list.add(reciept);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    public List<Reciept> getAll(String order) {
        ArrayList<Reciept> list = new ArrayList();
        String sql = "SELECT * FROM reciept ORDER BY " + order;
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                Reciept reciept = Reciept.fromRS(rs);
                list.add(reciept);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    @Override
    public Reciept save(Reciept obj) {
        String sql = "INSERT INTO reciept (re_total, re_discount, re_net, re_pay, re_pay_type, re_change, re_total_qty, User_id, customer_id)"
                + "VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?)";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setFloat(1, obj.getRe_total());
            stmt.setFloat(2, obj.getRe_discount());
            stmt.setFloat(3, obj.getRe_net());
            stmt.setFloat(4, obj.getRe_pay());
            stmt.setString(5, obj.getRe_pay_type());
            stmt.setFloat(6, obj.getRe_change());
            stmt.setInt(7, obj.getRe_total_qty());
            stmt.setInt(8, obj.getUser_id());
            stmt.setInt(9, obj.getCustomer_id());
//            System.out.println(stmt);
            stmt.executeUpdate();
            int id = DatabaseHelper.getInsertedId(stmt);
            obj.setReciept_id(id);
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
        return obj;
    }

    @Override
    public Reciept update(Reciept obj) {
        String sql = "UPDATE reciept" + " SET re_total = ?, re_discount = ?, re_net = ?, re_pay = ?, re_pay_type = ?, re_change = ?, re_total_qty = ?, user_id = ?, customer_id = ?"
                + "WHERE reciept_id = ?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setFloat(1, obj.getRe_total());
            stmt.setFloat(2, obj.getRe_discount());
            stmt.setFloat(3, obj.getRe_net());
            stmt.setFloat(4, obj.getRe_pay());
            stmt.setString(5, obj.getRe_pay_type());
            stmt.setFloat(6, obj.getRe_change());
            stmt.setInt(7, obj.getRe_total_qty());
            stmt.setInt(8, obj.getUser_id());
            stmt.setInt(9, obj.getCustomer_id());
//            System.out.println(stmt);
            int ret = stmt.executeUpdate();
            System.out.println(ret);
            return obj;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
    }

    @Override
    public int delete(Reciept obj) {
        String sql = "DELETE FROM reciept WHERE reciept_id=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getReciept_id());
            int ret = stmt.executeUpdate();
            return ret;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return -1;
    }

    public List<Reciept> getDailySale() {
        ArrayList<Reciept> list = new ArrayList();
        String sql = """
                        SELECT *, COUNT(reciept_id) as count_reciept,SUM(re_total) as total_price_day
                        FROM reciept
                        GROUP BY DATE(re_date)
                     """;
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                Reciept obj = Reciept.fromRS(rs);
                list.add(obj);
            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    public List<Reciept> getDailySale(String begin, String end, int limit) {
        ArrayList<Reciept> list = new ArrayList();
        String sql = """
                        SELECT *, COUNT(reciept_id) as count_reciept,SUM(re_total) as total_price_day
                        FROM reciept
                        WHERE re_date BETWEEN ? AND ?
                        GROUP BY DATE(re_date)
                        LIMIT ?
                     """;
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, begin);
            stmt.setString(2, end);
            stmt.setInt(3, limit);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                Reciept obj = Reciept.fromRS(rs);
                list.add(obj);
            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    public List<Reciept> getMonthSale(int limit) {
        ArrayList<Reciept> list = new ArrayList();
        String sql = """
                        SELECT *, COUNT(reciept_id) as count_reciept,SUM(re_total) as total_price_day
                        FROM reciept
                        GROUP BY strftime('%Y-%m',re_date)
                        LIMIT ?
                     """;
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, limit);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                Reciept obj = Reciept.fromRS(rs);
                list.add(obj);
            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    public List<Reciept> getMonthSale(String begin, String end, int limit) {
        ArrayList<Reciept> list = new ArrayList();
        String sql = """
                        SELECT *, COUNT(reciept_id) as count_reciept,SUM(re_total) as total_price_day
                        FROM reciept
                        WHERE re_date BETWEEN ? AND ?
                        GROUP BY strftime('%Y-%m',re_date)
                        LIMIT ?
                     """;
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
           stmt.setString(1, begin);
            stmt.setString(2, end);
            stmt.setInt(3, limit);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                Reciept obj = Reciept.fromRS(rs);
                list.add(obj);
            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }
    
        public List<Reciept> getYearSale(int limit) {
        ArrayList<Reciept> list = new ArrayList();
        String sql = """
                        SELECT *, COUNT(reciept_id) as count_reciept,SUM(re_total) as total_price_day
                        FROM reciept
                        GROUP BY strftime('%Y',re_date)
                        LIMIT ?
                     """;
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, limit);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                Reciept obj = Reciept.fromRS(rs);
                list.add(obj);
            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    public List<Reciept> getYearSale(String begin, String end, int limit) {
        ArrayList<Reciept> list = new ArrayList();
        String sql = """
                        SELECT *, COUNT(reciept_id) as count_reciept,SUM(re_total) as total_price_day
                        FROM reciept
                        WHERE re_date BETWEEN ? AND ?
                        GROUP BY strftime('%Y',re_date)
                        LIMIT ?
                     """;
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
           stmt.setString(1, begin);
            stmt.setString(2, end);
            stmt.setInt(3, limit);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                Reciept obj = Reciept.fromRS(rs);
                list.add(obj);
            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }
    
    public List<Reciept> getTotalPricePerDay(String begin, String end) {
        ArrayList<Reciept> list = new ArrayList();
        String sql = """
                        SELECT *, COUNT(reciept_id) as count_reciept, SUM(re_total) as total_price_day
                        FROM reciept
                        WHERE re_date BETWEEN ? AND ?
                        GROUP BY DATE(re_date)
                     """;
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, begin);
            stmt.setString(2, end);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                Reciept obj = Reciept.fromRS(rs);
                list.add(obj);
            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    public List<Reciept> getTotalPricePerDay() {
        ArrayList<Reciept> list = new ArrayList();
        String sql = """
                        SELECT *, COUNT(reciept_id) as count_reciept, SUM(re_total) as total_price_day          
                        FROM reciept
                        GROUP BY DATE(re_date)
                     """;
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                Reciept obj = Reciept.fromRS(rs);
                list.add(obj);
            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }
    
    public List<Reciept> getTotalPricePerMonth(String begin, String end) {
        ArrayList<Reciept> list = new ArrayList();
        String sql = """
                        SELECT *, COUNT(reciept_id) as count_reciept, SUM(re_total) as total_price_day
                        FROM reciept
                        WHERE re_date BETWEEN ? AND ?
                        GROUP BY strftime('%Y-%m',re_date)
                     """;
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, begin);
            stmt.setString(2, end);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                Reciept obj = Reciept.fromRS(rs);
                list.add(obj);
            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    public List<Reciept> getTotalPricePerMonth() {
        ArrayList<Reciept> list = new ArrayList();
        String sql = """
                        SELECT *, COUNT(reciept_id) as count_reciept, SUM(re_total) as total_price_day
                        FROM reciept
                        GROUP BY strftime('%Y-%m',re_date)
                     """;
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                Reciept obj = Reciept.fromRS(rs);
                list.add(obj);
            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }
    
        
    public List<Reciept> getTotalPricePerYear(String begin, String end) {
        ArrayList<Reciept> list = new ArrayList();
        String sql = """
                        SELECT *, COUNT(reciept_id) as count_reciept, SUM(re_total) as total_price_day
                        FROM reciept
                        WHERE re_date BETWEEN ? AND ?
                        GROUP BY strftime('%Y',re_date)
                     """;
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, begin);
            stmt.setString(2, end);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                Reciept obj = Reciept.fromRS(rs);
                list.add(obj);
            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    public List<Reciept> getTotalPricePerYear() {
        ArrayList<Reciept> list = new ArrayList();
        String sql = """
                        SELECT *, COUNT(reciept_id) as count_reciept, SUM(re_total) as total_price_day
                        FROM reciept
                        GROUP BY strftime('%Y',re_date)
                     """;
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                Reciept obj = Reciept.fromRS(rs);
                list.add(obj);
            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }
}
