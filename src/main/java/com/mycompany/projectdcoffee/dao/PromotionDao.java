/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.projectdcoffee.dao;

import com.mycompany.databaseproject.helper.DatabaseHelper;
import com.mycompany.projectdcoffee.model.Customer;
import com.mycompany.projectdcoffee.model.Promotion;
import com.mycompany.projectdcoffee.ui.POS.POSPanel;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.swing.JComboBox;

/**
 *
 * @author GAMING PC
 */
public class PromotionDao implements Dao<Promotion> {

    @Override
    public Promotion get(int Pid) {
        Promotion promotion = null;
        String sql = "SELECT * FROM promotion WHERE pr_id=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, Pid);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                promotion = Promotion.fromRS(rs);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());

        }
        return promotion;

    }

    @Override
    public List<Promotion> getAll(String pr_id) {
        ArrayList<Promotion> list = new ArrayList();
        String sql = "SELECT * FROM promotion";
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                Promotion promotion = Promotion.fromRS(rs);
                list.add(promotion);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());

        }
        return list;

    }

    @Override
    public Promotion save(Promotion obj) {
        String sql = "INSERT INTO promotion ( pr_name,pr_discount, pr_start_date, pr_end_date, pr_status)"
                + "VALUES(?,?,?,?,?)";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, obj.getPname());
            stmt.setInt(2, obj.getPdiscount());
            stmt.setString(3, obj.getPstart());
            stmt.setString(4, obj.getPend());
            stmt.setString(5, obj.getPstatus());

//            System.out.println(stmt);
            stmt.executeUpdate();
            int id = DatabaseHelper.getInsertedId(stmt);
            obj.setPid(id);
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
        return obj;

    }

    @Override
    public Promotion update(Promotion obj) {
        String sql = "UPDATE promotion"
                + " SET pr_name = ?, pr_discount = ?,pr_start_date = ?, pr_end_date = ?, pr_status = ?"
                + " WHERE pr_id = ?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, obj.getPname());
            stmt.setInt(2, obj.getPdiscount());
            stmt.setString(3, obj.getPstart());
            stmt.setString(4, obj.getPend());
            stmt.setString(5, obj.getPstatus());
            stmt.setInt(6, obj.getPid());

            System.out.println(stmt);
            int ret = stmt.executeUpdate();
            System.out.println(ret);
            return obj;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }

    }

    @Override
    public int delete(Promotion obj) {
        String sql = "DELETE FROM promotion WHERE pr_id=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getPid());
            int ret = stmt.executeUpdate();
            return ret;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());

        }
        return -1;

    }

    @Override
    public List<Promotion> getAll(String where, String order) {
        ArrayList<Promotion> list = new ArrayList();
        String sql = "SELECT * FROM promotion where " + where + " ORDER BY" + order;
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                Promotion promotion = Promotion.fromRS(rs);
                list.add(promotion);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;

    }

    public static void populateComboBox(JComboBox<String> comboBox) {
        Connection connection = null;
        try {
            connection = DriverManager.getConnection("jdbc:sqlite:DBCafe.db");

            Statement statement = connection.createStatement();
            String query = "SELECT * FROM promotion";
            ResultSet resultSet = statement.executeQuery(query);

            while (resultSet.next()) {
                comboBox.addItem(resultSet.getString("pr_name"));
                String pr_start = resultSet.getString("pr_start_date");
                String pr_end = resultSet.getString("pr_start_date");

            }
            
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
        public static  Promotion fetchOtherData(String selectedName) {
            Promotion promotion = null;
            String sql = "SELECT *  FROM promotion WHERE pr_name = '" + selectedName + "'";
            Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                promotion = Promotion.fromRS(rs);
            }
            
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return promotion;
    }

}
