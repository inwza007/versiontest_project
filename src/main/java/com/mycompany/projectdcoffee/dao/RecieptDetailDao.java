/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.projectdcoffee.dao;

import com.mycompany.databaseproject.helper.DatabaseHelper;
import com.mycompany.projectdcoffee.model.Reciept;
import com.mycompany.projectdcoffee.model.RecieptDetail;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author werapan
 */
public class RecieptDetailDao implements Dao<RecieptDetail> {

    @Override
    public RecieptDetail get(int id) {
        RecieptDetail recieptDetail = null;
        String sql = "SELECT * FROM reciept_detail WHERE reciept_detail_id=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                recieptDetail = RecieptDetail.fromRS(rs);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return recieptDetail;
    }

    public List<RecieptDetail> getAll() {
        ArrayList<RecieptDetail> list = new ArrayList();
        String sql = "SELECT * FROM reciept_detail";
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                RecieptDetail recieptDetail = RecieptDetail.fromRS(rs);
                list.add(recieptDetail);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    @Override
    public List<RecieptDetail> getAll(String where, String order) {
        ArrayList<RecieptDetail> list = new ArrayList();
        String sql = "SELECT * FROM reciept_detail WHERE " + where + " ORDER BY " + order;
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                RecieptDetail recieptDetail = RecieptDetail.fromRS(rs);
                list.add(recieptDetail);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    public List<RecieptDetail> getAll(String order) {
        ArrayList<RecieptDetail> list = new ArrayList();
        String sql = "SELECT * FROM reciept_detail ORDER BY " + order;
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                RecieptDetail recieptDetail = RecieptDetail.fromRS(rs);
                list.add(recieptDetail);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    @Override
    public RecieptDetail save(RecieptDetail obj) {
        String sql = "INSERT INTO reciept_detail (product_id, product_name,product_size, product_price, qty, total_price, reciept_id,pd_id,product_Sub_Type)"
                + "VALUES(?, ?, ?, ?, ?, ?,?,?,?)";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getProductId());
            stmt.setString(2, obj.getProductName());
            stmt.setString(3, obj.getProductSize());
            stmt.setFloat(4, obj.getProductPrice());
            stmt.setInt(5, obj.getQty());
            stmt.setFloat(6, obj.getTotalPrice());
            stmt.setInt(7, obj.getRecieptId());
            stmt.setInt(8,obj.getPromotion_id());
            stmt.setString(9, obj.getProductSubType());
            stmt.executeUpdate();
            int id = DatabaseHelper.getInsertedId(stmt);
            obj.setId(id);
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
        return obj;
    }

    @Override
    public RecieptDetail update(RecieptDetail obj) {
        String sql = "UPDATE recieptDetail" + " SET product_id = ?, product_name = ?, product_price = ?, qty = ?, total_price = ?, reciept_id = ?"
                + "WHERE reciept_detail_id = ?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getProductId());
            stmt.setString(2, obj.getProductName());
            stmt.setString(3, obj.getProductSize());
            stmt.setFloat(5, obj.getProductPrice());
            stmt.setInt(4, obj.getQty());
            stmt.setFloat(6, obj.getTotalPrice());
            stmt.setInt(7, obj.getRecieptId());
            stmt.setInt(8, obj.getId());
//            System.out.println(stmt);
            int ret = stmt.executeUpdate();
            System.out.println(ret);
            return obj;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
    }

    public RecieptDetail updateQty(RecieptDetail obj) {
        String sql = "UPDATE recieptDetail SET qty = ? WHERE reciept_detail_id = ?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getQty());
            stmt.setInt(2, obj.getId());
            int ret = stmt.executeUpdate();
            System.out.println(ret);
            return obj;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
    }

    public RecieptDetail checkAndUpdateQty(RecieptDetail obj) {
        RecieptDetail existingItem = getRecieptDetailByConditions(obj);

        if (existingItem != null) {
            existingItem.setQty(existingItem.getQty() + obj.getQty()); 
            updateQty(existingItem);
            return existingItem;
        } else {
            return save(obj);
        }
    }

    public RecieptDetail getRecieptDetailByConditions(RecieptDetail obj) {
        String sql = "SELECT * FROM reciept_detail WHERE product_name = ? AND product_size = ? AND productSubType = ?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, obj.getProductName());
            stmt.setString(2, obj.getProductSize());
            stmt.setString(3, obj.getProductSubType());
            ResultSet rs = stmt.executeQuery();

            if (rs.next()) {
                RecieptDetail existingItem = new RecieptDetail();
                existingItem.setId(rs.getInt("reciept_detail_id"));
                existingItem.setQty(rs.getInt("qty"));
                return existingItem;
            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return null; 
    }

    @Override
    public int delete(RecieptDetail obj) {
        String sql = "DELETE FROM reciept_detail WHERE reciept_detail_id=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getId());
            int ret = stmt.executeUpdate();
            return ret;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return -1;
    }

    public List<RecieptDetail> getProductDay() {
        ArrayList<RecieptDetail> list = new ArrayList();
        String sql = """
                     SELECT *, SUM(qty) as product_total_qty, reciept.re_date 
                     FROM reciept_detail 
                     INNER JOIN reciept ON reciept.reciept_id = reciept_detail.reciept_id
                     GROUP BY DATE (reciept.re_date), reciept_detail.product_name
                     ORDER BY product_total_qty DESC;
                     """;
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                RecieptDetail obj = RecieptDetail.fromRS(rs);
                list.add(obj);
            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    public List<RecieptDetail> getProductDay(String begin, String end) {
        ArrayList<RecieptDetail> list = new ArrayList();
        String sql = """
                     SELECT *, SUM(qty) as product_total_qty, reciept.re_date 
                     FROM reciept_detail 
                     INNER JOIN reciept ON reciept.reciept_id = reciept_detail.reciept_id
                     WHERE reciept.re_date BETWEEN ? AND ?
                     GROUP BY DATE (reciept.re_date), reciept_detai.product_name
                     ORDER BY product_total_qty DESC;
                     """;
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, begin);
            stmt.setString(2, end);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                RecieptDetail obj = RecieptDetail.fromRS(rs);
                list.add(obj);
            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    public List<RecieptDetail> getProductMonth() {
        ArrayList<RecieptDetail> list = new ArrayList();
        String sql = """
                     SELECT *, SUM(qty) as product_total_qty, reciept.re_date 
                     FROM reciept_detail 
                     INNER JOIN reciept ON reciept.reciept_id = reciept_detail.reciept_id
                     GROUP BY strftime('%Y-%m',re_date), reciept_detail.product_name
                     ORDER BY product_total_qty DESC;
                     """;
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                RecieptDetail obj = RecieptDetail.fromRS(rs);
                list.add(obj);
            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }
    
        public List<RecieptDetail> getProductMonth(String begin, String end) {
        ArrayList<RecieptDetail> list = new ArrayList();
        String sql = """
                     SELECT *, SUM(qty) as product_total_qty, reciept.re_date 
                     FROM reciept_detail 
                     INNER JOIN reciept ON reciept.reciept_id = reciept_detail.reciept_id
                     WHERE reciept.re_date BETWEEN ? AND ?
                     GROUP BY strftime('%Y-%m',re_date), reciept_detail.product_name
                     ORDER BY product_total_qty DESC;
                     """;
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, begin);
            stmt.setString(2, end);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                RecieptDetail obj = RecieptDetail.fromRS(rs);
                list.add(obj);
            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }
    
    public List<RecieptDetail> getProductYear() {
        ArrayList<RecieptDetail> list = new ArrayList();
        String sql = """
                     SELECT *, SUM(qty) as product_total_qty, reciept.re_date 
                     FROM reciept_detail 
                     INNER JOIN reciept ON reciept.reciept_id = reciept_detail.reciept_id
                     GROUP BY strftime('%Y',re_date), reciept_detail.product_name
                     ORDER BY product_total_qty DESC;
                     """;
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                RecieptDetail obj = RecieptDetail.fromRS(rs);
                list.add(obj);
            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }
    
            public List<RecieptDetail> getProductYear(String begin, String end) {
        ArrayList<RecieptDetail> list = new ArrayList();
        String sql = """
                     SELECT *, SUM(qty) as product_total_qty, reciept.re_date 
                     FROM reciept_detail 
                     INNER JOIN reciept ON reciept.reciept_id = reciept_detail.reciept_id
                     WHERE reciept.re_date BETWEEN ? AND ?
                     GROUP BY strftime('%Y',re_date), reciept_detail.product_name
                     ORDER BY product_total_qty DESC;
                     """;
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, begin);
            stmt.setString(2, end);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                RecieptDetail obj = RecieptDetail.fromRS(rs);
                list.add(obj);
            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }
}
