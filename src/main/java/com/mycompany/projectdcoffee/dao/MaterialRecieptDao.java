/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.projectdcoffee.dao;

import com.mycompany.databaseproject.helper.DatabaseHelper;
import com.mycompany.projectdcoffee.model.MaterialReciept;
import com.mycompany.projectdcoffee.model.MaterialRecieptDetail;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author test1
 */
public class MaterialRecieptDao implements Dao<MaterialReciept> {

    @Override
    public MaterialReciept get(int id) {
        MaterialReciept materialReciept = null;
        String sql = "SELECT * FROM material_reciept WHERE mr_id=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                materialReciept = MaterialReciept.fromRS(rs);
                MaterialRecieptDetailDao rdd = new MaterialRecieptDetailDao();
                ArrayList<MaterialRecieptDetail> materialRecieptDetails = (ArrayList<MaterialRecieptDetail>) rdd.getAll("mr_id=" + materialReciept.getId(), " mrd_id ");
                materialReciept.setMaterialRecieptDetails(materialRecieptDetails);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return materialReciept;
    }

    public List<MaterialReciept> getAll() {
        ArrayList<MaterialReciept> list = new ArrayList();
        String sql = "SELECT * FROM material_reciept";
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                MaterialReciept materialReciept = MaterialReciept.fromRS(rs);
                list.add(materialReciept);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    @Override
    public List<MaterialReciept> getAll(String where, String order) {
        ArrayList<MaterialReciept> list = new ArrayList();
        String sql = "SELECT * FROM material_receipt where " + where + " ORDER BY" + order;
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                MaterialReciept materialReciept = MaterialReciept.fromRS(rs);
                list.add(materialReciept);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    public List<MaterialReciept> getAll(String order) {
        ArrayList<MaterialReciept> list = new ArrayList();
        String sql = "SELECT * FROM material_reciept  ORDER BY" + order;
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                MaterialReciept materialReciept = MaterialReciept.fromRS(rs);
                list.add(materialReciept);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    @Override
    public MaterialReciept save(MaterialReciept obj) {

        String sql = "INSERT INTO material_reciept (suppler_id ,mr_total_qty, mr_total, user_id, store_id)"
                + "VALUES(?, ?, ? ,?)";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
//            stmt.setInt(1, obj.getId());
            stmt.setInt(1, obj.getSupplierId());
            stmt.setInt(2, obj.getTotalQty());
            stmt.setFloat(3, obj.getTotal());
            stmt.setInt(4, obj.getUserId());
            stmt.setFloat(5, obj.getStoreId());
//            System.out.println(stmt);
            stmt.executeUpdate();
            int id = DatabaseHelper.getInsertedId(stmt);
            obj.setId(id);
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
        return obj;
    }

    @Override
    public MaterialReciept update(MaterialReciept obj) {
        String sql = "UPDATE material_reciept"
                + " SET   supplier_id = ?, mr_total_qty = ?, mr_total = ?, user_id = ?, store_id  = ?"
                + " WHERE mr_id = ?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getSupplierId());
            stmt.setInt(2, obj.getTotalQty());
            stmt.setFloat(3, obj.getTotal());
            stmt.setInt(4, obj.getUserId());
            stmt.setInt(5, obj.getStoreId());
            stmt.setInt(6, obj.getId());
//            System.out.println(stmt);
            int ret = stmt.executeUpdate();
            System.out.println(ret);
            return obj;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
    }

    @Override
    public int delete(MaterialReciept obj) {
        String sql = "DELETE FROM material_reciept WHERE mr_id=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getId());
            int ret = stmt.executeUpdate();
            return ret;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return -1;
    }
}
