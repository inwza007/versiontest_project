/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.projectdcoffee.dao;

import com.mycompany.databaseproject.helper.DatabaseHelper;
import com.mycompany.projectdcoffee.model.Ledger;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Phattharaphon
 */
public class LedgerDao implements Dao<Ledger> {

    @Override
    public Ledger get(int id) {
        Ledger ledger = null;
        String sql = "SELECT * FROM store_expan WHERE se_id=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                ledger = Ledger.fromRS(rs);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return ledger;
    }

    public List<Ledger> getAll() {
        ArrayList<Ledger> list = new ArrayList();
        String sql = "SELECT * FROM store_expan";
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                Ledger ledger = Ledger.fromRS(rs);
                list.add(ledger);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    @Override
    public List<Ledger> getAll(String where, String order) {
        ArrayList<Ledger> list = new ArrayList();
        String sql = "SELECT * FROM store_expan WHERE " + where + " ORDER BY " + order;
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                Ledger ledger = Ledger.fromRS(rs);
                list.add(ledger);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    public List<Ledger> getAll(String order) {
        ArrayList<Ledger> list = new ArrayList();
        String sql = "SELECT * FROM store_expan ORDER BY " + order;
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                Ledger ledger = Ledger.fromRS(rs);
                list.add(ledger);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    @Override
    public Ledger save(Ledger obj) {
        String sql = "INSERT INTO store_expan (user_id, store_id, se_date, se_bill_electricity, se_bill_water, se_land_rent, se_total)"
                + "VALUES(?, ?, ?, ?, ?, ?, ?)";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getUserId());
            stmt.setInt(2, obj.getStoreId());
            stmt.setString(3, obj.getSeDate());
            stmt.setInt(4, obj.getSeBillElectricity());
            stmt.setInt(5, obj.getSeBillWater());
            stmt.setInt(6, obj.getSeLandRent());
            stmt.setInt(7, obj.getSeTotal());
//            System.out.println(stmt);
            stmt.executeUpdate();
            int id = DatabaseHelper.getInsertedId(stmt);
            obj.setSeId(id);
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
        return obj;
    }

    @Override
    public Ledger update(Ledger obj) {
        String sql = "UPDATE store_expan SET user_id = ?, store_id = ?, se_date = ?, se_bill_electricity = ?, se_bill_water = ?, se_land_rent = ?, se_total = ? WHERE se_id = ?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getUserId());
            stmt.setInt(2, obj.getStoreId());
            stmt.setString(3, obj.getSeDate());
            stmt.setInt(4, obj.getSeBillElectricity());
            stmt.setInt(5, obj.getSeBillWater());
            stmt.setInt(6, obj.getSeLandRent());
            stmt.setInt(7, obj.getSeTotal());
            stmt.setInt(8, obj.getSeId());

            int ret = stmt.executeUpdate();
            System.out.println(ret);
            return obj;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
    }

    @Override
    public int delete(Ledger obj) {
        String sql = "DELETE FROM store_expan WHERE se_id=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getSeId());
            int ret = stmt.executeUpdate();
            return ret;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return -1;
    }
}
