/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.projectdcoffee.dao;

import com.mycompany.databaseproject.helper.DatabaseHelper;
import com.mycompany.projectdcoffee.model.Store;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Phattharaphon
 */
public class StoreDao implements Dao<Store>{

    @Override
    public Store get(int id) {
        Store store = null;
        String sql = "SELECT * FROM store WHERE store_id = ? ";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                store = Store.fromRS(rs);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return store;
    }

    @Override
    public List<Store> getAll(String where, String order) {
        ArrayList<Store> list = new ArrayList();
        String sql = "SELECT * FROM user where " + where + " ORDER BY" + order;
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                Store user = Store.fromRS(rs);
                list.add(user);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    @Override
    public Store save(Store obj) {
        String sql = "INSERT INTO user (store_name, store_tel, store_addres)"
                + "VALUES(?, ?, ?)";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, obj.getStoreName());
            stmt.setString(2, obj.getStoreTel());
            stmt.setString(3, obj.getAddres());

            stmt.executeUpdate();
            int id = DatabaseHelper.getInsertedId(stmt);
            obj.setStoreId(id);
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
        return obj;
    }

    @Override
    public Store update(Store obj) {
        String sql = "UPDATE user"
                + " SET store_name = ?, store_tel = ?, store_addres = ?"
                + " WHERE store_id = ?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, obj.getStoreName());
            stmt.setString(2, obj.getStoreTel());
            stmt.setString(3, obj.getAddres());
            int ret = stmt.executeUpdate();
            System.out.println(ret);
            return obj;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
    }

    @Override
    public int delete(Store obj) {
        String sql = "DELETE FROM store WHERE store_id=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getStoreId());
            int ret = stmt.executeUpdate();
            return ret;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return -1;
   }

    @Override
    public List<Store> getAll(String store_id) {
        ArrayList<Store> list = new ArrayList();
        String sql = "SELECT * FROM store";
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                Store user = Store.fromRS(rs);
                list.add(user);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());

        }
        return list;

    }
    
}
