/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.projectdcoffee.dao;

import com.mycompany.databaseproject.helper.DatabaseHelper;
import com.mycompany.projectdcoffee.model.MaterialRecieptDetail;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author test1
 */
public class MaterialRecieptDetailDao implements Dao<MaterialRecieptDetail>{
    @Override
    public MaterialRecieptDetail get(int id) {
        MaterialRecieptDetail user = null;
        String sql = "SELECT * FROM material_reciept_detail WHERE mrd_id=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                user = MaterialRecieptDetail.fromRS(rs);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return user;
    }

    public List<MaterialRecieptDetail> getAll() {
        ArrayList<MaterialRecieptDetail> list = new ArrayList();
        String sql = "SELECT * FROM material_reciept_detail";
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                MaterialRecieptDetail user = MaterialRecieptDetail.fromRS(rs);
                list.add(user);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    @Override
    public List<MaterialRecieptDetail> getAll(String where, String order) {
        ArrayList<MaterialRecieptDetail> list = new ArrayList();
        String sql = "SELECT * FROM material_reciept_detail where " + where + " ORDER BY" + order;
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                MaterialRecieptDetail user = MaterialRecieptDetail.fromRS(rs);
                list.add(user);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    public List<MaterialRecieptDetail> getAll(String order) {
        ArrayList<MaterialRecieptDetail> list = new ArrayList();
        String sql = "SELECT * FROM material_reciept_detail  ORDER BY" + order;
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                MaterialRecieptDetail user = MaterialRecieptDetail.fromRS(rs);
                list.add(user);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    @Override
    public MaterialRecieptDetail save(MaterialRecieptDetail obj) {

        String sql = "INSERT INTO material_reciept_detail (mr_id, material_id, material_name, material_unit, mrd_price, mrd_qty, mrd_total)"
                + "VALUES(?, ?, ?, ?, ?, ?, ?)";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
//            stmt.setInt(1, obj.getId());
            stmt.setInt(1, obj.getMaterialRecieptId());
            stmt.setInt(2, obj.getMaterialId());
            stmt.setString(3, obj.getMaterialName());
            stmt.setString(4, obj.getMaterialUnit());
            stmt.setFloat(5, obj.getPrice());
            stmt.setInt(6, obj.getQty());
            stmt.setFloat(7, obj.getTotal());
//            System.out.println(stmt);
            stmt.executeUpdate();
            int id = DatabaseHelper.getInsertedId(stmt);
            obj.setId(id);
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
        return obj;
    }

    @Override
    public MaterialRecieptDetail update(MaterialRecieptDetail obj) {
        String sql = "UPDATE reciept_detail"
                + " SET mr_id = ?, material_id = ?, material_name = ?, material_unit = ?,"
                + " mrd_price = ?, mrd_qty = ?, mrd_total = ?"
                + " WHERE mrd_id = ?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getMaterialRecieptId());
            stmt.setInt(2, obj.getMaterialId());
            stmt.setString(3, obj.getMaterialName());
            stmt.setString(4, obj.getMaterialUnit());
            stmt.setFloat(5, obj.getPrice());
            stmt.setInt(6, obj.getQty());
            stmt.setFloat(7, obj.getTotal());
            stmt.setInt(8, obj.getId());
//            stmt.setInt(7, obj.getId());
//            System.out.println(stmt);
            int ret = stmt.executeUpdate();
            System.out.println(ret);
            return obj;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
    }

    @Override
    public int delete(MaterialRecieptDetail obj) {
        String sql = "DELETE FROM material_receipt_detail WHERE mrd_id=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getId());
            int ret = stmt.executeUpdate();
            return ret;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return -1;
    }
}
