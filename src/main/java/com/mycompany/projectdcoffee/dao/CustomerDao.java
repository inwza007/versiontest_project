/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.projectdcoffee.dao;

import com.mycompany.databaseproject.helper.DatabaseHelper;
import com.mycompany.projectdcoffee.model.Customer;
import com.mycompany.projectdcoffee.model.User;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author werapan
 */
public class CustomerDao implements Dao<Customer> {

    @Override
    public Customer get(int id) {
        Customer customer = null;
        String sql = "SELECT * FROM customer WHERE customer_id=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                customer = Customer.fromRS(rs);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return customer;
    }

    public Customer getByTel(String tel) {
        Customer customer = null;
        String sql = "SELECT * FROM customer WHERE customer_tel=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, tel);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                customer = Customer.fromRS(rs);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return customer;
    }

    public static boolean getByLoginByTel(String tel) {
        User user = null;
        String sql = "SELECT * FROM customer WHERE customer_tel=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, tel);

            ResultSet rs = stmt.executeQuery();

            return rs.next();

        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    public static int getByTelandPoint(String tel) {
        int cuspoint = 0;
        String sql = "SELECT customer_point FROM customer WHERE customer_tel=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, tel);
            ResultSet rs = stmt.executeQuery();

            if (rs.next()) {
                cuspoint = rs.getInt("customer_point");
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return cuspoint;
    }

    public static String getByTelandBirthday(String tel) {
        String Birthday = null;
        String sql = "SELECT customer_birthdate FROM customer WHERE customer_tel=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, tel);
            ResultSet rs = stmt.executeQuery();

            if (rs.next()) {
                Birthday = rs.getString("customer_birthdate");
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return Birthday;
    }

    public static int getIDByTel(String tel) {
        int cusId = 0;
        String sql = "SELECT customer_id FROM customer WHERE customer_tel=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, tel);
            ResultSet rs = stmt.executeQuery();

            if (rs.next()) {
                cusId = rs.getInt("customer_id");
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return cusId;
    }

    public Customer getByPoint(int point) {
        Customer customer = null;
        String sql = "SELECT * FROM customer WHERE customer_point=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, point);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                customer = Customer.fromRS(rs);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return customer;
    }

    public List<Customer> getAll() {
        ArrayList<Customer> list = new ArrayList();
        String sql = "SELECT * FROM customer";
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                Customer customer = Customer.fromRS(rs);
                list.add(customer);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    @Override
    public List<Customer> getAll(String where, String order) {
        ArrayList<Customer> list = new ArrayList();
        String sql = "SELECT * FROM customer WHERE " + where + " ORDER BY " + order;
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                Customer customer = Customer.fromRS(rs);
                list.add(customer);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    public List<Customer> getAll(String order) {
        ArrayList<Customer> list = new ArrayList();
        String sql = "SELECT * FROM customer ORDER BY " + order;
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                Customer customer = Customer.fromRS(rs);
                list.add(customer);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    @Override
    public Customer save(Customer obj) {
        String sql = "INSERT INTO customer (customer_firstname,customer_lastname, customer_tel)"
                + "VALUES(?, ?,?)";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, obj.getFirstName());
            stmt.setString(2, obj.getLastName());
            stmt.setString(3, obj.getTel());
//            System.out.println(stmt);
            stmt.executeUpdate();
            int id = DatabaseHelper.getInsertedId(stmt);
            obj.setId(id);
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
        return obj;
    }

    @Override
    public Customer update(Customer obj) {
        String sql = "UPDATE customer SET customer_firstname = ?,customer_lastname = ?, customer_tel = ?,customer_birthdate = ? WHERE customer_id = ?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, obj.getFirstName());
            stmt.setString(2, obj.getLastName());
            stmt.setString(3, obj.getTel());
            stmt.setString(4, obj.getBirthDay());
            stmt.setInt(5, obj.getId());
//            System.out.println(stmt);
            int ret = stmt.executeUpdate();
            System.out.println(ret);
            return obj;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
    }

    public Customer updatePoint(String Tel, int newPoint) {
        Customer updatedCustomer = null;
        String sql = "UPDATE customer SET customer_point = ? WHERE customer_tel = ?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, newPoint);
            stmt.setString(2, Tel);

            int rowsUpdated = stmt.executeUpdate();

            if (rowsUpdated > 0) {
                System.out.println("Customer's point updated successfully.");
                updatedCustomer = new Customer();
                updatedCustomer.setTel(Tel);
                updatedCustomer.setPoint(newPoint);
            } else {
                System.out.println("Customer not found or point update failed.");
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }

        return updatedCustomer;

    }

    public Customer updatePointABuy(int id, int newPointBuy) {
        Customer updatedCustomer = null;
        String sql = "UPDATE customer SET customer_point = ? WHERE customer_id = ?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, newPointBuy);
            stmt.setInt(2, id);

            int rowsUpdated = stmt.executeUpdate();

            if (rowsUpdated > 0) {
                System.out.println("Customer's point updated successfully.");
                updatedCustomer = new Customer();
                updatedCustomer.setId(id);
                updatedCustomer.setPoint(newPointBuy);
            } else {
                System.out.println("Customer not found or point update failed.");
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }

        return updatedCustomer;

    }
        public Customer updateUseBuy(int id, int newUsePoint) {
        Customer updatedCustomer = null;
        String sql = "UPDATE customer SET customer_usedPoint = ? WHERE customer_id = ?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, newUsePoint);
            stmt.setInt(2, id);

            int rowsUpdated = stmt.executeUpdate();

            if (rowsUpdated > 0) {
                System.out.println("Customer's point updated successfully.");
                updatedCustomer = new Customer();
                updatedCustomer.setId(id);
                updatedCustomer.setPoint(newUsePoint);
            } else {
                System.out.println("Customer not found or point update failed.");
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }

        return updatedCustomer;

    }

    public static int getPointByid(int id) {
        int cuspoint = 0;
        String sql = "SELECT customer_point FROM customer WHERE customer_id=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();

            if (rs.next()) {
                cuspoint = rs.getInt("customer_point");
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return cuspoint;
    }
        public static int getusePointByid(int id) {
        int cuspoint = 0;
        String sql = "SELECT customer_usedPoint FROM customer WHERE customer_id=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();

            if (rs.next()) {
                cuspoint = rs.getInt("customer_usedPoint");
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return cuspoint;
    }

    @Override
    public int delete(Customer obj) {
        String sql = "DELETE FROM customer WHERE customer_id=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getId());
            int ret = stmt.executeUpdate();
            return ret;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return -1;
    }

    public List<Customer> getCustomerByuserdPoint(int limit) {
        ArrayList<Customer> list = new ArrayList();
        String sql = """
                     SELECT ctm.* FROM customer ctm
                            
                            GROUP by ctm.customer_id
                            ORDER BY customer_usedPoint DESC
                            LIMIT ?
                     """;
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, limit);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                Customer obj = Customer.fromRS(rs);
                list.add(obj);
            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }
}
