/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.projectdcoffee.dao;

import com.mycompany.databaseproject.helper.DatabaseHelper;
import com.mycompany.projectdcoffee.model.StockImport;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author gortorr
 */
public class StockImportDao implements Dao<StockImport> {

    @Override
    public StockImport get(int id) {
        StockImport stockImport = null;
        String sql = "SELECT * FROM stock_imoprt WHERE si_id=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                stockImport = StockImport.fromRS(rs);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return stockImport;
    }

    public List<StockImport> getAll() {
        ArrayList<StockImport> list = new ArrayList();
        String sql = "SELECT * FROM stock_import";
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                StockImport stockImport = StockImport.fromRS(rs);
                list.add(stockImport);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }
    
    @Override
    public List<StockImport> getAll(String where, String order) {
        ArrayList<StockImport> list = new ArrayList();
        String sql = "SELECT * FROM stock_import where " + where + " ORDER BY" + order;
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                StockImport stockImport = StockImport.fromRS(rs);
                list.add(stockImport);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }
    
    @Override
    public List<StockImport> getAll(String order) {
        ArrayList<StockImport> list = new ArrayList();
        String sql = "SELECT * FROM stock_import  ORDER BY" + order;
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                StockImport stockImport = StockImport.fromRS(rs);
                list.add(stockImport);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    @Override
    public StockImport save(StockImport obj) {
        String sql = "INSERT INTO stock_import (si_id, si_name, si_qty, supplier_id, supplier_name, si_unit, si_price, si_date_import, user_id,material_id)"
                + "VALUES(?, ?, ?, ?, ?, ?,?,?,?,?,?)";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getSi_id());
            stmt.setInt(2, obj.getSi_supplier_id());
            stmt.setString(3, obj.getSi_supplier_name());
            stmt.setString(4, obj.getSi_name());
            stmt.setString(5, obj.getSi_unit());
            stmt.setFloat(6, obj.getSi_price());
            stmt.setFloat(7, obj.getSi_total());
            stmt.setInt(8, obj.getUser_id());
            stmt.setInt(9, obj.getMaterial_id());
            stmt.setDate(10, (Date) obj.getSi_date_import());
            stmt.setInt(11, obj.getSi_qty());
//            System.out.println(stmt);
            stmt.executeUpdate();
            int id = DatabaseHelper.getInsertedId(stmt);
            obj.setSi_id(id);
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
        return obj;
    }

    @Override
    public StockImport update(StockImport obj) {
        String sql = "UPDATE stock_import"
                + " SET  si_id = ?, si_name = ?, si_qty = ?, supplier_id = ?, supplier_name = ?, si_unit = ?, si_price = ?, si_date_import = ?, user_id = ?,si_total = ?,material_id = ?"
                + " WHERE si_id = ?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getSi_id());
            stmt.setInt(2, obj.getSi_supplier_id());
            stmt.setString(3, obj.getSi_supplier_name());
            stmt.setString(4, obj.getSi_name());
            stmt.setString(5, obj.getSi_unit());
            stmt.setFloat(6, obj.getSi_price());
            stmt.setFloat(7, obj.getSi_total());
            stmt.setInt(8, obj.getUser_id());
            stmt.setInt(9, obj.getMaterial_id());
            stmt.setDate(10, (Date) obj.getSi_date_import());
            stmt.setInt(11, obj.getSi_qty());
//            System.out.println(stmt);
            int ret = stmt.executeUpdate();
            System.out.println(ret);
            return obj;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
    }

    @Override
    public int delete(StockImport obj) {
        String sql = "DELETE FROM stcok_import WHERE si_id=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getSi_id());
            int ret = stmt.executeUpdate();
            return ret;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return -1; 
    }
    
}
