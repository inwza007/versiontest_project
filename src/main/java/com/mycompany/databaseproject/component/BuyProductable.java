  /*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package com.mycompany.databaseproject.component;

import com.mycompany.projectdcoffee.model.Product;

/**
 *
 * @author zacop
 */
public interface BuyProductable {
    public void buy(Product product, String size, int qty, String subStype);
}
