/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/GUIForms/JPanel.java to edit this template
 */
package com.mycompany.databaseproject.component;

import com.mycompany.projectdcoffee.ui.POS.AddTypeProductPanel;
import com.mycompany.projectdcoffee.ui.POS.POSPanel;
import com.mycompany.projectdcoffee.model.Product;
import com.mycompany.projectdcoffee.model.RecieptDetail;
import com.mycompany.projectdcoffee.service.ProductService;
import com.mycompany.projectdcoffee.ui.*;
import java.awt.Image;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.SwingUtilities;
import static javax.swing.WindowConstants.HIDE_ON_CLOSE;

/**
 *
 * @author GAMING PC
 */
public class ProductitemPanel extends javax.swing.JPanel {

    private Product editedProduct = new Product();
    private Product product;
    private ArrayList<BuyProductable> subscribers = new ArrayList<>();
    public int qty_product;
    public String size = "";
    public Integer qty = 0;
    public String sweetLevel = "";
    public String cancel = "";
    public String product_sub_type;
    public String p_type;
    public boolean status;
    private final POSPanel aThis;
    private ProductService productService;
    private ArrayList<Product> products;

    /**
     * Creates new form ProductitemPanel
     *
     *
     */
    public ProductitemPanel(Product p, POSPanel aThis, String type) {
        initComponents();
        this.aThis = aThis;
        product = p;
        FIlterShowProduct(type);

    }

    private void FIlterShowProduct(String type) {
        if (type.equals("all")) {
            showAllProduct(product);
        } else if (type.equals("drink")) {
            showOnlyDrinkProduct(product);
        } else if (type.equals("dessert")) {
            showOnlyDessrtProduct(product);
        }
    }

    private void showAllProduct(Product p) {
        product = p;
        productService = new ProductService();
        products = productService.getProductsOrderByName();
        int productSize = products.size();
        lblName.setText(product.getProductName());
        ImageIcon icon;
        if (product.getProductType().equals("drink")) {
            icon = new ImageIcon("./picture/product_drink/product" + p.getId() + ".png");
        } else {
            icon = new ImageIcon("./picture/product_dessert/product" + p.getId() + ".png");
        }
        Image image = icon.getImage();
        int width = image.getWidth(null);
        int height = image.getHeight(null);
        Image newImage = image.getScaledInstance((int) ((181.0 * width) / height), 175, Image.SCALE_SMOOTH);
        icon.setImage(newImage);
        lblImage.setIcon(icon);

        lblImage.addMouseListener(new MouseAdapter() {

            @Override
            public void mouseClicked(MouseEvent e) {
                openDialog(product.getId(),product.getProductType());
                if (cancel.equals("Y")) {
                    return;
                }
            }
            
        });
    }

    private void showOnlyDrinkProduct(Product p) {
        product = p;
        productService = new ProductService();
        products = productService.getProductsOrderByDrink();
        int productSize = products.size();
        lblName.setText(product.getProductName());
        ImageIcon icon = new ImageIcon("./picture/product_drink/product" + p.getId() + ".png");
        Image image = icon.getImage();
        int width = image.getWidth(null);
        int height = image.getHeight(null);
        Image newImage = image.getScaledInstance((int) ((181.0 * width) / height), 175, Image.SCALE_SMOOTH);
        icon.setImage(newImage);
        lblImage.setIcon(icon);
        lblImage.addMouseListener(new MouseAdapter() {

            @Override
            public void mouseClicked(MouseEvent e) {
                openDialog(product.getId(),product.getProductType());
                if (cancel.equals("Y")) {
                    return;
                }
            }
        });
    }

    private void showOnlyDessrtProduct(Product p) {
        product = p;
        productService = new ProductService();
        products = productService.getProductsOrderByDessert();
        int productSize = products.size();
        lblName.setText(product.getProductName());
        ImageIcon icon = new ImageIcon("./picture/product_dessert/product" + p.getId() + ".png");
        Image image = icon.getImage();
        int width = image.getWidth(null);
        int height = image.getHeight(null);
        Image newImage = image.getScaledInstance((int) ((181.0 * width) / height), 175, Image.SCALE_SMOOTH);
        icon.setImage(newImage);
        lblImage.setIcon(icon);
        lblImage.addMouseListener(new MouseAdapter() {

            @Override
            public void mouseClicked(MouseEvent e) {
                openDialog(product.getId(),product.getProductType());
                if (cancel.equals("Y")) {
                    return;
                }
            }
        });
    }

    public void addToTable(int qty, String size, String sweet, String subStype) {
        for (BuyProductable s : subscribers) {
            product.setProductSize(size);
            product.setProductSweetLevel(sweet);
            product.setProductSubType(subStype);
            s.buy(product, size, qty, subStype);

        }
    }

    public void openDialog(int id,String type) {
        JFrame frame = (JFrame) SwingUtilities.getRoot(this);
        AddTypeProductPanel addType = new AddTypeProductPanel(frame, this, editedProduct, id,type);
        enableForm(addType);
        addType.setLocationRelativeTo(this);
        addType.setVisible(true);
        size = addType.getS();
        qty = addType.getQty();
        sweetLevel = addType.getSweet();
        cancel = addType.getCancel();
        product_sub_type = addType.getSubType();
        aThis.productSubType = product_sub_type;

    }

    private void enableForm(AddTypeProductPanel addType) {
        p_type = product.getProductType();
        if (p_type.equals("drink")) {
            status = true;
            addType.enableForm(status);

        } else if (p_type.equals("dessert")) {
            status = false;
            addType.enableForm(status);
        }
    }

    public void addOnBuyProduct(BuyProductable subscriber) {
        subscribers.add(subscriber);
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        lblImage = new javax.swing.JLabel();
        lblName = new javax.swing.JLabel();

        jPanel1.setBackground(new java.awt.Color(255, 255, 255));

        lblImage.setBackground(new java.awt.Color(255, 255, 255));
        lblImage.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lblImage.setOpaque(true);
        lblImage.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lblImageMouseClicked(evt);
            }
        });

        lblName.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lblName.setText("Product Name");

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(lblName, javax.swing.GroupLayout.DEFAULT_SIZE, 152, Short.MAX_VALUE)
                    .addComponent(lblImage, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(lblImage, javax.swing.GroupLayout.PREFERRED_SIZE, 139, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(lblName, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
    }// </editor-fold>//GEN-END:initComponents

    private void lblImageMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lblImageMouseClicked

    }//GEN-LAST:event_lblImageMouseClicked


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JPanel jPanel1;
    private javax.swing.JLabel lblImage;
    private javax.swing.JLabel lblName;
    // End of variables declaration//GEN-END:variables
}
